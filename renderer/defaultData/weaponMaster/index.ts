import { name } from '@@identifier/core/weaponMaster';
import { DefaultDataSchemas, PickType } from '@@interface';
type DefaultDataSchema = PickType<DefaultDataSchemas, typeof name>;

import basis from './weaponMaster.default.json';
export default {
  basis: basis.memory,
  override: {},
} as DefaultDataSchema;
