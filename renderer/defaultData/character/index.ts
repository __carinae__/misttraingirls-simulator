import { name } from '@@identifier/core/character';
import { DefaultDataSchemas, PickType } from '@@interface';
type DefaultDataSchema = PickType<DefaultDataSchemas, typeof name>;

import merge from 'deepmerge';

import baseOverride from './base.override';
import basis from './character.default.json';
import layerOverride from './layer.override';
export default {
  basis: basis.memory,
  override: merge(baseOverride, layerOverride),
} as DefaultDataSchema;
