import { name } from '@@identifier/core/character';
import { DefaultDataSchemas, PickType } from '@@interface';
type DefaultDataSchema = PickType<DefaultDataSchemas, typeof name>;

/** [NOTE]
 * Whenever new characters are added, add their information as override
 * with computed properties (status bonus, layer status, etc.) undefined
 * so that such properties will be properly recomputed.
 */
export default {
  // '..': {
  //   bases: [
  //     {
  //       /** overriden properties */
  //       keys: ['weapon', 'attackType', 'from', 'race'],
  //       /** newly added entity ids */
  //       ids: [],
  //     },
  //   ],
  // },
} as Partial<DefaultDataSchema['override']>;
