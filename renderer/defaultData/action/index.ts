import { name } from '@@identifier/core/action';
import { DefaultDataSchemas, PickType } from '@@interface';
type DefaultDataSchema = PickType<DefaultDataSchemas, typeof name>;

import basis from './action.default.json';
import override from './action.override';
export default {
  basis: basis.memory,
  override,
} as DefaultDataSchema;
