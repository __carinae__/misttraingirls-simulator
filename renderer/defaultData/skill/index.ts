import { name } from '@@identifier/core/skill';
import { DefaultDataSchemas, PickType } from '@@interface';
type DefaultDataSchema = PickType<DefaultDataSchemas, typeof name>;

import basis from './skill.default.json';
import override from './skill.override';
export default {
  basis: basis.memory,
  override,
} as DefaultDataSchema;
