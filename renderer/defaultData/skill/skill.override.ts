import { name } from '@@identifier/core/skill';
import { DefaultDataSchemas, PickType } from '@@interface';
type DefaultDataSchema = PickType<DefaultDataSchemas, typeof name>;

export default {} as DefaultDataSchema['override'];
