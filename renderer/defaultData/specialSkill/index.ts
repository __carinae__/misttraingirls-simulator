import { name } from '@@identifier/core/specialSkill';
import { DefaultDataSchemas, PickType } from '@@interface';
type DefaultDataSchema = PickType<DefaultDataSchemas, typeof name>;

import basis from './specialSkill.default.json';
import override from './specialSkill.override';
export default {
  basis: basis.memory,
  override,
} as DefaultDataSchema;
