import { name } from '@@identifier/core/ability';
import { DefaultDataSchemas, PickType } from '@@interface';
type DefaultDataSchema = PickType<DefaultDataSchemas, typeof name>;

import basis from './ability.basis';
import override from './ability.override';
export default {
  basis,
  override,
} as DefaultDataSchema;
