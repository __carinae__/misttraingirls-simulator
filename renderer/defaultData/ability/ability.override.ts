import { name } from '@@identifier/core/ability';
import { DefaultDataSchemas, PickType } from '@@interface';
type DefaultDataSchema = PickType<DefaultDataSchemas, typeof name>;

export default {} as DefaultDataSchema['override'];
