import { name } from '@@identifier/core/ability';
import { DefaultDataSchemas, PickType } from '@@interface';
type DefaultDataSchema = PickType<DefaultDataSchemas, typeof name>;

export default {
  abilities: {
    'パワーアタック【小】': {
      id: 'パワーアタック【小】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          postActionHook: [
            {
              type: 'statusEffect',
              target: '自身',
              status: '物攻',
              percentage: 3,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'パワーアタック【中】': {
      id: 'パワーアタック【中】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          postActionHook: [
            {
              type: 'statusEffect',
              target: '自身',
              status: '物攻',
              percentage: 5,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'パワーアタック【大】': {
      id: 'パワーアタック【大】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          postActionHook: [
            {
              type: 'statusEffect',
              target: '自身',
              status: '物攻',
              percentage: 10,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'ダメージアップ【極小】': {
      id: 'ダメージアップ【極小】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          preBattleHook: [
            {
              type: 'statusEffect',
              target: '自身',
              status: 'コンディション',
              percentage: 2,
              turn: Infinity,
            },
          ],
        },
      ],
    },
    'ダメージアップ【小】': {
      id: 'ダメージアップ【小】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          preBattleHook: [
            {
              type: 'statusEffect',
              target: '自身',
              status: 'コンディション',
              percentage: 5,
              turn: Infinity,
            },
          ],
        },
      ],
    },
    'ダメージアップ【中】': {
      id: 'ダメージアップ【中】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          preBattleHook: [
            {
              type: 'statusEffect',
              target: '自身',
              status: 'コンディション',
              percentage: 8,
              turn: Infinity,
            },
          ],
        },
      ],
    },
    'ダメージアップ【大】': {
      id: 'ダメージアップ【大】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          preBattleHook: [
            {
              type: 'statusEffect',
              target: '自身',
              status: 'コンディション',
              percentage: 10,
              turn: Infinity,
            },
          ],
        },
      ],
    },
    'ドレインアタック【小】': {
      id: 'ドレインアタック【小】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          postActionHook: [
            {
              type: 'cure',
              target: '自身',
              value: 3,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'ドレインアタック改【小】': {
      id: 'ドレインアタック改【小】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          postActionHook: [
            {
              type: 'cure',
              target: '自身',
              value: 3,
              probabilityPercentage: 20,
            },
          ],
        },
      ],
    },
    'ドレインアタック【中】': {
      id: 'ドレインアタック【中】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          postActionHook: [
            {
              type: 'cure',
              target: '自身',
              value: 5,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'ドレインアタック改【中】': {
      id: 'ドレインアタック改【中】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          postActionHook: [
            {
              type: 'cure',
              target: '自身',
              value: 5,
              probabilityPercentage: 20,
            },
          ],
        },
      ],
    },
    'ドレインアタック【大】': {
      id: 'ドレインアタック【大】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          postActionHook: [
            {
              type: 'cure',
              target: '自身',
              value: 10,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'ドレインアタック改【大】': {
      id: 'ドレインアタック改【大】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          postActionHook: [
            {
              type: 'cure',
              target: '自身',
              value: 10,
              probabilityPercentage: 20,
            },
          ],
        },
      ],
    },
    'ウィークマスター攻【小】': {
      id: 'ウィークマスター攻【小】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          preBattleHook: [
            {
              type: 'statusEffect',
              target: '自身',
              status: 'コンディション',
              percentage: 4,
              turn: Infinity,
              condition: [{ attack: { weak: true } }],
            },
          ],
        },
      ],
    },
    'ウィークマスター攻【中】': {
      id: 'ウィークマスター攻【中】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          preBattleHook: [
            {
              type: 'statusEffect',
              target: '自身',
              status: 'コンディション',
              percentage: 8,
              turn: Infinity,
              condition: [{ attack: { weak: true } }],
            },
          ],
        },
      ],
    },
    'ウィークマスター攻【大】': {
      id: 'ウィークマスター攻【大】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          preBattleHook: [
            {
              type: 'statusEffect',
              target: '自身',
              status: 'コンディション',
              percentage: 10,
              turn: Infinity,
              condition: [{ attack: { weak: true } }],
            },
          ],
        },
      ],
    },
    'ウィークマスター攻【特大】': {
      id: 'ウィークマスター攻【特大】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          preBattleHook: [
            {
              type: 'statusEffect',
              target: '自身',
              status: 'コンディション',
              percentage: 15,
              turn: Infinity,
              condition: [{ attack: { weak: true } }],
            },
          ],
        },
      ],
    },
    '雑魚どもが！': {
      id: '雑魚どもが！',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          preWaveHook: [
            {
              type: 'cure',
              target: '自身',
              value: 5,
              probabilityPercentage: 10,
            },
          ],
          postActionHook: [
            {
              type: 'cure',
              target: '自身',
              value: 5,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'ディフェンドカウンター【大】': {
      id: 'ディフェンドカウンター【大】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          pushPassiveActions: [
            {
              type: 'instantProtect',
              percentage: 15,
              probabilityPercentage: 25,
              turn: Infinity,
            },
            {
              type: 'counter',
              postActionPieceHook: true,
              turn: Infinity,
              actions: [
                {
                  type: 'attack',
                  impact: [{ value: 12 }],
                },
              ],
            },
          ],
        },
      ],
    },
    'ガードアタック【小】': {
      id: 'ガードアタック【小】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          postActionHook: [
            {
              type: 'statusEffect',
              target: '自身',
              status: '物防',
              percentage: 3,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'ガードアタック【中】': {
      id: 'ガードアタック【中】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          postActionHook: [
            {
              type: 'statusEffect',
              target: '自身',
              status: '物防',
              percentage: 5,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'ガードアタック【大】': {
      id: 'ガードアタック【大】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          postActionHook: [
            {
              type: 'statusEffect',
              target: '自身',
              status: '物防',
              percentage: 10,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'スピードアタック【小】': {
      id: 'スピードアタック【小】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          postActionHook: [
            {
              type: 'statusEffect',
              target: '自身',
              status: '速さ',
              percentage: 3,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'スピードアタック【中】': {
      id: 'スピードアタック【中】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          postActionHook: [
            {
              type: 'statusEffect',
              target: '自身',
              status: '速さ',
              percentage: 5,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'スピードアタック【大】': {
      id: 'スピードアタック【大】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          postActionHook: [
            {
              type: 'statusEffect',
              target: '自身',
              status: '速さ',
              percentage: 10,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'ヒットアタック【小】': {
      id: 'ヒットアタック【小】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          postActionHook: [
            {
              type: 'statusEffect',
              target: '自身',
              status: '命中',
              percentage: 3,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'ヒットアタック【中】': {
      id: 'ヒットアタック【中】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          postActionHook: [
            {
              type: 'statusEffect',
              target: '自身',
              status: '命中',
              percentage: 5,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'ヒットアタック【大】': {
      id: 'ヒットアタック【大】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          postActionHook: [
            {
              type: 'statusEffect',
              target: '自身',
              status: '命中',
              percentage: 10,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'マインドアタック【小】': {
      id: 'マインドアタック【小】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          postActionHook: [
            {
              type: 'statusEffect',
              target: '自身',
              status: '魔防',
              percentage: 3,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'マインドアタック【中】': {
      id: 'マインドアタック【中】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          postActionHook: [
            {
              type: 'statusEffect',
              target: '自身',
              status: '魔防',
              percentage: 5,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'マインドアタック【大】': {
      id: 'マインドアタック【大】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          postActionHook: [
            {
              type: 'statusEffect',
              target: '自身',
              status: '魔防',
              percentage: 10,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'マジックアタック【小】': {
      id: 'マジックアタック【小】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          postActionHook: [
            {
              type: 'statusEffect',
              target: '自身',
              status: '魔攻',
              percentage: 3,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'マジックアタック【中】': {
      id: 'マジックアタック【中】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          postActionHook: [
            {
              type: 'statusEffect',
              target: '自身',
              status: '魔攻',
              percentage: 5,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'マジックアタック【大】': {
      id: 'マジックアタック【大】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          postActionHook: [
            {
              type: 'statusEffect',
              target: '自身',
              status: '魔攻',
              percentage: 10,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'ラックアタック【小】': {
      id: 'ラックアタック【小】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          postActionHook: [
            {
              type: 'statusEffect',
              target: '自身',
              status: '幸運',
              percentage: 3,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'ラックアタック【中】': {
      id: 'ラックアタック【中】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          postActionHook: [
            {
              type: 'statusEffect',
              target: '自身',
              status: '幸運',
              percentage: 5,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'ラックアタック【大】': {
      id: 'ラックアタック【大】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          postActionHook: [
            {
              type: 'statusEffect',
              target: '自身',
              status: '幸運',
              percentage: 10,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'リカバリーアタック【小】': {
      id: 'リカバリーアタック【小】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          postActionHook: [
            {
              type: 'statusEffect',
              target: '自身',
              status: '回復',
              percentage: 3,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'リカバリーアタック【中】': {
      id: 'リカバリーアタック【中】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          postActionHook: [
            {
              type: 'statusEffect',
              target: '自身',
              status: '回復',
              percentage: 5,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'リカバリーアタック【大】': {
      id: 'リカバリーアタック【大】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          postActionHook: [
            {
              type: 'statusEffect',
              target: '自身',
              status: '回復',
              percentage: 10,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'キュアアタック【極小】': {
      id: 'キュアアタック【極小】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          postActionHook: [
            {
              type: 'healStatusAilment',
              target: '自身',
              healAll: true,
              probabilityPercentage: 2,
            },
          ],
        },
      ],
    },
    'キュアアタック【小】': {
      id: 'キュアアタック【小】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          postActionHook: [
            {
              type: 'healStatusAilment',
              target: '自身',
              healAll: true,
              probabilityPercentage: 5,
            },
          ],
        },
      ],
    },
    'キュアアタック【中】': {
      id: 'キュアアタック【中】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          postActionHook: [
            {
              type: 'healStatusAilment',
              target: '自身',
              healAll: true,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'キュアアタック【大】': {
      id: 'キュアアタック【大】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          postActionHook: [
            {
              type: 'healStatusAilment',
              target: '自身',
              healAll: true,
              probabilityPercentage: 25,
            },
          ],
        },
      ],
    },
    'パワーエール【小】': {
      id: 'パワーエール【小】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          preTurnHook: [
            {
              type: 'statusEffect',
              target: '味方全体',
              status: '物攻',
              percentage: 1,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'パワーエール【中】': {
      id: 'パワーエール【中】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          preTurnHook: [
            {
              type: 'statusEffect',
              target: '味方全体',
              status: '物攻',
              percentage: 2,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'パワーエール【大】': {
      id: 'パワーエール【大】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          preTurnHook: [
            {
              type: 'statusEffect',
              target: '味方全体',
              status: '物攻',
              percentage: 5,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'ガードエール【小】': {
      id: 'ガードエール【小】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          preTurnHook: [
            {
              type: 'statusEffect',
              target: '味方全体',
              status: '物防',
              percentage: 1,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'ガードエール【中】': {
      id: 'ガードエール【中】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          preTurnHook: [
            {
              type: 'statusEffect',
              target: '味方全体',
              status: '物防',
              percentage: 2,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'ガードエール【大】': {
      id: 'ガードエール【大】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          preTurnHook: [
            {
              type: 'statusEffect',
              target: '味方全体',
              status: '物防',
              percentage: 5,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'スピードエール【小】': {
      id: 'スピードエール【小】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          preTurnHook: [
            {
              type: 'statusEffect',
              target: '味方全体',
              status: '速さ',
              percentage: 1,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'スピードエール【中】': {
      id: 'スピードエール【中】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          preTurnHook: [
            {
              type: 'statusEffect',
              target: '味方全体',
              status: '速さ',
              percentage: 2,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'スピードエール【大】': {
      id: 'スピードエール【大】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          preTurnHook: [
            {
              type: 'statusEffect',
              target: '味方全体',
              status: '速さ',
              percentage: 5,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'ヒットエール【小】': {
      id: 'ヒットエール【小】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          preTurnHook: [
            {
              type: 'statusEffect',
              target: '味方全体',
              status: '命中',
              percentage: 1,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'ヒットエール【中】': {
      id: 'ヒットエール【中】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          preTurnHook: [
            {
              type: 'statusEffect',
              target: '味方全体',
              status: '命中',
              percentage: 2,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'ヒットエール【大】': {
      id: 'ヒットエール【大】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          preTurnHook: [
            {
              type: 'statusEffect',
              target: '味方全体',
              status: '命中',
              percentage: 5,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'マインドエール【小】': {
      id: 'マインドエール【小】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          preTurnHook: [
            {
              type: 'statusEffect',
              target: '味方全体',
              status: '魔防',
              percentage: 1,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'マインドエール【中】': {
      id: 'マインドエール【中】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          preTurnHook: [
            {
              type: 'statusEffect',
              target: '味方全体',
              status: '魔防',
              percentage: 2,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'マインドエール【大】': {
      id: 'マインドエール【大】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          preTurnHook: [
            {
              type: 'statusEffect',
              target: '味方全体',
              status: '魔防',
              percentage: 5,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'マジックエール【小】': {
      id: 'マジックエール【小】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          preTurnHook: [
            {
              type: 'statusEffect',
              target: '味方全体',
              status: '魔攻',
              percentage: 1,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'マジックエール【中】': {
      id: 'マジックエール【中】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          preTurnHook: [
            {
              type: 'statusEffect',
              target: '味方全体',
              status: '魔攻',
              percentage: 2,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'マジックエール【大】': {
      id: 'マジックエール【大】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          preTurnHook: [
            {
              type: 'statusEffect',
              target: '味方全体',
              status: '魔攻',
              percentage: 5,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'ラックエール【小】': {
      id: 'ラックエール【小】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          preTurnHook: [
            {
              type: 'statusEffect',
              target: '味方全体',
              status: '幸運',
              percentage: 1,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'ラックエール【中】': {
      id: 'ラックエール【中】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          preTurnHook: [
            {
              type: 'statusEffect',
              target: '味方全体',
              status: '幸運',
              percentage: 2,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'ラックエール【大】': {
      id: 'ラックエール【大】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          preTurnHook: [
            {
              type: 'statusEffect',
              target: '味方全体',
              status: '幸運',
              percentage: 5,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'リカバリーエール【小】': {
      id: 'リカバリーエール【小】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          preTurnHook: [
            {
              type: 'statusEffect',
              target: '味方全体',
              status: '回復',
              percentage: 1,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'リカバリーエール【中】': {
      id: 'リカバリーエール【中】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          preTurnHook: [
            {
              type: 'statusEffect',
              target: '味方全体',
              status: '回復',
              percentage: 2,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },
    'リカバリーエール【大】': {
      id: 'リカバリーエール【大】',
      undeletable: true,
      effects: [
        {
          target: [{ tag: '自身' }],
          preTurnHook: [
            {
              type: 'statusEffect',
              target: '味方全体',
              status: '回復',
              percentage: 5,
              turn: 2,
              probabilityPercentage: 10,
            },
          ],
        },
      ],
    },

    // '': {
    //   id: '',
    //   effects: [
    //     {
    //       target: ,
    //     },
    //   ],
    // },
  },
} as DefaultDataSchema['basis'];
