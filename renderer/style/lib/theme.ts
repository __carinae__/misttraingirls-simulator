import { createTheme } from '@mui/material/styles';

export const theme = createTheme({
  palette: {
    mode: 'dark',
    primary: {
      main: '#91a3ff',
    },
    secondary: {
      main: '#c7c9ff',
    },
    background: {
      default: '#181818',
      paper: '#212021',
    },
  },
  typography: {
    /**
     * In Japanese the characters are usually larger.
     * The computed font size F by the browser follows this mathematical equation:
     * F = {specification} * (typography.fontsize}/14 * {html.fontsize}/{typography.htmlFontsize}}
     */
    fontSize: 10,
    /**
     * Tell Material-UI what's the font-size on the html element is.
     * font-size: 62.5%; // 62.5% of 16px = 10px
     */
    htmlFontSize: 16,
  },
});
