import { createStyles, makeStyles } from '@mui/styles';
import React from 'react';

import { color } from './shared';

const size = { rhombus: 15, ball: 9 };
const delay = 700;
const ballColor = color.secondary;

export const useStyles = makeStyles(
  createStyles({
    loopingRhombusesSpinner: {
      boxSizing: 'border-box',
      '& *:': {
        boxSizing: 'border-box',
      },
      width: size.rhombus * 4,
      height: size.rhombus,
      position: 'absolute',
      top: 0,
      right: 0,
      bottom: 0,
      left: 0,
      margin: 'auto',
    },
    '@keyframes loopingRhombusesSpinnerAnimation': {
      '0%': {
        transform: 'translateX(0) rotate(45deg) scale(0)',
        backgroundColor: color.primary,
      },
      '50%': {
        transform: `translateX(${-2 * size.rhombus}px) rotate(45deg) scale(1)`,
        backgroundColor: color.secondary,
      },
      '100%': {
        transform: `translateX(${-4 * size.rhombus}px) rotate(45deg) scale(0)`,
        backgroundColor: color.primary,
      },
    },
    rhombus: {
      height: size.rhombus,
      width: size.rhombus,
      left: size.rhombus * 4 - size.rhombus / 2,
      position: 'absolute',
      margin: '0 auto',
      borderRadius: 2,
      transformOrigin: 'center',
      transform: 'translateY(0) rotate(45deg) scale(0)',
      animation: `$loopingRhombusesSpinnerAnimation ${5 * delay}ms linear infinite`,
      '&:nth-child(1)': {
        animationDelay: `${-delay}ms`,
      },
      '&:nth-child(2)': {
        animationDelay: `${-delay * 2}ms`,
      },
      '&:nth-child(3)': {
        animationDelay: `${-delay * 3}ms`,
      },
      '&:nth-child(4)': {
        animationDelay: `${-delay * 4}ms`,
      },
      '&:nth-child(5)': {
        animationDelay: `${-delay * 5}ms`,
      },
    },
    '@keyframes loopingBoundBallAnimation': {
      '0%': {
        transform: `translate(${-2 * size.rhombus}px, ${size.rhombus / 2}px) rotate(45deg)`,
      },
      '25%': {
        transform: `translate(${-2 * size.rhombus}px, ${size.rhombus / 4}px) rotate(90deg)`,
      },
      '50%': {
        transform: `translate(${-2 * size.rhombus}px, 0) rotate(135deg)`,
      },
      '75%': {
        transform: `translate(${-2 * size.rhombus}px, ${size.rhombus / 4}px) rotate(180deg)`,
      },
      '100%': {
        transform: `translate(${-2 * size.rhombus}px, ${size.rhombus / 2}px) rotate(225deg)`,
      },
    },
    boundBall: {
      position: 'absolute',
      bottom: size.rhombus + size.ball / 2 - 1,
      left: size.rhombus * 4 - size.ball / 2,
      width: size.ball,
      border: `4px solid ${ballColor}`,
      backgroundColor: '#e7e9ff',
      height: size.ball,
      borderRadius: 2,
      transformOrigin: 'center',
      animation: `$loopingBoundBallAnimation ${delay}ms linear infinite`,
    },
  })
);

export const LoopingRhombusesSpinner: React.FC = props => {
  const classes = useStyles();
  const { children, ...rest } = props;
  return (
    <div className={classes.loopingRhombusesSpinner} {...rest}>
      {children}
    </div>
  );
};

export const Rhombus: React.FC = props => {
  const classes = useStyles();
  const { children, ...rest } = props;
  return (
    <div className={classes.rhombus} {...rest}>
      {children}
    </div>
  );
};

export const BoundBall: React.FC = props => {
  const classes = useStyles();
  const { children, ...rest } = props;
  return (
    <div className={classes.boundBall} {...rest}>
      {children}
    </div>
  );
};
