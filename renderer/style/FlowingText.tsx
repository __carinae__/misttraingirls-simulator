import { ClassNameMap, createStyles, makeStyles } from '@mui/styles';
import React from 'react';

export const useStyles = (textWidth: number): ((props?: unknown) => ClassNameMap) =>
  makeStyles(
    createStyles({
      '@keyframes flowingAnimation': {
        '0%': {
          transform: 'translateX(100%)',
        },
        '100%': {
          transform: `translateX(${-textWidth}px)`,
        },
      },
      flowingText: {
        textAlign: 'left',
        fontSize: 10.5,
        fontWeight: 580,
        color: '#fff',
        animation: `$flowingAnimation ${(18 / 1000) * (textWidth + 820)}s linear infinite`,
        width: '100%',
        whiteSpace: 'nowrap',
      },
    })
  );

type FlowingTextProps = {
  textWidth: number;
};
export const FlowingText: React.FC<FlowingTextProps> = props => {
  const { textWidth, children } = props;
  const classes = useStyles(textWidth)();
  return <div className={classes.flowingText}>{children}</div>;
};
