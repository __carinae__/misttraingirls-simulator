import { inputClasses } from '@mui/material';
import { Theme } from '@mui/material/styles';
import createStyles from '@mui/styles/createStyles';
import makeStyles from '@mui/styles/makeStyles';

/**
 * Constants.
 */
export const drawerWidth = 45;
export const listWidth = 183;
export const formControlWidth = 188.5;
export const formControlSideMargin = 3.5;
export const formControlSidePadding = 5;
export const formContentHeight = 41.375;
export const borderWidth = 1;
export const weaponMasterWindowHeight = 63;
export const dummySDdisplayHeight = 142;
export const tabHeight = 32;
export const tabHeaderHeight = 48;
export const fontSize = { default: 10 };
export const tagMarginColumn = 12.25 / 2;
export const tagMarginColumnInGroup = 7 / 2;

export const highlightedPanelColor = '#272627';
export const panelColor = '#242324';
export const adornmentColor = '#bbbbbb';

const rootStyle = (theme: Theme) =>
  ({
    root: {
      textAlign: 'center',
      '& .MuiBadge-root': {
        marginRight: theme.spacing(4),
      },
      display: 'flex',
      flexDirection: 'row',
    },
    '@global': {
      '*::-webkit-scrollbar': {
        width: 0,
        height: 0,
      },
    },
    drawer: {
      width: drawerWidth,
    },
    drawerPaper: {
      width: drawerWidth,
      overflow: 'hidden',
      backgroundColor: theme.palette.background.default,
      borderRight: `${borderWidth}px solid ${theme.palette.background.default}`,
    },
    drawerPaperRight: {
      width: drawerWidth,
      overflow: 'hidden',
      backgroundColor: theme.palette.background.default,
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'flex-end',
    },
    content: {
      backgroundColor: theme.palette.background.default,
      height: '100%',
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'nowrap',
      overflow: 'hidden',
    },
  } as const);

const tabListStyle = (theme: Theme) =>
  ({
    tab: {
      minWidth: drawerWidth,
      width: drawerWidth,
    },
    tabs: {
      // borderRight: `${borderWidth}px solid ${theme.palette.divider}`,
    },
    listBasement: {
      width: listWidth,
    },
    list: {
      width: listWidth - 2,
      height: `calc(100% - ${tabHeaderHeight}px - ${2 * borderWidth}px)`,
      overflow: 'hidden',
      border: `${borderWidth}px solid ${highlightedPanelColor}`,
      borderLeft: '0px',
    },
    listHeader: {
      width: listWidth - 2,
      height: tabHeaderHeight,
      minHeight: tabHeaderHeight,
      paddingTop: 3.5,
    },
    listItem: {
      marginLeft: -7,
      marginRight: -10,
      height: tabHeight,
      minHeight: tabHeight,
    },
    tabIconLabelWrapper: {
      flexDirection: 'row',
      justifyContent: 'left',
      backgroundColor: theme.palette.divider,
      borderTop: `1px solid ${theme.palette.background.paper}`,
      borderBottom: `1px solid ${theme.palette.background.paper}`,
      boxShadow: `0px 1px 30px 0px ${theme.palette.background.paper} inset`,
    },
    listItemVirtualized: {
      height: tabHeight,
      minHeight: tabHeight,
    },
    tabIconLabelWrapperVirtualized: {
      height: tabHeight,
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'left',
      backgroundColor: theme.palette.divider,
      // borderTop: `1px solid ${theme.palette.background.default}`,
      borderBottom: `1px solid ${theme.palette.background.default}`,
      boxShadow: `0px 1px 30px 0px ${theme.palette.background.default} inset`,
      borderTopLeftRadius: 16,
      borderBottomLeftRadius: 16,
    },
    sortFilterFormGroup: {
      padding: '0px 0px 18px 10px',
      margin: '0px 0px 10px 10px',
      backgroundColor: highlightedPanelColor,
      borderRadius: 6,
    },
    sortOrderGroup: {
      padding: '0px 0px 18px 10px',
      margin: '0px 0px 10px 10px',
      backgroundColor: theme.palette.background.paper,
      borderRadius: 6,
    },
  } as const);

const tooltipStyle = (theme: Theme) =>
  ({
    weaponMasterTooltip: {
      fontSize: 10,
      backgroundColor: panelColor,
      fontWeight: theme.typography.fontWeightBold,
      border: `1px solid ${theme.palette.divider}`,
      margin: '2px 0px 0px 52px',
      padding: '5px 10px 3px 10px',
    },
    tooltip: {
      fontSize: 10,
      fontWeight: theme.typography.fontWeightBold,
      textAlign: 'center',
    },
    tabTooltip: {
      fontSize: 10,
      backgroundColor: theme.palette.primary.main,
      color: theme.palette.background.default,
      fontWeight: theme.typography.fontWeightBold,
    },
    tabTooltipUnselected: {
      fontSize: 10,
      fontWeight: theme.typography.fontWeightBold,
    },
    tabTooltipArrow: {
      color: theme.palette.secondary.main,
    },
  } as const);

const backgroundStyle = (theme: Theme) =>
  ({
    defaultBackground: {
      backgroundColor: theme.palette.background.default,
    },
    paperBackground: {
      backgroundColor: theme.palette.background.paper,
    },
    panelBackground: {
      backgroundColor: panelColor,
    },
    highlightedPanelBackground: {
      backgroundColor: highlightedPanelColor,
    },
  } as const);

const shadowStyle = (theme: Theme) =>
  ({
    insideShadowDark: {
      boxShadow: '0px 1px 2px 0px #151515 inset',
    },
    insideShadow: {
      boxShadow: '0px 1px 2px 0px #1f1e1f inset',
    },
    insideShadowDivider: {
      boxShadow: `0px 0px 30px -10px ${theme.palette.divider} inset`,
    },
  } as const);

const borderStyle = (theme: Theme) =>
  ({
    borderHighlightColor: {
      border: `${borderWidth}px solid #888888`,
    },
    borderPrimaryColor: {
      border: `${borderWidth}px solid ${theme.palette.primary.main}`,
    },
    borderSecondaryColor: {
      border: `${borderWidth}px solid ${theme.palette.secondary.main}`,
    },
    borderPaperColor: {
      border: `${borderWidth}px solid ${theme.palette.background.paper}`,
    },
    borderHighlightedPanelColor: {
      border: `${borderWidth}px solid ${highlightedPanelColor}`,
    },
    borderDividerColor: {
      border: `${borderWidth}px solid ${theme.palette.divider}`,
    },
    borderPanelColor: {
      border: `${borderWidth}px solid ${panelColor}`,
    },
    borderBackgroundColor: {
      border: `${borderWidth}px solid ${theme.palette.background.default}`,
    },
    borderLeftRightDividerColor: {
      borderLeft: `${borderWidth}px solid ${theme.palette.divider}`,
      borderRight: `${borderWidth}px solid ${theme.palette.divider}`,
    },
    borderLeftRightPanelColor: {
      borderLeft: `${borderWidth}px solid ${panelColor}`,
      borderRight: `${borderWidth}px solid ${panelColor}`,
    },
  } as const);

const growthTableStyle = (theme: Theme) =>
  ({
    growthTable: {
      color: 'white',
      backgroundColor: panelColor,
      padding: 0,
      margin: 0,
    },
    growthTableCheckboxColumnIndicator: {
      color: theme.palette.primary.main,
      backgroundColor: theme.palette.divider,
      borderTop: `1px solid ${theme.palette.background.paper}`,
      boxShadow: `0px 1px 35px 0px ${theme.palette.background.default} inset`,
      textAlign: 'center',
      height: 28,
      width: 28,
    },
    growthTableIdColumnIndicator: {
      pointerEvents: 'none',
      color: theme.palette.primary.main,
      fontWeight: 600,
      backgroundColor: theme.palette.divider,
      borderColor: theme.palette.background.paper,
      boxShadow: `0px 1px 35px 0px ${theme.palette.background.default} inset`,
      textAlign: 'left',
      height: 28,
      width: 180,
    },
    growthTableNumberColumnIndicator: {
      pointerEvents: 'none',
      color: theme.palette.primary.main,
      fontWeight: 600,
      backgroundColor: theme.palette.divider,
      borderColor: theme.palette.background.paper,
      boxShadow: `0px 1px 35px 0px ${theme.palette.background.default} inset`,
      textAlign: 'center',
      width: 60,
      height: 28,
    },
    growthSubTableIndicator: {
      pointerEvents: 'none',
      color: theme.palette.primary.main,
      fontWeight: 600,
      backgroundColor: theme.palette.divider,
      borderColor: theme.palette.background.paper,
      boxShadow: `0px 1px 35px 0px ${theme.palette.background.default} inset`,
      textAlign: 'center',
      width: 120,
      height: 28,
    },
    growthTableIdColumn: {
      pointerEvents: 'none',
      color: 'white',
      backgroundColor: theme.palette.divider,
      borderColor: theme.palette.background.paper,
      boxShadow: `0px 1px 30px 0px ${theme.palette.background.default} inset`,
      textAlign: 'left',
      height: 28,
      width: 180,
    },
    growthTableCheckboxColumn: {
      color: 'white',
      backgroundColor: theme.palette.divider,
      borderTop: `1px solid ${theme.palette.background.paper}`,
      boxShadow: `0px 1px 30px 0px ${theme.palette.background.default} inset`,
      textAlign: 'left',
      height: 28,
      width: 28,
    },
    growthTableIdColumnNoShadow: {
      pointerEvents: 'none',
      color: 'white',
      backgroundColor: theme.palette.divider,
      borderColor: theme.palette.background.paper,
      textAlign: 'left',
      height: 28,
      width: 180,
    },
    growthTableIdColumnFirst: {
      pointerEvents: 'none',
      color: 'white',
      backgroundColor: theme.palette.divider,
      borderTopColor: theme.palette.background.paper,
      borderRightColor: theme.palette.background.paper,
      borderLeftColor: theme.palette.background.paper,
      borderBottomColor: theme.palette.divider,
      textAlign: 'left',
      height: 28,
      width: 180,
    },
    growthTableIdColumnMiddle: {
      pointerEvents: 'none',
      color: 'white',
      backgroundColor: theme.palette.divider,
      borderTopColor: theme.palette.divider,
      borderRightColor: theme.palette.background.paper,
      borderLeftColor: theme.palette.background.paper,
      borderBottomColor: theme.palette.divider,
      textAlign: 'left',
      height: 28,
      width: 180,
    },
    growthTableIdColumnLast: {
      pointerEvents: 'none',
      color: 'white',
      backgroundColor: theme.palette.divider,
      borderTopColor: theme.palette.divider,
      borderRightColor: theme.palette.background.paper,
      borderLeftColor: theme.palette.background.paper,
      borderBottomColor: theme.palette.background.paper,
      textAlign: 'left',
      height: 28,
      width: 180,
    },
    growthTableNumberColumn: {
      color: '#ebebeb',
      backgroundColor: panelColor,
      borderColor: panelColor,
      borderRightColor: theme.palette.background.paper,
      textAlign: 'center',
      width: 60,
      height: 28,
    },
    growthSubTableColumn: {
      color: '#ebebeb',
      backgroundColor: panelColor,
      borderColor: panelColor,
      borderRightColor: theme.palette.background.paper,
      textAlign: 'center',
      width: 120,
      height: 28,
    },
    growthTableNumberColumnEditor: {
      color: 'white',
      backgroundColor: panelColor,
      borderColor: panelColor,
      borderRightColor: theme.palette.background.paper,
      textAlign: 'center',
      width: '100%',
      height: 28 - 4,
    },
    nonPointerEvents: { pointerEvents: 'none' },
  } as const);

const dialogStyle = () =>
  ({
    dialogTitleWindow: { borderRadius: '13px 13px 0px 0px' },
    dialogActionWindow: {
      boxShadow: '0px 2px 12px 0px #1b1b1b inset',
      borderRadius: '0px 0px 13px 13px',
    },
  } as const);

/**
 * Integrated style function.
 */
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    ...rootStyle(theme),
    ...tabListStyle(theme),
    ...tooltipStyle(theme),
    ...backgroundStyle(theme),
    ...shadowStyle(theme),
    ...borderStyle(theme),
    ...growthTableStyle(theme),
    ...dialogStyle(),
    monitorPropertyGroup: {
      borderRadius: 10,
      margin: `${tagMarginColumn}px 0px`,
      padding: `${tagMarginColumn}px 0px 0px 0px`,
      border: `1px solid ${theme.palette.background.default}`,
      backgroundColor: panelColor,
      width: formControlWidth,
    },
    groupedButton: {
      backgroundColor: highlightedPanelColor,
      marginBottom: 5,
    },
    formWidthPaperContainer: {
      width: formControlWidth + formControlSideMargin * 2,
      display: 'flex',
      flexDirection: 'column',
      height: `100%`,
      padding: 0,
      alignItems: 'center',
      justifyContent: 'center',
    },
    formWidthPaperContainerAlighnTop: {
      width: formControlWidth + formControlSideMargin * 2,
      display: 'flex',
      flexDirection: 'column',
      height: `100%`,
      padding: 0,
      alignItems: 'center',
    },
    paperContainerUnderWMWindow: {
      height: `calc(100% - 128px)`,
      overflow: 'scroll',
      flexDirection: 'row',
      flexWrap: 'wrap',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    paperColumn: {
      margin: `0px ${formControlSideMargin}px`,
      padding: 0,
      alignItems: 'center',
      justifyContent: 'center',
      width: formControlWidth,
      minHeight: 0,
    },
    tagMarginColumn: {
      height: tagMarginColumn,
    },
    dummyCard: {
      margin: `${tagMarginColumn}px 0px`,
      width: formControlWidth,
      height: formContentHeight + (1 + 2.5) * 2,
      backgroundColor: 'transparent',
    },
    tagCardHalf: {
      margin: `${tagMarginColumn}px 0px`,
      padding: '2.5px 2.5px 2.5px 0px',
      textAlign: 'center',
      borderRadius: 10,
      width: (formControlWidth - formControlSideMargin) / 2,
    },
    tagCard: {
      margin: `${tagMarginColumn}px 0px`,
      padding: '2.5px 2.5px 2.5px 0px',
      textAlign: 'center',
      borderRadius: 10,
      width: formControlWidth,
    },
    tagCardInGroup: {
      margin: `${tagMarginColumnInGroup}px 0px`,
      padding: '2.5px 2.5px 2.5px 0px',
      textAlign: 'center',
      borderRadius: 10,
      backgroundColor: highlightedPanelColor,
    },
    tagCardInGroupTransparent: {
      margin: `${tagMarginColumnInGroup}px 0px`,
      padding: '2.5px 2.5px 2.5px 0px',
      textAlign: 'center',
      borderRadius: 10,
      backgroundColor: 'transparent',
    },
    formControlHalf: {
      margin: '0px 0px 3px 0px',
      padding: `0px ${formControlSidePadding}px 0px ${formControlSidePadding}px`,
      width: formControlWidth / 2,
    },
    formControlHalfAutocomplete: {
      margin: '0px 44px 3px 44px',
      padding: `0px ${formControlSidePadding}px 0px ${formControlSidePadding}px`,
      width: formControlWidth / 2 + 2,
    },
    formControl: {
      margin: '0px 0px 3px 0px',
      padding: `0px ${formControlSidePadding}px 0px ${formControlSidePadding}px`,
      width: formControlWidth,
    },
    inputLabel: {
      margin: `0px 0px 0px ${formControlSidePadding}px`,
      fontSize: fontSize.default,
    },
    autocompleteOption: {
      padding: 0,
    },
    underline: {
      '&:before': {
        borderBottom: `0px solid ${theme.palette.divider}`,
      },
      '&:after': {
        borderBottom: `1px solid ${theme.palette.primary.main}`,
      },
      [`&:hover:not(.${inputClasses.disabled}):before`]: {
        borderBottom: `1px solid #cccccc`,
      },
    },
    defaultFontSize: {
      fontSize: fontSize.default,
    },
    sortFilterTabIndicator: {
      width: 0,
      height: 0,
    },
    roundedComponent: {
      borderRadius: 10,
    },
    imageGrayScale: { filter: 'grayscale(1)' },
    adornmentBaseColor: {
      color: adornmentColor,
    },
    alternativeSDdisplay: {
      margin: 0,
      padding: 0,
      textAlign: 'center',
      alignContent: 'center',
      width: formControlWidth + formControlSideMargin * 2,
      height: dummySDdisplayHeight,
      backgroundColor: theme.palette.background.paper,
    },
    weaponMasterWindow: {
      height: weaponMasterWindowHeight - 3,
      margin: '0px 2px -1px 0px',
      backgroundColor: theme.palette.background.paper,
    },
    flavorTextWindow: {
      height: 66,
      margin: '1px 3px 0px 0px',
      backgroundColor: theme.palette.background.paper,
    },
    monitorLayerWidth: {
      width: `calc(100vw - ${
        drawerWidth * 2 + listWidth * 2 + formControlWidth + formControlSideMargin * 2 + 2 * 3 - 4
      }px)`,
    },
    monitorSkillWidth: {
      width: `calc(100vw - ${drawerWidth * 2 + listWidth + formControlWidth + formControlSideMargin * 2 + 2 * 2}px)`,
    },
    smallRadioAndCheckbox: {
      '& svg': {
        padding: '1px 0px 1px 0px',
        margin: '0px 0px 0px 0px',
        width: 13,
        height: 13,
      },
    },
  })
);
