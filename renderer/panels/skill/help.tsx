import React from 'react';

import { HelpDialog as _HelpDialog } from '@@panelComponents';

const msgGroups = [
  [`スキルに関する情報を管理・編集するパネルです。`],
  [
    `『ミストトレインガールズ～霧の世界の車窓から～』は合同会社EXNOAおよび株式会社KMSにより開発・運営されるオンラインRPGであり、` +
      `合同会社EXNOAの登録商標です。当ソフトウェアが引用するゲーム関連画像および固有名称等は全て権利者に帰属します。`,
  ],
];

const HelpDialog: React.FC = () => {
  return <_HelpDialog msgGroups={msgGroups} title="スキル管理" />;
};

export default HelpDialog;
