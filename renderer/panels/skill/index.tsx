import { Box } from '@mui/material';
import React from 'react';
import { GiMagicSwirl } from 'react-icons/gi';
import { useDispatch } from 'react-redux';

import { actions, selectors } from '@@core/redux';
import { useSelector } from '@@core/redux';
import { name } from '@@identifier/core/skill';
import { name as panelName } from '@@identifier/ui/skill';
import { ISkill, terms } from '@@interface';
import { AttributeIconInTab, EntityTabList, makeFilterOptions, makeSortOptionsFromKeys } from '@@panelComponents';

import { AddSkillDialog } from './AddSkillDialog';
import HelpDialog from './help';
import { MonitorSkill } from './MonitorSkill';

const skillSortOptions = makeSortOptionsFromKeys(
  [
    {
      legend: '基本情報',
      sortFunctionIds: [
        'id',
        'impactRankNumbered:sp.maxAwaken',
        'range:impactRankNumbered:sp.maxAwaken',
        'targetOption:impactRankNumbered:sp.maxAwaken',
      ],
      labels: ['スキル名', '威力表示', terms.actionRanges.legend, terms.actionTargetOptions.legend],
    },
    {
      legend: '消費ポイント',
      sortFunctionIds: [
        'sp.default:impactRankNumbered:sp.maxAwakenLevel',
        'sp.maxAwakenLevel:sp.default:impactRankNumbered',
        'sp.maxAwaken:impactRankNumbered',
        'rp:sp.maxAwaken:impactRankNumbered',
      ],
      labels: ['初期SP', '最大覚醒数', '最大覚醒時SP', 'RP'],
    },
  ],
  []
);

const baseFilterOptions = makeFilterOptions([
  // { key: ['weapon'], ref: terms.weapons },
  // { key: ['attackType'], ref: terms.attackStatusTags },
  // { key: ['from'], ref: terms.affiliations },
  // { key: ['race'], ref: terms.races },
]);

const Panel: React.FC = () => {
  const dispatch = useDispatch();

  const skillSortFilterProps = useSelector(state => state.present.ui[panelName].sortFilterProps.skills);
  const skills = useSelector(state => selectors[name].skills.custom(state, skillSortFilterProps));
  const skillId = useSelector(state => state.present.ui[panelName].selectedId.skill);
  const skillIdBuffer = useSelector(state => state.present.ui[panelName].selectedIdBuffer.skill);
  const selectedSkill = skillId ? skills.entities[skillId] : null;

  const setSkillId = (id: string | false) => {
    dispatch(actions[panelName].setSkillId(id));
  };

  return (
    <Box
      style={{
        display: 'flex',
        flexDirection: 'row',
        overflow: 'hidden',
        height: '100vh',
      }}>
      <EntityTabList
        entities={skills}
        entityId={skillId}
        entityIdBuffer={skillIdBuffer}
        setEntityId={setSkillId}
        setEntityIdBuffer={(id: string | false) => dispatch(actions[panelName].setSkillIdBuffer(id))}
        sortFilterProps={skillSortFilterProps}
        setSortFilterPropsFunc={actions[panelName].setSkillSortFilterProps}
        sortOptions={skillSortOptions}
        filterOptions={baseFilterOptions}
        AddEntityDialog={AddSkillDialog}
        deleteEntity={() => {
          if (skillId) {
            setSkillId(false);
            dispatch(actions[name].deleteSkill({ id: skillId }));
          }
        }}
        TabIcon={(props: { entity: ISkill }) => <AttributeIconInTab attributes={props.entity.attributes} />}
        resolveEntityIdMismatch
      />
      {skillId ? <MonitorSkill skill={selectedSkill} /> : null}
    </Box>
  );
};

export default {
  name: 'スキル管理',
  Component: Panel,
  Icon: function Icon() {
    return <GiMagicSwirl size={22} />;
  } as React.FC,
  Help: HelpDialog,
};
