import { Box, Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@mui/material';
import clsx from 'clsx';
import React, { useState } from 'react';
import { GiMagicSwirl } from 'react-icons/gi';
import { useDispatch } from 'react-redux';

import { actions } from '@@core/redux';
import { name } from '@@identifier/core/skill';
import { CustomTextField } from '@@panelComponents';
import { useStyles } from '@@style/home';
import { theme } from '@@style/lib/theme';

const init = { id: '' } as const;

export type AddSkillDialogProps = {
  open: boolean;
  handleClose: () => void;
  setEntityId: (id: string) => void;
};

export const AddSkillDialog: React.FC<AddSkillDialogProps> = props => {
  const classes = useStyles({});
  const dispatch = useDispatch();

  const { open, handleClose, setEntityId } = props;

  const [id, setId] = useState(init.id as string);

  const initialize = () => {
    setId(init.id);
  };

  const addSkill = () => {
    dispatch(actions[name].addSkill({ id }));
    initialize();
  };

  return (
    <Dialog open={open} onClose={handleClose} scroll="paper">
      <DialogTitle
        className={clsx(classes.paperBackground, classes.dialogTitleWindow)}
        style={{ fontSize: 13, color: theme.palette.primary.main, paddingBottom: 15 }}>
        <GiMagicSwirl style={{ fontSize: 16, margin: '0px 11px -2.5px 10px' }} />
        {'新規スキル登録'}
      </DialogTitle>
      <Box className={classes.paperBackground}>
        <DialogContent dividers style={{ borderRadius: 50, boxShadow: theme.shadows[24] }}>
          <Box flexDirection="row" display="flex">
            <Box
              display="flex"
              flexDirection="column"
              style={{ alignItems: 'center', justifyContent: 'center' }}
              width={300}
              height={100}>
              <Box className={classes.paperColumn}>
                <CustomTextField label="スキル名" value={id} setState={value => setId(value)} panelColor autoFocus />
              </Box>
            </Box>
          </Box>
        </DialogContent>
      </Box>
      <DialogActions className={clsx(classes.paperBackground, classes.dialogActionWindow)}>
        <Button onClick={handleClose} color="primary">
          Close
        </Button>
        <Button
          onClick={() => {
            addSkill();
            setEntityId(id);
            handleClose();
          }}
          color="primary">
          Register
        </Button>
      </DialogActions>
    </Dialog>
  );
};
