import { Box, Checkbox, FormControlLabel, Typography } from '@mui/material';
import clsx from 'clsx';
import React, { useState } from 'react';
import { GiMagicSwirl } from 'react-icons/gi';
import { ImLink } from 'react-icons/im';
import { useDispatch } from 'react-redux';

import { actions, selectors, useSelector } from '@@core/redux';
import { name } from '@@identifier/core/skill';
import { ISkill, terms } from '@@interface';
import {
  BadgeNumberComplete,
  CustomAutoComplete,
  CustomNumberComplete,
  CustomTextField,
  MonitorPropertyGroup,
  SwitchableChipList,
  TagCard,
  TagCardContainer,
} from '@@panelComponents';
import { AttackBlock, TargetSelectBlock } from '@@panelComponents/ActionPieceBlock';
import { useStyles } from '@@style/home';
import { theme } from '@@style/lib/theme';

const tabMenu = [
  {
    value: 'skill',
    Icon: function Icon() {
      return <GiMagicSwirl fontSize={15} />;
    },
    label: '行動内容設定',
  },
  {
    value: 'link',
    Icon: function Icon() {
      return <ImLink fontSize={15} />;
    },
    label: 'リンク内容設定',
  },
];

type MonitorSkillProps = {
  skill: ISkill;
};
export const MonitorSkill: React.FC<MonitorSkillProps> = props => {
  const classes = useStyles({});
  const dispatch = useDispatch();
  const { skill } = props;
  const { id, rp, sp, impactRank, attackStatus, range, targetOption, description, actionPieces, fast, delay } = skill;

  /** Search which base characters have the selected skill. */
  const bases = useSelector(state => selectors.character.bases.custom(state, { sortFunctionId: 'id' }));
  const basesFiltered = bases.ids.filter(baseId => Object.keys(bases.entities[baseId].skillLevels).includes(id));

  const [tabId, setTabId] = useState('skill');

  return (
    <>
      {/* Basic Information*/}
      <Box
        style={{
          margin: '0px 2px 0px 0px',
          padding: 0,
        }}
        bgcolor={theme.palette.background.paper}>
        <Box className={classes.formWidthPaperContainer}>
          <Box
            className={clsx(classes.paperColumn, classes.paperBackground)}
            style={{
              overflowY: 'scroll',
              overflowX: 'hidden',
            }}>
            <MonitorPropertyGroup label={null}>
              <CustomNumberComplete
                minValue={1}
                maxValue={16}
                step={1}
                value={sp.default}
                label="初期SP・最大覚醒数"
                inGroup
                setState={value => {
                  dispatch(actions.metaFlags.stampState({}));
                  dispatch(actions[name].updateSkillField({ id, keys: ['sp', 'default'], value: Number(value) }));
                }}
                badge={
                  <BadgeNumberComplete
                    value={sp.maxAwakenLevel}
                    maxValue={3}
                    minValue={1}
                    step={1}
                    label={null}
                    setState={value => {
                      dispatch(actions.metaFlags.stampState({}));
                      dispatch(
                        actions[name].updateSkillField({ id, keys: ['sp', 'maxAwakenLevel'], value: Number(value) })
                      );
                    }}
                  />
                }
              />
              <CustomNumberComplete
                minValue={0}
                maxValue={2}
                step={1}
                value={rp}
                label="RP"
                inGroup
                setState={value => {
                  dispatch(actions.metaFlags.stampState({}));
                  dispatch(actions[name].updateSkillField({ id, keys: ['rp'], value: Number(value) }));
                }}
              />
              <Box flexDirection="row" display="flex">
                <CustomAutoComplete
                  options={[...terms.actionRanges.values]}
                  value={range}
                  label={terms.actionRanges.legend}
                  setState={value => {
                    dispatch(actions.metaFlags.stampState({}));
                    dispatch(actions[name].updateSkillField({ id, keys: ['range'], value }));
                  }}
                  half
                  inGroup
                />
                <Box style={{ marginLeft: -7 }} />
                <CustomAutoComplete
                  options={[...terms.actionTargetOptions.values]}
                  value={targetOption}
                  label={terms.actionTargetOptions.legend}
                  setState={value => {
                    dispatch(actions.metaFlags.stampState({}));
                    dispatch(actions[name].updateSkillField({ id, keys: ['targetOption'], value }));
                  }}
                  half
                  inGroup
                />
              </Box>
            </MonitorPropertyGroup>
            <CustomAutoComplete
              options={[...terms.impactRanks.values]}
              value={impactRank}
              label={'威力表示'}
              setState={value => {
                dispatch(actions.metaFlags.stampState({}));
                dispatch(actions[name].updateSkillField({ id, keys: ['impactRank'], value }));
              }}
              panelColor
            />

            <TagCardContainer label={' '} panelColor>
              <Box display="flex" flexDirection="column">
                <Typography
                  className={classes.defaultFontSize}
                  style={{ margin: '0px 0px 0px 0px', fontSize: 7.5, textAlign: 'left' }}
                  sx={{ fontWeight: 200, color: '#bbbbbb' }}>
                  {'攻撃参照'}
                </Typography>
                <Box display="flex" flexDirection="row" style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Box flexGrow={5} />
                  <FormControlLabel
                    checked={attackStatus.includes('物攻')}
                    control={<Checkbox className={classes.smallRadioAndCheckbox} />}
                    label={'物攻'}
                    style={{ width: 70 }}
                    classes={{ label: classes.defaultFontSize }}
                    disabled
                  />
                  <FormControlLabel
                    checked={attackStatus.includes('魔攻')}
                    control={<Checkbox className={classes.smallRadioAndCheckbox} />}
                    label={'魔攻'}
                    style={{ width: 70 }}
                    classes={{ label: classes.defaultFontSize }}
                    disabled
                  />
                  <Box flexGrow={1} />
                </Box>
              </Box>
            </TagCardContainer>

            <TagCardContainer label={' '} panelColor>
              <Box display="flex" flexDirection="column">
                <Typography
                  className={classes.defaultFontSize}
                  style={{ margin: '0px 0px 0px 0px', fontSize: 7.5, textAlign: 'left' }}
                  sx={{ fontWeight: 200, color: '#bbbbbb' }}>
                  {'行動順'}
                </Typography>
                <Box display="flex" flexDirection="row" style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Box flexGrow={5} />
                  <FormControlLabel
                    checked={fast ?? false}
                    control={
                      <Checkbox
                        className={classes.smallRadioAndCheckbox}
                        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                          dispatch(actions.metaFlags.stampState({}));
                          dispatch(actions[name].updateActionOrder({ id, fast: event.target.checked }));
                        }}
                      />
                    }
                    label={'最速'}
                    style={{ width: 70 }}
                    classes={{ label: classes.defaultFontSize }}
                  />
                  <FormControlLabel
                    checked={delay ?? false}
                    control={
                      <Checkbox
                        className={classes.smallRadioAndCheckbox}
                        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                          dispatch(actions.metaFlags.stampState({}));
                          dispatch(actions[name].updateActionOrder({ id, delay: event.target.checked }));
                        }}
                      />
                    }
                    label={'最遅'}
                    style={{ width: 70 }}
                    classes={{ label: classes.defaultFontSize }}
                  />
                  <Box flexGrow={1} />
                </Box>
              </Box>
            </TagCardContainer>

            <Box height={115}>
              <CustomTextField
                label="説明"
                value={description}
                setState={value => {
                  dispatch(actions.metaFlags.stampState({}));
                  dispatch(
                    actions[name].updateSkillField({
                      id,
                      keys: ['description'],
                      value: value
                        .replace(/\r?\n/g, '　')
                        .replace('、　', '、')
                        .replace('　　', '　')
                        .replace('――', '──'),
                    })
                  );
                }}
                multiline
                maxRows={5}
                panelColor
              />
            </Box>
            <Box height={250}>
              <MonitorPropertyGroup label="習得キャラクター" maxHeight={218}>
                <Box height={10} />
                {basesFiltered.map(baseFiltered => (
                  <Box key={baseFiltered} margin="-30px 0px 0px 0px">
                    <TagCard value={baseFiltered} label={null} inGroup transparent textAlign="left" contentHalf />
                  </Box>
                ))}
              </MonitorPropertyGroup>
            </Box>
          </Box>
        </Box>
      </Box>

      {/* Detailed Information */}
      <Box className={classes.monitorSkillWidth} display="flex" flexDirection="column">
        <SwitchableChipList
          tabId={tabId}
          tabMenu={tabMenu}
          onChangeTabList={(event, id) => {
            setTabId(id);
          }}
          orientation="horizontal"
        />
        <Box
          style={{
            margin: '0px 2px 0px 0px',
            padding: 0,
            height: '100%',
            width: '100%',
            alignItems: 'center',
            justifyContent: 'center',
          }}
          display="flex"
          flexDirection="column"
          bgcolor={theme.palette.background.paper}>
          {(() => {
            switch (tabId) {
              case 'skill':
                return (
                  <Box
                    className={clsx(classes.insideShadow, classes.borderBackgroundColor)}
                    display="flex"
                    flexDirection="column"
                    style={{
                      borderRadius: 10,
                      height: `calc(100vh - 128px)`,
                      width: `calc(100vw - 490px)`,
                      overflowX: 'scroll',
                      overflowY: 'hidden',
                    }}>
                    {/* Stack Blocks */}
                    {actionPieces.map((actionPiece, i) => {
                      switch (actionPiece.type) {
                        case 'attack':
                          return (
                            <AttackBlock
                              key={i}
                              index={i}
                              impact={actionPiece.impact}
                              hundleDeleteBlock={() => {
                                dispatch(
                                  actions[name].updateSkillField({
                                    id,
                                    keys: ['actionPieces', i],
                                    value: undefined,
                                    option: { deleteArrayElement: true },
                                  })
                                );
                              }}
                              hundleAddReferenceElementHolder={(referenceElm, j) => {
                                dispatch(
                                  actions[name].updateSkillField({
                                    id,
                                    keys: [
                                      'actionPieces',
                                      i,
                                      'impact',
                                      j,
                                      'reference',
                                      actionPiece.impact[j].reference.length,
                                    ],
                                    value: referenceElm,
                                  })
                                );
                              }}
                              hundleDeleteReferenceElementHolder={(j, k) => {
                                if (actionPiece.impact[j].reference.length === 1) {
                                  if (actionPiece.impact.length === 1) {
                                    dispatch(
                                      actions[name].updateSkillField({
                                        id,
                                        keys: ['actionPieces', i],
                                        value: undefined,
                                        option: { deleteArrayElement: true },
                                      })
                                    );
                                  } else {
                                    dispatch(
                                      actions[name].updateSkillField({
                                        id,
                                        keys: ['actionPieces', i, 'impact', j],
                                        value: undefined,
                                        option: { deleteArrayElement: true },
                                      })
                                    );
                                  }
                                } else {
                                  dispatch(
                                    actions[name].updateSkillField({
                                      id,
                                      keys: ['actionPieces', i, 'impact', j, 'reference', k],
                                      value: undefined,
                                      option: { deleteArrayElement: true },
                                    })
                                  );
                                }
                              }}
                              hundleAddImpactHolder={impactElm => {
                                dispatch(
                                  actions[name].updateSkillField({
                                    id,
                                    keys: ['actionPieces', i, 'impact', actionPiece.impact.length],
                                    value: impactElm,
                                  })
                                );
                              }}
                              onChangeImpactValue={(value, j) => {
                                dispatch(
                                  actions[name].updateSkillField({
                                    id,
                                    keys: ['actionPieces', i, 'impact', j, 'value'],
                                    value,
                                  })
                                );
                              }}
                              onChangeAttribute={(value, j, k) => {
                                dispatch(
                                  actions[name].updateSkillField({
                                    id,
                                    keys: ['actionPieces', i, 'impact', j, 'reference', k, 'attribute'],
                                    value,
                                  })
                                );
                              }}
                              onChangeAttackStatus={(value, j, k) => {
                                dispatch(
                                  actions[name].updateSkillField({
                                    id,
                                    keys: ['actionPieces', i, 'impact', j, 'reference', k, 'status'],
                                    value,
                                  })
                                );
                              }}
                            />
                          );
                        case 'targetSelect':
                          const { range, targetOption } = actionPiece;
                          return (
                            <TargetSelectBlock
                              key={i}
                              index={i}
                              range={range}
                              targetOption={targetOption}
                              hundleDeleteBlock={() => {
                                dispatch(
                                  actions[name].updateSkillField({
                                    id,
                                    keys: ['actionPieces', i],
                                    value: undefined,
                                    option: { deleteArrayElement: true },
                                  })
                                );
                              }}
                              onChangeRange={value => {
                                dispatch(
                                  actions[name].updateSkillField({ id, keys: ['actionPieces', i, 'range'], value })
                                );
                              }}
                              onChangeTargetOption={value => {
                                dispatch(
                                  actions[name].updateSkillField({
                                    id,
                                    keys: ['actionPieces', i, 'targetOption'],
                                    value,
                                  })
                                );
                              }}
                            />
                          );
                      }
                    })}
                  </Box>
                );
            }
          })()}
        </Box>
      </Box>
    </>
  );
};
