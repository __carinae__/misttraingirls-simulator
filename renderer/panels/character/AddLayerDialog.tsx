import { Box, Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@mui/material';
import clsx from 'clsx';
import React, { useEffect, useState } from 'react';
import { BsPersonPlusFill } from 'react-icons/bs';
import { useDispatch } from 'react-redux';

import { actions } from '@@core/redux';
import { name } from '@@identifier/core/character';
import { AttackStatusTag, LayerType, Rarity, StatusTag, terms, Weapon } from '@@interface';
import { CustomAutoComplete, CustomTextField, MonitorPropertyGroup, TagCard } from '@@panelComponents';
import { useStyles } from '@@style/home';
import { theme } from '@@style/lib/theme';

const dialogWidth = 625;
const init = {
  id: '',
  rarity: 'SS',
  type: 'アタッカー',
  flavorText: '',
} as const;

const getDefaultAttackType = (weapon: Weapon): AttackStatusTag => {
  if (weapon === '杖' || weapon === '魔具') {
    return '魔攻';
  } else {
    return '物攻';
  }
};

const getDefaultGearLevelStatusBonus = (
  type: LayerType,
  attackType: AttackStatusTag
): { alpha: StatusTag; beta: StatusTag } => {
  switch (type) {
    case 'アタッカー':
      return { alpha: attackType, beta: '速さ' };
    case 'ディフェンダー':
      if (attackType === '魔攻') {
        return { alpha: '魔防', beta: '物防' };
      } else {
        return { alpha: '物防', beta: '魔防' };
      }
    case 'サポーター':
      return { alpha: '幸運', beta: '速さ' };
    case 'トリックスター':
      return { alpha: '速さ', beta: '幸運' };
  }
};

export type AddLayerDialogProps = {
  open: boolean;
  handleClose: () => void;
  extra: { baseId: string; weapon: Weapon };
  setEntityId: (id: string, ...args: unknown[]) => void;
};

export const AddLayerDialog: React.FC<AddLayerDialogProps> = props => {
  const classes = useStyles({});
  const dispatch = useDispatch();

  const {
    open,
    handleClose,
    extra: { baseId, weapon },
    setEntityId,
  } = props;

  const [id, setId] = useState(init.id as string);
  const [rarity, setRarity] = useState(init.rarity as Rarity);
  const [type, setType] = useState(init.type as LayerType);
  const [attackType, setAttackType] = useState(getDefaultAttackType(weapon));
  const [layerLevelAttackType, setLayerLevelAttackType] = useState(attackType);
  const [gearLevelStatusBonus, setGearLevelStatusBonus] = useState(getDefaultGearLevelStatusBonus(type, attackType));
  const [flavorText, setFlavorText] = useState(init.flavorText as string);

  const initialize = () => {
    setId(init.id);
    setRarity(init.rarity);
    setType(init.type);
    setLayerLevelAttackType(attackType);
    setGearLevelStatusBonus(getDefaultGearLevelStatusBonus(type, attackType));
    setFlavorText(init.flavorText);
  };

  const addLayer = () => {
    dispatch(
      actions[name].addLayer({
        id,
        rarity,
        type,
        attackType,
        layerLevelAttackType,
        gearLevelStatusBonus,
        flavorText,
        baseId,
      })
    );
    initialize();
  };

  useEffect(() => {
    initialize();
    setAttackType(getDefaultAttackType(weapon));
  }, []);

  useEffect(() => {
    setLayerLevelAttackType(attackType);
    setGearLevelStatusBonus(getDefaultGearLevelStatusBonus(type, attackType));
  }, [type, attackType]);

  return (
    <Dialog open={baseId && open} onClose={handleClose} scroll="paper" maxWidth="lg">
      <DialogTitle
        className={clsx(classes.paperBackground, classes.dialogTitleWindow)}
        style={{
          fontSize: 13,
          color: theme.palette.primary.main,
          paddingBottom: 15,
        }}>
        <BsPersonPlusFill style={{ fontSize: 15, margin: '0px 11px -2.5px 10px' }} />
        {'新規レイヤー登録'}
      </DialogTitle>
      <Box className={classes.paperBackground} style={{ width: dialogWidth }}>
        <DialogContent dividers style={{ borderRadius: 50, boxShadow: theme.shadows[24] }}>
          <Box flexDirection="row" display="flex">
            <Box className={classes.paperColumn}>
              <TagCard label="ベース名" value={baseId} transparent highlight />
              <MonitorPropertyGroup label={'　'}>
                <CustomAutoComplete
                  options={[...terms.layerTypes.values]}
                  value={type}
                  label={terms.layerTypes.legend}
                  setState={value => setType(value as LayerType)}
                  contentHalf
                  transparent
                  inGroup
                />
                <CustomAutoComplete
                  options={[...terms.rarities.values]}
                  value={rarity}
                  label={terms.rarities.legend}
                  setState={value => setRarity(value as Rarity)}
                  contentHalf
                  transparent
                  inGroup
                />
                <CustomAutoComplete
                  options={[...terms.attackStatusTags.values]}
                  value={attackType}
                  label="通常攻撃参照"
                  setState={value => setAttackType(value as AttackStatusTag)}
                  contentHalf
                  inGroup
                />
              </MonitorPropertyGroup>
            </Box>
            <Box className={classes.paperColumn}>
              <CustomTextField label="レイヤー名" value={id} setState={value => setId(value)} panelColor autoFocus />
              <MonitorPropertyGroup label={'ステータスボーナス'}>
                <CustomAutoComplete
                  options={[...terms.attackStatusTags.values]}
                  value={layerLevelAttackType}
                  label="レベル強化 攻撃参照"
                  setState={value => setLayerLevelAttackType(value as AttackStatusTag)}
                  contentHalf
                  inGroup
                  transparent
                />
                <CustomAutoComplete
                  options={[...terms.statusTags.values]}
                  value={gearLevelStatusBonus.alpha}
                  label="ギアレベル強化 α"
                  setState={value =>
                    setGearLevelStatusBonus({ alpha: value as StatusTag, beta: gearLevelStatusBonus.beta })
                  }
                  contentHalf
                  inGroup
                  transparent
                />
                <CustomAutoComplete
                  options={[...terms.statusTags.values]}
                  value={gearLevelStatusBonus.beta}
                  label="ギアレベル強化 β"
                  setState={value =>
                    setGearLevelStatusBonus({ beta: value as StatusTag, alpha: gearLevelStatusBonus.alpha })
                  }
                  contentHalf
                  inGroup
                  transparent
                />
              </MonitorPropertyGroup>
            </Box>
            <Box className={classes.paperColumn} display="flex" flexDirection="column">
              <CustomTextField
                label="FLAVOR TEXT"
                value={flavorText}
                setState={value => {
                  setFlavorText(value.replace(/\r?\n/g, '　').replace('　　', '　').replace('――', '──'));
                }}
                multiline
                maxRows={14}
                panelColor
              />
            </Box>
          </Box>
        </DialogContent>
      </Box>
      <DialogActions className={clsx(classes.paperBackground, classes.dialogActionWindow)}>
        <Button onClick={handleClose} color="primary">
          Close
        </Button>
        <Button
          onClick={() => {
            addLayer();
            setEntityId(id, baseId);
            handleClose();
          }}
          color="primary">
          Register
        </Button>
      </DialogActions>
    </Dialog>
  );
};
