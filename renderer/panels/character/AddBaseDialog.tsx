import { Box, Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@mui/material';
import clsx from 'clsx';
import React, { useState } from 'react';
import { BsPersonPlusFill } from 'react-icons/bs';
import { useDispatch } from 'react-redux';

import { actions, useSelector } from '@@core/redux';
import { name } from '@@identifier/core/character';
import { Affiliation, Race, terms, Weapon } from '@@interface';
import { CustomAutoComplete, CustomTextField } from '@@panelComponents';
import { useStyles } from '@@style/home';
import { theme } from '@@style/lib/theme';

const init = { id: '', weapon: '剣', from: 'セントイリス', race: '人間' } as const;

export type AddBaseDialogProps = {
  open: boolean;
  handleClose: () => void;
  setEntityId: (id: string) => void;
};

export const AddBaseDialog: React.FC<AddBaseDialogProps> = props => {
  const classes = useStyles({});
  const dispatch = useDispatch();

  const { open, handleClose, setEntityId } = props;
  const { maxHP } = useSelector(state => state.present.core[name].nonEntityFields);

  const [id, setId] = useState(init.id as string);
  const [weapon, setWeapon] = useState(init.weapon as Weapon);
  const [from, setFrom] = useState(init.from as Affiliation);
  const [race, setRace] = useState(init.race as Race);

  const initialize = () => {
    setId(init.id);
    setWeapon(init.weapon);
    setFrom(init.from);
    setRace(init.race);
  };

  const addBase = () => {
    dispatch(actions[name].addBase({ id, weapon, from, race, hp: maxHP }));
    initialize();
  };

  return (
    <Dialog open={open} onClose={handleClose} scroll="paper">
      <DialogTitle
        className={clsx(classes.paperBackground, classes.dialogTitleWindow)}
        style={{ fontSize: 13, color: theme.palette.primary.main, paddingBottom: 15 }}>
        <BsPersonPlusFill style={{ fontSize: 15, margin: '0px 11px -2.5px 10px' }} />
        {'新規ベースキャラクター登録'}
      </DialogTitle>
      <Box className={classes.paperBackground}>
        <DialogContent dividers style={{ borderRadius: 50, boxShadow: theme.shadows[24] }}>
          <Box flexDirection="row" display="flex">
            <Box className={classes.paperColumn} display="flex" flexDirection="column">
              <CustomTextField label="ベース名" value={id} setState={value => setId(value)} panelColor autoFocus />
              <CustomAutoComplete
                options={[...terms.weapons.values]}
                value={weapon}
                label={terms.weapons.legend}
                setState={value => setWeapon(value as Weapon)}
              />
            </Box>
            <Box className={classes.paperColumn}>
              <CustomAutoComplete
                options={[...terms.affiliations.values]}
                value={from}
                label={terms.affiliations.legend}
                setState={value => setFrom(value as Affiliation)}
                panelColor
              />
              <CustomAutoComplete
                options={[...terms.races.values]}
                value={race}
                label={terms.races.legend}
                setState={value => setRace(value as Race)}
                panelColor
              />
            </Box>
          </Box>
        </DialogContent>
      </Box>
      <DialogActions className={clsx(classes.paperBackground, classes.dialogActionWindow)}>
        <Button onClick={handleClose} color="primary">
          Close
        </Button>
        <Button
          onClick={() => {
            addBase();
            setEntityId(id);
            handleClose();
          }}
          color="primary">
          Register
        </Button>
      </DialogActions>
    </Dialog>
  );
};
