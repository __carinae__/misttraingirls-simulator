import { Box, Tooltip, Typography } from '@mui/material';
import clsx from 'clsx';
import React from 'react';
import { useDispatch } from 'react-redux';

import { useSelector } from '@@core/redux';
import { actions, selectors } from '@@core/redux';
import { name } from '@@identifier/core/weaponMaster';
import { IWeaponMaster, terms, Weapon } from '@@interface';
import { NumberField } from '@@panelComponents';
import { panelColor, useStyles } from '@@style/home';
import { theme } from '@@style/lib/theme';

const iconSize = 15;
const getColor = (weapon: Weapon, selectedWeapon: Weapon): string => {
  if (weapon === selectedWeapon) {
    return theme.palette.primary.main;
  } else {
    return theme.palette.text.primary;
  }
};

type WeaponMasterTagProps = {
  weapon: Weapon;
  selectedWeapon: Weapon;
  wms: Record<string, IWeaponMaster>;
};
const WeaponMasterTag: React.FC<WeaponMasterTagProps> = props => {
  const { weapon, selectedWeapon, wms } = props;
  const classes = useStyles({});
  const dispatch = useDispatch();

  return (
    <Box
      key={weapon}
      style={{
        flexGrow: 1,
        flexDirection: 'row',
        display: 'flex',
        marginRight: -10,
        overflow: 'hidden',
      }}>
      <NumberField
        transparent
        value={wms[weapon].level}
        label={null}
        half
        disableLabel
        startAdornment={
          <Tooltip
            title={
              <Box>
                <Box style={{ textAlign: 'center' }} display="flex" flexDirection="row">
                  <Box style={{ textAlign: 'left' }}>{'SKILL : '}</Box>
                  <Box
                    style={{
                      textAlign: 'right',
                      fontSize: 12,
                      margin: '-2px 5px 0px 5px',
                      color: theme.palette.primary.main,
                    }}
                    flexGrow={1}>
                    {wms[weapon]?.condition?.skill}
                  </Box>
                  <Box style={{ textAlign: 'right' }}>{'%'}</Box>
                </Box>
                <Box style={{ textAlign: 'center' }} display="flex" flexDirection="row">
                  <Box style={{ textAlign: 'left' }}>{'TRANS : '}</Box>
                  <Box
                    style={{
                      textAlign: 'right',
                      fontSize: 12,
                      margin: '-2px 5px 0px 5px',
                      color: theme.palette.primary.main,
                    }}
                    flexGrow={1}>
                    {wms[weapon]?.condition?.trans}
                  </Box>
                  <Box style={{ textAlign: 'right' }}>{'%'}</Box>
                </Box>
              </Box>
            }
            placement="bottom"
            classes={{ tooltip: classes.weaponMasterTooltip }}>
            <Box
              style={{
                backgroundColor: '#2e2d2e',
                margin: '0px -15px 4px 0px',
                borderRadius: '50%',
                border: `1px solid ${theme.palette.background.default}`,
                width: 32,
                height: 32,
              }}>
              {React.createElement(terms.weapons.Icons[weapon], {
                style: {
                  margin: '8px 0px 0px 1px',
                  fontSize: iconSize,
                  color: getColor(weapon, selectedWeapon),
                },
              })}
            </Box>
          </Tooltip>
        }
        minValue={1}
        inputTypeInteger
        setState={(value: number) => {
          dispatch(actions.metaFlags.stampState({}));
          dispatch(actions[name].updateLevel({ id: weapon, level: value }));
        }}
      />
    </Box>
  );
};

type MonitorWeaponMasterProps = {
  selectedWeapon: Weapon;
};
export const MonitorWeaponMaster: React.FC<MonitorWeaponMasterProps> = props => {
  const { selectedWeapon } = props;
  const classes = useStyles({});
  const wms = useSelector(state => selectors[name].weaponMasters.standard.selectEntities(state));

  return (
    <Box className={classes.weaponMasterWindow}>
      <Box
        style={{
          borderBottomLeftRadius: 26,
          border: `1px solid ${theme.palette.background.default}`,
          width: '100%',
          height: '100%',
          boxShadow: theme.shadows[1],
        }}
        bgcolor={panelColor}>
        <Box
          style={{
            flexDirection: 'row',
            display: 'flex',
            marginTop: -7,
            flexWrap: 'wrap',
            overflowY: 'scroll',
            overflowX: 'hidden',
            height: 65,
          }}>
          {terms.weapons.values.map(weapon => (
            <WeaponMasterTag key={weapon} {...{ weapon, selectedWeapon, wms }} />
          ))}
        </Box>
        <Typography
          className={clsx(classes.inputLabel, classes.defaultFontSize)}
          style={{
            textAlign: 'left',
            margin: '-15px 0px 0px 7px',
            color: 'grey',
            fontWeight: 600,
          }}>
          {'WEAPON MASTER LEVEL'}
        </Typography>
      </Box>
    </Box>
  );
};
