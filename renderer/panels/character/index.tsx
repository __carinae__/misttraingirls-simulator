import { Box } from '@mui/material';
import merge from 'deepmerge';
import React /** , { useEffect } */ from 'react';
import { BsPersonCircle } from 'react-icons/bs';
import { useDispatch } from 'react-redux';

import { Character } from '@@core/domain';
import { actions, selectors } from '@@core/redux';
import { useSelector } from '@@core/redux';
import { name } from '@@identifier/core/character';
import { name as panelName } from '@@identifier/ui/character';
import { terms } from '@@interface';
import {
  BaseRarityIconInTab,
  EntityTabList,
  LayerRarityIconInTab,
  makeFilterOptions,
  makeSortOptionsFromKeys,
} from '@@panelComponents';
import { useStyles } from '@@style/home';

import { AddBaseDialog } from './AddBaseDialog';
import { AddLayerDialog } from './AddLayerDialog';
import HelpDialog from './help';
import { MonitorBase } from './MonitorBase';
import { LayerFlavorText } from './MonitorFlavorText';
import { MonitorLayer } from './MonitorLayer';
import { MonitorWeaponMaster } from './MonitorWeaponMaster';

const baseSortOptions = makeSortOptionsFromKeys(
  [
    {
      legend: '基本情報',
      sortFunctionIds: ['id', 'weapon', 'from', 'race'],
      labels: ['ベース名', terms.weapons.legend, terms.affiliations.legend, terms.races.legend],
    },
  ],
  [
    {
      rootKey: 'status',
      ref: { legend: 'ベース', values: terms.statusTags.values },
    },
  ]
);

const baseFilterOptions = makeFilterOptions([
  { key: ['weapon'], ref: terms.weapons },
  { key: ['from'], ref: terms.affiliations },
  { key: ['race'], ref: terms.races },
]);

const layerSortOptions = makeSortOptionsFromKeys(
  [
    {
      legend: '基本情報',
      sortFunctionIds: [
        'baseId:rarity:index',
        'rarity:baseId:index',
        'type:baseId:rarity:index',
        'attackType:baseId:rarity:index',
      ],
      labels: ['ベース名', terms.rarities.legend, terms.layerTypes.legend, terms.attackStatusTags.legend],
    },
  ],
  [
    {
      rootKey: 'bonus',
      ref: { legend: '加算値', values: terms.statusTags.values },
    },
    {
      rootKey: 'scale',
      ref: { legend: '補正係数', values: terms.statusTags.values },
    },
    {
      rootKey: 'status',
      ref: { legend: 'レイヤーステータス', values: terms.statusTags.values },
    },
    {
      rootKey: 'resistance',
      ref: { legend: '属性耐性', values: terms.attributes.values },
    },
  ]
);

const layerFilterOptions = makeFilterOptions([
  { key: ['rarity'], ref: terms.rarities },
  { key: ['type'], ref: terms.layerTypes },
  { key: ['attackType'], ref: terms.attackStatusTags },
]);

const Panel: React.FC = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const baseSortFilterProps = useSelector(state => state.present.ui[panelName].sortFilterProps.bases);
  const bases = useSelector(state => selectors[name].bases.custom(state, baseSortFilterProps));
  const baseId = useSelector(state => state.present.ui[panelName].selectedId.base);
  const baseIdBuffer = useSelector(state => state.present.ui[panelName].selectedIdBuffer.base);
  const selectedBase = baseId ? bases.entities[baseId] : null;

  const layerSortFilterProps = useSelector(state => state.present.ui[panelName].sortFilterProps.layers);
  const layers = useSelector(state =>
    selectors[name].layers.custom(
      state,
      merge(layerSortFilterProps, {
        filters: {
          baseId: bases.ids.length
            ? Object.fromEntries(bases.ids.map(id => [id, true]))
            : {
                /**
                 * To prevent layers from being undesirably unfiltered when all of the bases disappear by base filter.
                 * This is a simple trick to utilize the fact that there is no layer named 'none' in data.
                 * */
                none: true,
              },
        },
      })
    )
  );
  const layerId = useSelector(state => state.present.ui[panelName].selectedId.layer);
  const layerIdBuffer = useSelector(state => state.present.ui[panelName].selectedIdBuffer.layer);
  const selectedLayer = layerId ? layers.entities[layerId] : null;

  const determineLayerIdfromBaseId = (_baseId: string | false, _layerId: string | false): string | false => {
    if (_baseId && bases.entities.hasOwnProperty(_baseId)) {
      const layersBelonging = Character.getLayersBelonging(bases.entities[_baseId].layerIds);
      if (_layerId && layersBelonging.includes(_layerId) && layers.entities.hasOwnProperty(_layerId)) {
        return _layerId;
      } else {
        const num = layersBelonging.length;
        for (let i = 0; i < num; i++) {
          const _layerIdBelonging = layersBelonging[num - 1 - i];
          if (layers.entities.hasOwnProperty(_layerIdBelonging)) {
            return _layerIdBelonging;
          }
        }
      }
    }
    return false;
  };

  const setBaseId = (id: string | false) => {
    dispatch(
      actions[panelName].selectBaseTab({
        id,
        layerIdLinked: id ? determineLayerIdfromBaseId(id, layerId) : false,
      })
    );
  };
  const setLayerId = (id: string | false, baseId?: string | false) => {
    dispatch(
      actions[panelName].selectLayerTab({
        id,
        baseIdLinked: baseId ?? (id && layers.entities[id] ? layers.entities[id].baseId : false),
      })
    );
  };

  // const { mounted } = useSelector(state => state.present.ui.metaFlags);
  // useEffect(() => {
  //   if (mounted) {
  //     for (const _baseId of bases.ids) {
  //       console.log(_baseId);
  //       const base = bases.entities[_baseId];
  //       const skillLevels = {};
  //       for (const _layerId of Character.getLayersBelonging(base.layerIds)) {
  //         const layer = layers.entities[_layerId];
  //         for (const _skillId of layer.skillIds) {
  //           skillLevels[_skillId] = 99;
  //         }
  //       }
  //       dispatch(actions.character.updateBaseField({ id: _baseId, keys: ['skillLevels'], value: skillLevels }));
  //     }
  //   }
  // }, [mounted]);

  return (
    <Box
      style={{
        display: 'flex',
        flexDirection: 'row',
        overflow: 'hidden',
        height: '100vh',
      }}>
      <EntityTabList
        entities={bases}
        entityId={baseId}
        entityIdBuffer={baseIdBuffer}
        setEntityId={setBaseId}
        setEntityIdBuffer={(id: string | false) => dispatch(actions[panelName].setBaseIdBuffer(id))}
        sortFilterProps={baseSortFilterProps}
        setSortFilterPropsFunc={actions[panelName].setBaseSortFilterProps}
        sortOptions={baseSortOptions}
        filterOptions={baseFilterOptions}
        AddEntityDialog={AddBaseDialog}
        deleteEntity={() => {
          if (baseId) {
            setBaseId(false);
            dispatch(actions[name].deleteBase({ id: baseId }));
          }
        }}
        TabIcon={BaseRarityIconInTab}
        resolveEntityIdMismatch
      />
      <EntityTabList
        entities={layers}
        entityId={layerId}
        entityIdBuffer={layerIdBuffer}
        setEntityId={setLayerId}
        setEntityIdBuffer={(id: string | false) => dispatch(actions[panelName].setLayerIdBuffer(id))}
        sortFilterProps={layerSortFilterProps}
        setSortFilterPropsFunc={actions[panelName].setLayerSortFilterProps}
        sortOptions={layerSortOptions}
        filterOptions={layerFilterOptions}
        AddEntityDialog={AddLayerDialog}
        extraPropsAdd={{ baseId, weapon: selectedBase?.weapon }}
        deleteEntity={() => {
          if (layerId) {
            setLayerId(false);
            dispatch(actions[name].deleteLayer({ id: layerId }));
          }
        }}
        TabIcon={LayerRarityIconInTab}
        resolveEntityIdMismatch={false}
      />

      <Box style={{ flexGrow: 1, flexDirection: 'column' }} className={classes.monitorLayerWidth}>
        {baseId ? <MonitorWeaponMaster selectedWeapon={selectedBase.weapon} /> : null}
        {baseId && layerId ? <LayerFlavorText text={selectedLayer.flavorText} /> : null}
        {baseId && layerId ? <MonitorLayer base={selectedBase} layer={selectedLayer} /> : null}
      </Box>
      {baseId ? <MonitorBase base={selectedBase} layer={selectedLayer} /> : null}
    </Box>
  );
};

export default {
  name: 'キャラクター管理',
  Component: Panel,
  Icon: function Icon() {
    return <BsPersonCircle size={18} />;
  } as React.FC,
  Help: HelpDialog,
};
