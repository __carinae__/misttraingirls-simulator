import { Box, IconButton } from '@mui/material';
import clsx from 'clsx';
import React, { useState } from 'react';
import { FaLock, FaUnlockAlt } from 'react-icons/fa';
import { VscError } from 'react-icons/vsc';
import { useDispatch } from 'react-redux';

import { actions, selectors, useSelector } from '@@core/redux';
import { name } from '@@identifier/core/character';
import { IBase, ILayer, terms } from '@@interface';
import {
  BadgeNumberComplete,
  CustomAutoComplete,
  CustomNumberComplete,
  DummyCard,
  MonitorPropertyGroup,
  NumberField,
  TagCard,
} from '@@panelComponents';
import { useStyles } from '@@style/home';
import { theme } from '@@style/lib/theme';

const statusTags = terms.statusTags.values;
const numStatusTags = statusTags.length;

type MonitorLayerProps = {
  base: IBase;
  layer: ILayer;
};

export const MonitorLayer: React.FC<MonitorLayerProps> = props => {
  const classes = useStyles({});
  const dispatch = useDispatch();

  const skillIds = useSelector(state => selectors.skill.skills.extract(state, { sortFunctionId: 'id' })).ids;
  const specialSkillIds = useSelector(state =>
    selectors.specialSkill.specialSkills.extract(state, { sortFunctionId: 'id' })
  ).ids;
  const abilityIds = useSelector(state =>
    selectors.ability.abilities.extract(state, { sortFunctionId: 'idRanked' })
  ).ids;

  const { base, layer } = props;
  const { id: baseId, weapon, skillLevels } = base;
  const {
    id,
    type,
    attackType,
    layerLevelAttackType,
    gearLevelStatusBonus,
    layerLevel,
    gearLevel,
    intimacyLevel,
    bonus,
    scale,
    SSkill,
    status,
    skillIds: layerSkillIds,
    abilityIds: layerAbilityIds,
    resistance,
  } = layer;

  const [readOnly, setReadOnly] = useState(false);
  const [showError, setShowError] = useState(false);

  const layerBonusTagCards = new Array(numStatusTags);
  const layerScaleTagCards = new Array(numStatusTags);
  const layerStatusTagCards = new Array(numStatusTags);

  for (let i = 0; i < numStatusTags; i++) {
    const tag = statusTags[i];
    layerBonusTagCards[i] = <TagCard key={tag} value={bonus?.[tag]} label={tag} inGroup half transparent />;
    layerScaleTagCards[i] = (
      <NumberField
        key={tag}
        value={scale[tag]}
        label={tag}
        endAdornment="%"
        startAdornment={<div className={classes.adornmentBaseColor}>×</div>}
        inGroup
        half
        transparent
        minValue={0}
        setState={(value: number) => {
          dispatch(actions.metaFlags.stampState({}));
          dispatch(actions[name].updateScale({ tag, value, baseId: base.id, layerId: layer.id }));
        }}
        readOnly={readOnly}
      />
    );
    layerStatusTagCards[i] = <TagCard key={tag} value={status?.[tag]} label={tag} highlight inGroup half />;
  }

  return (
    <Box
      style={{
        margin: '0px 2px 0px 0px',
        padding: 0,
      }}
      className={classes.paperContainerUnderWMWindow}
      bgcolor={theme.palette.background.paper}>
      <Box className={classes.formWidthPaperContainer}>
        <Box
          className={classes.paperColumn}
          bgcolor={theme.palette.background.paper}
          style={{
            overflowY: 'scroll',
            overflowX: 'hidden',
          }}>
          <Box className={classes.tagMarginColumn} />
          <Box className={classes.tagMarginColumn} />

          <MonitorPropertyGroup label={'育成度'}>
            <CustomNumberComplete
              minValue={1}
              maxValue={50}
              step={1}
              value={layerLevel}
              label="レベル"
              setState={value => {
                dispatch(actions.metaFlags.stampState({}));
                dispatch(actions[name].updateLevel({ layerLevel: Number(value), baseId: base.id, layerId: layer.id }));
              }}
              inGroup
              contentHalf
              transparent
              readOnly={readOnly}
              showError={showError}
            />
            <CustomNumberComplete
              minValue={1}
              maxValue={20}
              step={1}
              value={gearLevel}
              label="ギアレベル"
              setState={value => {
                dispatch(actions.metaFlags.stampState({}));
                dispatch(actions[name].updateLevel({ gearLevel: Number(value), baseId: base.id, layerId: layer.id }));
              }}
              inGroup
              contentHalf
              transparent
              readOnly={readOnly}
              showError={showError}
            />
            <CustomNumberComplete
              minValue={1}
              maxValue={10}
              step={1}
              value={intimacyLevel}
              label="思い出RANK"
              setState={value => {
                dispatch(actions.metaFlags.stampState({}));
                dispatch(
                  actions[name].updateLevel({ intimacyLevel: Number(value), baseId: base.id, layerId: layer.id })
                );
              }}
              inGroup
              contentHalf
              transparent
              readOnly={readOnly}
              showError={showError}
            />
          </MonitorPropertyGroup>
          <TagCard value={type} label="タイプ" panelColor />

          <MonitorPropertyGroup label={'加算値'}>{layerBonusTagCards}</MonitorPropertyGroup>
        </Box>
      </Box>

      <Box className={classes.formWidthPaperContainer}>
        <Box
          className={classes.paperColumn}
          bgcolor={theme.palette.background.paper}
          style={{
            overflowY: 'scroll',
            overflowX: 'hidden',
          }}>
          <Box className={classes.tagMarginColumn} />
          <Box className={classes.tagMarginColumn} />

          <MonitorPropertyGroup label={'ステータスボーナス'}>
            <TagCard value={layerLevelAttackType} label="レベル強化 攻撃参照" inGroup transparent contentHalf />
            <TagCard value={gearLevelStatusBonus.alpha} label="ギアレベル強化 α" inGroup transparent contentHalf />
            <TagCard value={gearLevelStatusBonus.beta} label="ギアレベル強化 β" inGroup transparent contentHalf />
          </MonitorPropertyGroup>
          <TagCard
            value={attackType}
            label="通常攻撃参照"
            startAdornment={React.createElement(terms.weapons.Icons[weapon], {
              style: {
                margin: '0px -81px 0px 20px',
                fontSize: 12,
                color: '#bbbbbb',
              },
            })}
            endAdornment={<Box style={{ marginLeft: 3 }} />}
          />

          <MonitorPropertyGroup label={'補正係数'}>{layerScaleTagCards}</MonitorPropertyGroup>
        </Box>
      </Box>

      <Box className={classes.formWidthPaperContainer}>
        <Box
          className={classes.paperColumn}
          bgcolor={theme.palette.background.paper}
          style={{
            overflowY: 'scroll',
            overflowX: 'hidden',
          }}>
          <Box className={classes.tagMarginColumn} />
          <Box className={classes.tagMarginColumn} />

          <MonitorPropertyGroup label={'アビリティ'}>
            <CustomAutoComplete
              readOnly={readOnly}
              showError={showError}
              options={abilityIds}
              value={layerAbilityIds[0]}
              label=" "
              setState={value => {
                dispatch(actions.metaFlags.stampState({}));
                dispatch(actions[name].updateLayerField({ id, keys: ['abilityIds', 0], value }));
              }}
              inGroup
              freeSolo
            />
            <CustomAutoComplete
              readOnly={readOnly}
              showError={showError}
              options={abilityIds}
              value={layerAbilityIds[1]}
              label=" "
              setState={value => {
                dispatch(actions.metaFlags.stampState({}));
                dispatch(actions[name].updateLayerField({ id, keys: ['abilityIds', 1], value }));
              }}
              inGroup
              freeSolo
            />
            <CustomAutoComplete
              readOnly={readOnly}
              showError={showError}
              options={abilityIds}
              value={layerAbilityIds[2]}
              label=" "
              setState={value => {
                dispatch(actions.metaFlags.stampState({}));
                dispatch(actions[name].updateLayerField({ id, keys: ['abilityIds', 2], value }));
              }}
              inGroup
              freeSolo
            />
          </MonitorPropertyGroup>

          <DummyCard>
            <IconButton
              onClick={() => setReadOnly(!readOnly)}
              className={clsx(classes.groupedButton, classes.insideShadowDark)}
              style={{
                color: readOnly ? theme.palette.primary.main : theme.palette.secondary.main,
                margin: '5px 3.5px 0px 0px',
              }}
              component="span"
              color="inherit"
              size="large">
              {readOnly ? <FaLock fontSize={15} /> : <FaUnlockAlt fontSize={15} />}
            </IconButton>
            <IconButton
              onClick={() => setShowError(!showError)}
              className={clsx(classes.groupedButton, classes.insideShadowDark)}
              style={{ color: showError ? 'red' : theme.palette.secondary.main, margin: '5px 0px 0px 3.5px' }}
              component="span"
              color="inherit"
              size="large">
              <VscError fontSize={15} />
            </IconButton>
          </DummyCard>

          <MonitorPropertyGroup label={'レイヤーステータス'}>{layerStatusTagCards}</MonitorPropertyGroup>
        </Box>
      </Box>

      <Box className={classes.formWidthPaperContainer}>
        <Box
          className={classes.paperColumn}
          bgcolor={theme.palette.background.paper}
          style={{
            overflowY: 'scroll',
            overflowX: 'hidden',
          }}>
          <Box className={classes.tagMarginColumn} />
          <Box className={classes.tagMarginColumn} />

          <MonitorPropertyGroup label={'スキル'}>
            <CustomAutoComplete
              readOnly={readOnly}
              showError={showError}
              options={skillIds as string[]}
              value={layerSkillIds[0]}
              label=" "
              setState={skillId => {
                dispatch(actions.metaFlags.stampState({}));
                dispatch(
                  actions[name].updateSkillInCharacter({ baseId: base.id, layerId: layer.id, index: 0, skillId })
                );
              }}
              inGroup
              freeSolo
              badge={
                layerSkillIds[0] ? (
                  <BadgeNumberComplete
                    readOnly={readOnly}
                    showError={showError}
                    value={skillLevels[layerSkillIds[0]]}
                    maxValue={99}
                    minValue={1}
                    step={1}
                    label={null}
                    setState={value => {
                      dispatch(actions.metaFlags.stampState({}));
                      dispatch(
                        actions[name].updateBaseField({
                          id: baseId,
                          keys: ['skillLevels', layerSkillIds[0]],
                          value: Number(value),
                        })
                      );
                    }}
                  />
                ) : null
              }
            />
            <CustomAutoComplete
              readOnly={readOnly}
              showError={showError}
              options={skillIds as string[]}
              value={layerSkillIds[1]}
              label=" "
              setState={skillId => {
                dispatch(actions.metaFlags.stampState({}));
                dispatch(
                  actions[name].updateSkillInCharacter({ baseId: base.id, layerId: layer.id, index: 1, skillId })
                );
              }}
              inGroup
              freeSolo
              badge={
                layerSkillIds[1] ? (
                  <BadgeNumberComplete
                    readOnly={readOnly}
                    showError={showError}
                    value={skillLevels[layerSkillIds[1]]}
                    maxValue={99}
                    minValue={1}
                    step={1}
                    label={null}
                    setState={value => {
                      dispatch(actions.metaFlags.stampState({}));
                      dispatch(
                        actions[name].updateBaseField({
                          id: baseId,
                          keys: ['skillLevels', layerSkillIds[1]],
                          value: Number(value),
                        })
                      );
                    }}
                  />
                ) : null
              }
            />
            <CustomAutoComplete
              readOnly={readOnly}
              showError={showError}
              options={skillIds as string[]}
              value={layerSkillIds[2]}
              label=" "
              setState={skillId => {
                dispatch(actions.metaFlags.stampState({}));
                dispatch(
                  actions[name].updateSkillInCharacter({ baseId: base.id, layerId: layer.id, index: 2, skillId })
                );
              }}
              inGroup
              freeSolo
              badge={
                layerSkillIds[2] ? (
                  <BadgeNumberComplete
                    readOnly={readOnly}
                    showError={showError}
                    value={skillLevels[layerSkillIds[2]]}
                    maxValue={99}
                    minValue={1}
                    step={1}
                    label={null}
                    setState={value => {
                      dispatch(actions.metaFlags.stampState({}));
                      dispatch(
                        actions[name].updateBaseField({
                          id: baseId,
                          keys: ['skillLevels', layerSkillIds[2]],
                          value: Number(value),
                        })
                      );
                    }}
                  />
                ) : null
              }
            />
          </MonitorPropertyGroup>

          {SSkill != null ? (
            <CustomAutoComplete
              options={specialSkillIds as string[]}
              value={SSkill}
              label="スペシャルスキル"
              freeSolo
              setState={value => {
                dispatch(actions.metaFlags.stampState({}));
                dispatch(actions[name].updateLayerField({ id, keys: ['SSkill'], value }));
              }}
              readOnly={readOnly}
              showError={showError}
            />
          ) : (
            <DummyCard />
          )}

          <MonitorPropertyGroup label={'属性耐性'}>
            {terms.attributes.values.map(tag => {
              return (
                <NumberField
                  readOnly={readOnly}
                  key={tag}
                  value={resistance[tag]}
                  label={' '}
                  startAdornment={React.createElement(terms.attributes.Icons[tag])}
                  endAdornment="%"
                  inGroup
                  half
                  inputTypeInteger
                  setState={(value: number) => {
                    dispatch(actions.metaFlags.stampState({}));
                    dispatch(actions[name].updateLayerField({ id, keys: ['resistance', tag], value }));
                  }}
                />
              );
            })}
          </MonitorPropertyGroup>
        </Box>
      </Box>
    </Box>
  );
};
