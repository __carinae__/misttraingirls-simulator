import { TabContext, TabPanel } from '@mui/lab';
import {
  Box,
  Button,
  Checkbox,
  Chip,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  Input,
} from '@mui/material';
import clsx from 'clsx';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import { GiGrowth, GiReturnArrow } from 'react-icons/gi';
import { GoDiff, GoGraph } from 'react-icons/go';
import { useDispatch } from 'react-redux';
import Spreadsheet, { Matrix } from 'react-spreadsheet';
import { useKeys } from 'rooks';

import { AttackStatusTag, IBase, ILayer, terms } from '@@core/__interface__';
import { Character } from '@@core/domain';
import { actions, selectors, useSelector } from '@@core/redux';
import { verifyInputNumber } from '@@core/utils';
import { name } from '@@identifier/core/character';
import { BaseRarityIcon, LayerRarityIcon, SwitchableChipList } from '@@panelComponents';
import { useStyles } from '@@style/home';
import { theme } from '@@style/lib/theme';

const statusTags = terms.statusTags.values;

/**
 * Table comopnents
 * */
const getBaseDataViewer = (base: IBase, color?: string, fontWeight?: number) =>
  function DataViewer({ cell }) {
    return (
      <Box display="flex" flexDirection="row">
        <Box margin="3px 0px -3px 10px">
          <BaseRarityIcon entity={base} />
        </Box>
        <Box margin="3px 0px -3px 0px" style={{ color, fontWeight }}>
          {cell.value}
        </Box>
      </Box>
    );
  };

const getLayerDataViewer = (layer: ILayer) =>
  function DataViewer({ cell }) {
    return (
      <Box display="flex" flexDirection="row">
        <Box margin="3px 0px -3px 10px">
          <LayerRarityIcon entity={layer} />
        </Box>
        <Box margin="3px 0px -3px 0px">{cell.value}</Box>
      </Box>
    );
  };

const makeDataViewer = (getColor?: (value: number) => string, getFontWeight?: (value: number) => number) =>
  function DataViewer({ cell }) {
    return (
      <Box
        style={{
          ...(getColor && { color: getColor(cell.value) }),
          ...(getFontWeight && { fontWeight: getFontWeight(cell.value) }),
        }}>
        {cell.value ?? ''}
      </Box>
    );
  };

const DataEditor = ({ cell, onChange }) => {
  const classes = useStyles({});

  return (
    <Input
      size="small"
      className={clsx(classes.defaultFontSize, classes.growthTableNumberColumnEditor)}
      classes={{ underline: classes.underline }}
      inputProps={{ style: { textAlign: 'center', marginTop: 4, marginLeft: 0 } }}
      value={cell.value != null ? cell.value.toString() : ''}
      onChange={event => {
        const value = Number(event.target.value);
        if (verifyInputNumber(value, { inputTypeInteger: true }) && cell.value !== value) {
          onChange({ ...cell, value });
        }
      }}
      disableUnderline
      autoFocus
    />
  );
};

const editableCell = (
  value: number | string | undefined,
  cellProps: {
    className?: string;
    DataViewer?: ReturnType<typeof makeDataViewer>;
    readOnly?: boolean;
  } = {}
) => {
  const { className = '', readOnly = false, DataViewer } = cellProps;
  return useMemo(() => {
    return {
      value,
      className: className,
      DataViewer: DataViewer ?? makeDataViewer(),
      ...(readOnly ? { readOnly } : { DataEditor }),
    };
  }, [value]);
};

const baseIdIndexCell = (
  id: number | string | undefined,
  isOnMAxHpBand: boolean,
  prioritizedStatusBreak: number,
  base: IBase,
  cellProps: {
    className?: string;
  } = {}
) => {
  const { className = '' } = cellProps;
  return useMemo(() => {
    return {
      value: id,
      className: className,
      DataViewer: getBaseDataViewer(
        base,
        getColorBaseId(isOnMAxHpBand, prioritizedStatusBreak),
        getFontWeightBaseId(isOnMAxHpBand, prioritizedStatusBreak)
      ),
      readOnly: true,
    };
  }, [id, isOnMAxHpBand, prioritizedStatusBreak]);
};

const getColorGrowthStep = (value: number) => {
  switch (value) {
    case undefined:
      return '#ffffff';
    case 0:
      return '#ffffff';
    case 10:
      return '#fef2d0';
    case 20:
      return '#edddce';
    case 30:
      return '#dcc7cb';
    case 40:
      return '#cbb1c9';
    default:
      return '#ba9bc7';
  }
};

const getColorGrowthLimit = (value: number) => {
  return value < 0
    ? '#a0b6d9'
    : value === 0
    ? 'white'
    : value === 10
    ? '#f7e0e2'
    : value === 20
    ? '#f2c4c5'
    : value === 30
    ? '#eea8a9'
    : value === 40
    ? '#ea8d8c'
    : value >= 50
    ? '#e7726f'
    : '';
};

const getColorBaseId = (isOnMAxHpBand: boolean, prioritizedStatusBreak: number) => {
  return isOnMAxHpBand ? getColorGrowthLimit(prioritizedStatusBreak) : 'white';
};

const getColorMaxBaseDiff = (value: number) => {
  switch (value) {
    case 0:
      return '#ee8488';
    case -50:
      return '#b8c5e4';
    default:
      return '#e0eaf6';
  }
};

const getFontWeightGrowthLimit = (value: number) => {
  return value >= 30 ? 700 : 400;
};

const getFontWeightBaseId = (isOnMAxHpBand: boolean, prioritizedStatusBreak: number) => {
  return isOnMAxHpBand ? getFontWeightGrowthLimit(prioritizedStatusBreak) : 400;
};

const tabMenu = [
  {
    value: 'status',
    Icon: function Icon() {
      return <GoGraph fontSize={15} />;
    },
    label: 'ベースステータス',
  },
  {
    value: 'difference',
    Icon: function Icon() {
      return <GoDiff fontSize={15} />;
    },
    label: '限界突破確認',
  },
  {
    value: 'growthLimit',
    Icon: function Icon() {
      return <GiReturnArrow fontSize={15} style={{ transform: 'rotate(-70deg) scaleY(-1)' }} />;
    },
    label: 'レイヤー毎限界値',
  },
];

export type GrowthTableDialogProps = {
  open: boolean;
  handleClose: () => void;
};
export const GrowthTableDialog: React.FC<GrowthTableDialogProps> = props => {
  const classes = useStyles({});
  const className = clsx(classes.defaultFontSize, classes.growthTableNumberColumn);

  const dispatch = useDispatch();
  const { open, handleClose } = props;

  /** Component controll */
  const { tableId } = useSelector(state => state.present.ui.characterPanel.GrowthTable);
  const setTableId = (tableId: string) => {
    dispatch(actions.characterPanel.setGrowthTableId(tableId));
  };

  const bases = useSelector(state => selectors[name].bases.custom(state, { sortFunctionId: 'id' }));
  const layers = useSelector(state => selectors[name].layers.custom(state, { sortFunctionId: 'baseId:rarity:index' }));
  const { maxHP } = useSelector(state => state.present.core[name].nonEntityFields);
  const numBases = bases.ids.length;
  const numLayers = layers.ids.length;

  /** Headers */
  const baseIdHeader = {
    value: '　　　ベース名',
    readOnly: true,
    className: clsx(classes.defaultFontSize, classes.growthTableIdColumnIndicator),
  };
  const layerIdHeader = {
    value: '　　　レイヤー名',
    readOnly: true,
    className: clsx(classes.defaultFontSize, classes.growthTableIdColumnIndicator),
  };
  const maxBaseDiffHeader = {
    value: '最大差',
    readOnly: true,
    className: clsx(classes.defaultFontSize, classes.growthTableNumberColumnIndicator),
  };
  const growthStepHeader = {
    value: '育成幅',
    readOnly: true,
    className: clsx(classes.defaultFontSize, classes.growthTableNumberColumnIndicator),
  };
  const hpHeader = {
    value: 'HP',
    readOnly: true,
    className: clsx(classes.defaultFontSize, classes.growthTableNumberColumnIndicator),
  };
  const statusTagsHeaders = statusTags.map(tag => ({
    value: tag,
    readOnly: true,
    className: clsx(classes.defaultFontSize, classes.growthTableNumberColumnIndicator),
  }));

  /**
   * 1st tab id: 'status'
   * 1st table page data: growth step, hp, base status
   * */
  const baseStatusIdTableData = new Array(numBases);
  const baseStatusTableData = new Array(numBases);
  const maxHPCell = editableCell(maxHP, { className: clsx(className, classes.growthSubTableColumn) });
  const maxBaseStatusBaseline = Character.getBaseStatusBaseline(maxHP);

  /**
   * Checkbox for automatically update base statuses based on growth step
   * */
  const [allChecked, setAllChecked] = useState(false);
  const [checkboxes, setCheckboxes] = useState(new Array(numBases).fill(false));
  const Checkboxes = new Array(numBases);
  const checkedAny = checkboxes.some(check => check);

  const onChangeAllCheckbox = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { checked } = event.target;
    if (checked) {
      setAllChecked(true);
      setCheckboxes(new Array(numBases).fill(true));
    } else {
      setAllChecked(false);
      setCheckboxes(new Array(numBases).fill(false));
    }
  };

  const onChangeCheckbox = (i: number) => (event: React.ChangeEvent<HTMLInputElement>) => {
    const { checked } = event.target;
    const tmp = checkboxes.slice();
    if (checked) {
      tmp[i] = true;
    } else {
      tmp[i] = false;
      setAllChecked(false);
    }
    setCheckboxes(tmp);
  };

  /**
   * 2nd tab id: 'difference'
   * */
  const differenceTableData = new Array(numBases);

  /**
   * 3rd tab id: 'growthLimit'
   * 3rd table page data: status growth limits of layers
   * */
  const growthLimitIdTableData = new Array(numLayers);
  const growthLimitTableData = new Array(numLayers);

  /**
   * Fill in the table contents required at once.
   */
  let numReadLayers = 0;
  for (let j = 0; j < numBases; j++) {
    const id = bases.ids[j];
    const base = bases.entities[id];
    const { growthStep, hp, status } = base;
    const layersBelonging = Character.getLayersBelonging(base.layerIds);
    const baseStatusBaseline = Character.getBaseStatusBaseline(hp);
    const maxBaseDiff = baseStatusBaseline - maxBaseStatusBaseline;
    let prioritizedAttackStatus: AttackStatusTag;

    /**
     * 1st page
     * */
    baseStatusTableData[j] = [
      editableCell(growthStep ?? 0, { className, DataViewer: makeDataViewer(getColorGrowthStep) }),
      editableCell(hp, { className }),
      ...statusTags.map(tag => editableCell(status[tag], { className })),
    ];
    Checkboxes[j] = (
      <Box key={j} className={clsx(classes.defaultFontSize, classes.growthTableCheckboxColumn)}>
        <Checkbox
          checked={checkboxes[j]}
          onChange={onChangeCheckbox(j)}
          className={classes.smallRadioAndCheckbox}
          style={{ width: 28, height: 28 }}
        />
      </Box>
    );

    /**
     * 2nd page and 3rd page ([NOTE] 2nd page's data are also required in 1st page's command)
     * */
    const numLayersBelonging = layersBelonging.length;
    const baseGrowthLimit = {};

    for (let i = numLayersBelonging - 1; i >= 0; i--) {
      const layerId = layersBelonging[i];
      const layer = layers.entities[layerId];
      const { statusGrowthLimit, attackType } = layer;

      if (i === numLayersBelonging - 1) prioritizedAttackStatus = attackType;

      for (const tag of statusTags) {
        const value = statusGrowthLimit[tag];
        if (value != null && (baseGrowthLimit[tag] == null || baseGrowthLimit[tag] < value)) {
          baseGrowthLimit[tag] = value;
        }
      }

      growthLimitIdTableData[numReadLayers] = [
        editableCell(i === numLayersBelonging - 1 ? id : '', {
          className: clsx(
            classes.defaultFontSize,
            numLayersBelonging === 1
              ? classes.growthTableIdColumnNoShadow
              : i === 0
              ? classes.growthTableIdColumnLast
              : i === numLayersBelonging - 1
              ? classes.growthTableIdColumnFirst
              : classes.growthTableIdColumnMiddle
          ),
          readOnly: true,
          DataViewer: i === numLayersBelonging - 1 ? getBaseDataViewer(base) : undefined,
        }),
        editableCell(layerId, {
          readOnly: true,
          className: clsx(classes.defaultFontSize, classes.growthTableIdColumn),
          DataViewer: getLayerDataViewer(layer),
        }),
      ];
      growthLimitTableData[numReadLayers] = statusTags.map(tag =>
        editableCell(statusGrowthLimit[tag], {
          className,
          DataViewer: makeDataViewer(getColorGrowthLimit, getFontWeightGrowthLimit),
        })
      );
      numReadLayers++;
    }

    /**
     * Determine base id display color based on the status limit breaks.
     */
    const prioritizedStatusBreak =
      status[prioritizedAttackStatus] - baseStatusBaseline - (baseGrowthLimit[prioritizedAttackStatus] ?? 0);

    const maxBaseDiffCell = editableCell(maxBaseDiff, {
      className: clsx(classes.defaultFontSize, classes.growthTableNumberColumn, classes.nonPointerEvents),
      readOnly: true,
      DataViewer: makeDataViewer(getColorMaxBaseDiff),
    });

    baseStatusIdTableData[j] = [
      editableCell(id, {
        className: clsx(classes.defaultFontSize, classes.growthTableIdColumn),
        readOnly: true,
        DataViewer: getBaseDataViewer(base),
      }),
      maxBaseDiffCell,
    ];
    differenceTableData[j] = [
      baseIdIndexCell(id, maxBaseDiff === 0, prioritizedStatusBreak, base, {
        className: clsx(classes.defaultFontSize, classes.growthTableIdColumn),
      }),
      maxBaseDiffCell,
      editableCell(growthStep, {
        readOnly: true,
        className: clsx(className, classes.nonPointerEvents),
        DataViewer: makeDataViewer(getColorGrowthStep),
      }),
      editableCell(hp, {
        readOnly: true,
        className: clsx(className, classes.nonPointerEvents),
      }),
      ...statusTags.map(tag => {
        const value = status[tag] - baseStatusBaseline - (baseGrowthLimit[tag] ?? 0);
        return editableCell(value, {
          className: clsx(classes.defaultFontSize, classes.growthTableNumberColumn, classes.nonPointerEvents),
          readOnly: true,
          DataViewer: makeDataViewer(getColorGrowthLimit, getFontWeightGrowthLimit),
        });
      }),
    ];
  }

  /** Scrolls */
  /** For linked scrolls in 1st page */
  const s1 = useRef(null);
  const s2 = useRef(null);

  /** For linked scrolls in 2nd page */
  const s3 = useRef(null);

  /** For linked scrolls in 3rd page */
  const s4 = useRef(null);
  const s5 = useRef(null);

  const onScrolls1 = () => {
    if (s2.current != null) {
      s2.current.scrollTop = s1.current.scrollTop;
    }
  };
  const onScrolls2 = () => {
    if (s1.current != null) {
      s1.current.scrollTop = s2.current.scrollTop;
    }
  };
  const onScrolls4 = () => {
    if (s5.current != null) {
      s5.current.scrollTop = s4.current.scrollTop;
    }
  };
  const onScrolls5 = () => {
    if (s4.current != null) {
      s4.current.scrollTop = s5.current.scrollTop;
    }
  };

  /** Scroll linking between 1st and 2nd page */
  const [linkedScrollTop, setScrollTop] = useState(0);
  useEffect(() => {
    if (open) {
      if (s1.current != null) {
        s1.current.scrollTop = linkedScrollTop;
      }
      if (s2.current != null) {
        s2.current.scrollTop = linkedScrollTop;
      }
      if (s3.current != null) {
        s3.current.scrollTop = linkedScrollTop;
      }
    }
  }, [tableId]);

  useKeys(['Meta', 'ArrowUp'], () => {
    if (s1 && s1.current) {
      s1.current.scrollTop = 0;
    } else if (s3 && s3.current) {
      s3.current.scrollTop = 0;
    } else if (s5 && s5.current) {
      s5.current.scrollTop = 0;
    }
  });

  useKeys(['Meta', 'ArrowDown'], () => {
    if (s1 && s1.current) {
      s1.current.scrollTop = s1.current.scrollHeight;
    } else if (s3 && s3.current) {
      s3.current.scrollTop = s3.current.scrollHeight;
    } else if (s5 && s5.current) {
      s5.current.scrollTop = s5.current.scrollHeight;
    }
  });

  /** Hundlers */
  const onChangeTabList = (event: React.ChangeEvent<unknown>, id: string) => {
    /** Restore scroll top depending on the current table id */
    switch (tableId) {
      case 'status':
        if (s1.current != null) {
          setScrollTop(s1.current.scrollTop);
        }
        break;
      case 'difference':
        if (s3.current != null) {
          setScrollTop(s3.current.scrollTop);
        }
        break;
      default:
        break;
    }
    setTableId(id);
  };

  const onChangeBaseStatusTableData = (
    data: Matrix<{
      value: number;
      className: string;
      DataEditor: typeof DataEditor;
      DataViewer: ReturnType<typeof makeDataViewer>;
    }>
  ) => {
    dispatch(actions.metaFlags.stampState({}));
    dispatch(actions[name].onChangeBaseStatusTableData({ ids: bases.ids, data }));
  };

  const onChangeGrowthLimitTableData = (
    data: Matrix<{
      value: number;
      className: string;
      DataEditor: typeof DataEditor;
      DataViewer: ReturnType<typeof makeDataViewer>;
    }>
  ) => {
    dispatch(actions.metaFlags.stampState({}));
    dispatch(actions[name].onChangeGrowthLimitTableData({ ids: layers.ids, data }));
  };

  const onChangeMaxHPCell = (
    data: Matrix<{
      value: string | number;
      readOnly?: boolean;
      className: string;
      DataEditor?: typeof DataEditor;
    }>
  ) => {
    const cell = data[0][0];
    dispatch(actions.metaFlags.stampState({}));
    dispatch(actions[name].updateNonEntityField({ keys: ['maxHP'], value: cell != null ? cell.value : 0 }));
  };

  const hundleRaiseHP = () => {
    dispatch(actions.metaFlags.stampState({}));
    dispatch(actions[name].hundleRaiseHP({ ids: bases.ids, checkboxes }));
  };

  const hundleRaiseBaseStatus = () => {
    dispatch(actions.metaFlags.stampState({}));
    dispatch(actions[name].hundleRaiseBaseStatus({ ids: bases.ids, checkboxes, differenceTableData }));
  };

  /**
   * Maximize HPs
   */
  const raiseHPButton = (
    <Box onClick={hundleRaiseHP} style={{ textAlign: 'center' }}>
      <Chip
        icon={
          <IconButton
            className={clsx(classes.groupedButton, classes.insideShadowDivider)}
            style={{
              margin: '1px -5px 0px 3px',
              color: checkedAny ? theme.palette.primary.main : theme.palette.divider,
            }}
            component="span"
            size="large">
            <GiGrowth fontSize={15} />
          </IconButton>
        }
        label={
          <Box width={80} style={{ textAlign: 'center' }}>
            {'一律でHP最大化'}
          </Box>
        }
        style={{ height: 46, borderRadius: 20, margin: 0 }}
        className={clsx(classes.panelBackground, classes.insideShadowDark)}
      />
    </Box>
  );

  /**
   * Raise Base Statuses
   * */
  const raiseBaseStatusButton = (
    <Box onClick={hundleRaiseBaseStatus} style={{ textAlign: 'center' }}>
      <Chip
        icon={
          <IconButton
            className={clsx(classes.groupedButton, classes.insideShadowDivider)}
            style={{
              margin: '1px -5px 0px 3px',
              color: checkedAny ? theme.palette.primary.main : theme.palette.divider,
            }}
            component="span"
            size="large">
            <GiGrowth fontSize={15} />
          </IconButton>
        }
        label={
          <Box width={80} style={{ textAlign: 'center' }}>
            {'成長限界まで育成'}
          </Box>
        }
        style={{ height: 46, borderRadius: 20, margin: 0 }}
        className={clsx(classes.panelBackground, classes.insideShadowDark)}
      />
    </Box>
  );

  /** Rendering */
  return (
    <Dialog open={open} onClose={handleClose} scroll="paper" fullWidth maxWidth="lg" disableEscapeKeyDown>
      <DialogTitle
        className={clsx(classes.paperBackground, classes.dialogTitleWindow)}
        style={{ fontSize: 13, color: theme.palette.primary.main, paddingBottom: 15 }}>
        <GoGraph style={{ fontSize: 15, margin: '0px 11px -2.5px 10px' }} />
        {'育成管理'}
      </DialogTitle>
      <Box className={classes.paperBackground}>
        <DialogContent dividers style={{ borderRadius: 50, boxShadow: theme.shadows[24] }}>
          <Box display="flex" flexDirection="row" style={{ justifyContent: 'center' }}>
            {/* Tab list and main table */}
            <TabContext value={tableId}>
              <Box display="flex" flexDirection="column" style={{ width: 175, marginLeft: -25 }}>
                <SwitchableChipList tabId={tableId} tabMenu={tabMenu} onChangeTabList={onChangeTabList} />
                {/* Extra chips */}
                <TabPanel value="status" style={{ padding: 0, margin: 0 }}>
                  {raiseHPButton}
                  {raiseBaseStatusButton}
                </TabPanel>
              </Box>

              {/* Table linked to tab 'status' */}
              <TabPanel value="status" style={{ display: 'flex', flexDirection: 'row', padding: 0, margin: 0 }}>
                {/* Static table */}
                <Box>
                  <Box
                    overflow="hidden"
                    textAlign="center"
                    style={{ display: 'flex', flexDirection: 'row', padding: 0, margin: 0 }}>
                    {/* All selecting checkbox */}
                    <Box className={clsx(classes.defaultFontSize, classes.growthTableCheckboxColumnIndicator)}>
                      <Checkbox
                        checked={allChecked}
                        onChange={onChangeAllCheckbox}
                        className={classes.smallRadioAndCheckbox}
                        style={{ width: 28, height: 28 }}
                      />
                    </Box>
                    <Spreadsheet
                      data={[[baseIdHeader, maxBaseDiffHeader]]}
                      className={classes.growthTable}
                      hideColumnIndicators={true}
                      hideRowIndicators={true}
                    />
                  </Box>
                  <Box
                    ref={s1}
                    onScroll={onScrolls1}
                    style={{
                      marginTop: -0.5,
                      maxHeight: '72vh',
                      width: '100%',
                      overflowX: 'hidden',
                      overflowY: 'scroll',
                      textAlign: 'center',
                      display: 'flex',
                      flexDirection: 'row',
                    }}>
                    {/* Checkbox column */}
                    <Box>{Checkboxes}</Box>
                    <Spreadsheet
                      data={baseStatusIdTableData}
                      className={classes.growthTable}
                      hideColumnIndicators={true}
                      hideRowIndicators={true}
                    />
                  </Box>
                </Box>

                {/* Editable table */}
                <Box style={{ marginRight: 3 }}>
                  <Box
                    overflow="hidden"
                    textAlign="center"
                    style={{ display: 'flex', flexDirection: 'row', padding: 0, margin: 0 }}>
                    <Spreadsheet
                      data={[[growthStepHeader, hpHeader, ...statusTagsHeaders]]}
                      className={classes.growthTable}
                      hideColumnIndicators={true}
                      hideRowIndicators={true}
                    />
                  </Box>
                  <Box
                    ref={s2}
                    onScroll={onScrolls2}
                    style={{
                      marginTop: -0.5,
                      maxHeight: '72vh',
                      width: '100%',
                      overflowX: 'hidden',
                      overflowY: 'scroll',
                      textAlign: 'center',
                      display: 'flex',
                      flexDirection: 'row',
                    }}>
                    <Spreadsheet
                      data={baseStatusTableData}
                      className={classes.growthTable}
                      hideColumnIndicators={true}
                      hideRowIndicators={true}
                      onChange={onChangeBaseStatusTableData}
                    />
                  </Box>
                </Box>
              </TabPanel>

              {/* Table linked to tab 'difference' */}
              <TabPanel value="difference" style={{ display: 'flex', flexDirection: 'row', padding: 0, margin: 0 }}>
                <Box width={28} />
                {/* Static table */}
                <Box style={{ marginRight: 3 }}>
                  <Box overflow="hidden" textAlign="center" style={{ display: 'flex', flexDirection: 'row' }}>
                    <Spreadsheet
                      data={[[baseIdHeader, maxBaseDiffHeader, growthStepHeader, hpHeader, ...statusTagsHeaders]]}
                      className={classes.growthTable}
                      hideColumnIndicators={true}
                      hideRowIndicators={true}
                    />
                  </Box>
                  <Box
                    ref={s3}
                    style={{
                      marginTop: -0.5,
                      maxHeight: '72vh',
                      width: '100%',
                      overflowX: 'hidden',
                      overflowY: 'scroll',
                      textAlign: 'center',
                      display: 'flex',
                      flexDirection: 'row',
                    }}>
                    <Spreadsheet
                      data={differenceTableData}
                      className={classes.growthTable}
                      hideColumnIndicators={true}
                      hideRowIndicators={true}
                    />
                  </Box>
                </Box>
              </TabPanel>

              {/* Table linked to tab 'growthLimit' */}
              <TabPanel value="growthLimit" style={{ display: 'flex', flexDirection: 'row', padding: 0, margin: 0 }}>
                <Box width={28} />
                {/* Static table */}
                <Box>
                  <Box overflow="hidden" textAlign="center" style={{ display: 'flex', flexDirection: 'row' }}>
                    <Spreadsheet
                      data={[[baseIdHeader, layerIdHeader]]}
                      className={classes.growthTable}
                      hideColumnIndicators={true}
                      hideRowIndicators={true}
                    />
                  </Box>
                  <Box
                    ref={s4}
                    onScroll={onScrolls4}
                    style={{
                      marginTop: -0.5,
                      maxHeight: '72vh',
                      width: '100%',
                      overflowX: 'hidden',
                      overflowY: 'scroll',
                      textAlign: 'center',
                      display: 'flex',
                      flexDirection: 'row',
                    }}>
                    <Spreadsheet
                      data={growthLimitIdTableData}
                      className={classes.growthTable}
                      hideColumnIndicators={true}
                      hideRowIndicators={true}
                    />
                  </Box>
                </Box>

                {/* Editable table */}
                <Box style={{ marginRight: 3 }}>
                  <Box overflow="hidden" textAlign="center" style={{ display: 'flex', flexDirection: 'row' }}>
                    <Spreadsheet
                      data={[statusTagsHeaders]}
                      className={classes.growthTable}
                      hideColumnIndicators={true}
                      hideRowIndicators={true}
                    />
                  </Box>
                  <Box
                    ref={s5}
                    onScroll={onScrolls5}
                    style={{
                      marginTop: -0.5,
                      maxHeight: '72vh',
                      width: '100%',
                      overflowX: 'hidden',
                      overflowY: 'scroll',
                      textAlign: 'center',
                      display: 'flex',
                      flexDirection: 'row',
                    }}>
                    <Spreadsheet
                      data={growthLimitTableData}
                      className={classes.growthTable}
                      hideColumnIndicators={true}
                      hideRowIndicators={true}
                      onChange={onChangeGrowthLimitTableData}
                    />
                  </Box>
                </Box>
              </TabPanel>
            </TabContext>

            {/* Sub table */}
            <Box flexDirection="column" display="flex">
              <Spreadsheet
                data={[
                  [
                    {
                      value: '最大HP',
                      readOnly: true,
                      className: clsx(classes.defaultFontSize, classes.growthSubTableIndicator),
                    },
                  ],
                ]}
                className={classes.growthTable}
                hideColumnIndicators={true}
                hideRowIndicators={true}
              />
              <Spreadsheet
                data={[[maxHPCell]]}
                className={classes.growthTable}
                hideColumnIndicators={true}
                hideRowIndicators={true}
                onChange={onChangeMaxHPCell}
              />
              <Spreadsheet
                data={[
                  [
                    {
                      value: '最大ベース基準値',
                      readOnly: true,
                      className: clsx(classes.defaultFontSize, classes.growthSubTableIndicator),
                    },
                  ],
                ]}
                className={classes.growthTable}
                hideColumnIndicators={true}
                hideRowIndicators={true}
              />
              <Spreadsheet
                data={[
                  [
                    {
                      value: maxBaseStatusBaseline,
                      readOnly: true,
                      className: clsx(classes.defaultFontSize, classes.growthSubTableColumn, classes.nonPointerEvents),
                    },
                  ],
                ]}
                className={classes.growthTable}
                hideColumnIndicators={true}
                hideRowIndicators={true}
              />
            </Box>
          </Box>
        </DialogContent>
      </Box>
      <DialogActions className={clsx(classes.paperBackground, classes.dialogActionWindow)}>
        <Button onClick={handleClose} color="primary">
          Close
        </Button>
      </DialogActions>
    </Dialog>
  );
};
