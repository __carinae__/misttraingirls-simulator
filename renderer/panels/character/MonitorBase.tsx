import { Box, IconButton } from '@mui/material';
import clsx from 'clsx';
import React, { useMemo } from 'react';
import { GoGraph } from 'react-icons/go';
import { useDispatch } from 'react-redux';

import { actions, useSelector } from '@@core/redux';
import { name } from '@@identifier/core/character';
import { IBase, ILayer, StatusTag, terms } from '@@interface';
import { DummyCard, ImageMotionSD, MonitorPropertyGroup, NumberField, TagCard } from '@@panelComponents';
import { useStyles } from '@@style/home';
import { theme } from '@@style/lib/theme';

import { GrowthTableDialog } from './GrowthTable';

type MonitorBaseProps = {
  base: IBase;
  layer: ILayer;
};
export const MonitorBase: React.FC<MonitorBaseProps> = props => {
  const classes = useStyles({});
  const dispatch = useDispatch();
  const openGrowthTable = useSelector(state => state.present.ui.characterPanel.GrowthTable.open);
  const setOpenGrowthTable = (open: boolean) => {
    dispatch(actions.characterPanel.setOpenGrowthTable(open));
  };
  const { base, layer } = props;
  const { id, from, race, hp, status } = base;

  const imageMotionSD = useMemo(
    () => (
      <Box className={clsx(classes.paperColumn, classes.paperBackground)} style={{ padding: 0, margin: 0 }}>
        <ImageMotionSD
          baseId={id}
          height={140}
          {...(layer != null && layer.baseId === id
            ? { rarity: layer.rarity, layerId: layer.id }
            : { rarity: 'SS', layerId: '' })}
        />
      </Box>
    ),
    [id, layer?.id]
  );
  const growthTableButton = useMemo(
    () => (
      <DummyCard>
        <IconButton
          onClick={() => {
            setOpenGrowthTable(true);
          }}
          className={clsx(classes.groupedButton, classes.insideShadowDark)}
          style={{ color: theme.palette.secondary.main }}
          component="span"
          color="inherit"
          size="large">
          <GoGraph fontSize={15} />
        </IconButton>
      </DummyCard>
    ),
    []
  );

  return (
    <>
      {openGrowthTable ? (
        <GrowthTableDialog open={openGrowthTable} handleClose={() => setOpenGrowthTable(false)} />
      ) : null}
      <Box
        style={{
          margin: '0px 2px 0px 0px',
          padding: 0,
        }}
        bgcolor={theme.palette.background.paper}>
        <Box className={classes.formWidthPaperContainer}>
          {imageMotionSD}
          {growthTableButton}

          <Box
            className={clsx(classes.paperColumn, classes.paperBackground)}
            style={{
              overflowY: 'scroll',
              overflowX: 'hidden',
            }}>
            <TagCard value={from} label={terms.affiliations.legend} panelColor />
            <Box style={{ marginTop: -2 }}>
              <Box className={classes.tagMarginColumn} />
            </Box>

            <TagCard value={race} label={terms.races.legend} panelColor />
            <Box style={{ marginTop: -2 }}>
              <Box className={classes.tagMarginColumn} />
            </Box>

            <NumberField
              value={hp}
              label={'HP'}
              inputTypeInteger
              minValue={0}
              setState={(value: number) => {
                dispatch(actions.metaFlags.stampState({}));
                dispatch(actions[name].updateBaseField({ id, keys: ['hp'], value }));
              }}
            />

            <MonitorPropertyGroup label={'ベースステータス'}>
              {terms.statusTags.values.map(tag => {
                return (
                  <NumberField
                    key={tag}
                    value={status[tag]}
                    label={tag}
                    inGroup
                    half
                    inputTypeInteger
                    minValue={0}
                    setState={(value: number) => {
                      dispatch(actions.metaFlags.stampState({}));
                      dispatch(
                        actions[name].updateBaseStatus({
                          status: { [tag]: value } as Record<StatusTag, number>,
                          id,
                        })
                      );
                    }}
                  />
                );
              })}
            </MonitorPropertyGroup>
            <Box className={classes.tagMarginColumn} />
          </Box>
        </Box>
      </Box>
    </>
  );
};
