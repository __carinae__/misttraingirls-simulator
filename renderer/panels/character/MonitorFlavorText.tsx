import { Box } from '@mui/material';
import clsx from 'clsx';
import React, { memo } from 'react';

import { FlowingText } from '@@style/FlowingText';
import { useStyles } from '@@style/home';

type LayerFlavorTextProps = {
  text: string;
};
export const LayerFlavorText = memo((props: LayerFlavorTextProps): JSX.Element => {
  const { text } = props;
  const classes = useStyles({});
  const context = document.createElement('canvas').getContext('2d');
  context.font = "10.5px 'Noto Sans', sans-serif";
  const metrics = context.measureText(text);

  return (
    <Box className={classes.flavorTextWindow}>
      <Box
        style={{
          borderRadius: 26,
          borderRight: '0px',
          width: '100%',
          height: '100%',
          overflowX: 'hidden',
          overflowY: 'visible',
        }}
        className={clsx(classes.panelBackground, classes.insideShadow)}>
        <div style={{ height: 25, marginTop: 25 }}>
          <FlowingText textWidth={metrics.width}>{text}</FlowingText>
        </div>
      </Box>
    </Box>
  );
});
