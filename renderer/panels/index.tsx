import * as character from './character';
import * as skill from './skill';

/**
 * Register panels.
 * */
const __panels = [character, skill];

export const panelKeys = Array.from({ length: __panels.length }, (_, i) => 'No-' + i);
export const panels = Object.fromEntries(panelKeys.map((key, i) => [key, __panels[i].default]));
