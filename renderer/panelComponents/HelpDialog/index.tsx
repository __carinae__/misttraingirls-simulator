import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Divider,
} from '@mui/material';
import clsx from 'clsx';
import React from 'react';
import { MdLiveHelp } from 'react-icons/md';
import { useDispatch } from 'react-redux';

import { actions, useSelector } from '@@core/redux';
import { useStyles } from '@@style/home';
import { theme } from '@@style/lib/theme';

type HelpDialogProps = {
  title: string;
  msgGroups: string[][];
};
export const HelpDialog: React.FC<HelpDialogProps> = props => {
  const classes = useStyles({});
  const dispatch = useDispatch();

  const { title, msgGroups } = props;

  const { open } = useSelector(state => state.present.ui.help);
  const handleClose = () => dispatch(actions.help.setOpen(false));

  const descriptionElementRef = React.useRef<HTMLElement>(null);

  return (
    <>
      <Dialog open={open} onClose={handleClose} scroll="paper" fullWidth maxWidth="md">
        <DialogTitle
          className={clsx(classes.paperBackground, classes.dialogTitleWindow)}
          style={{ fontSize: 13, color: theme.palette.primary.main, paddingBottom: 15 }}>
          <MdLiveHelp style={{ fontSize: 16, margin: '0px 10px -4px 10px' }} />
          {title}
        </DialogTitle>
        <Box className={classes.paperBackground}>
          <DialogContent dividers style={{ borderRadius: 50, boxShadow: theme.shadows[24] }}>
            {msgGroups.map((msgs, g) => {
              return (
                <div key={'msg-' + g}>
                  <br />
                  {msgs.map((msg, i) => (
                    <DialogContentText key={'msg-' + i} ref={descriptionElementRef} align="justify" gutterBottom>
                      {msg}
                    </DialogContentText>
                  ))}
                  <br />
                  {g === msgGroups.length - 1 ? null : <Divider />}
                </div>
              );
            })}
          </DialogContent>
        </Box>
        <DialogActions className={clsx(classes.paperBackground, classes.dialogActionWindow)}>
          <Button onClick={handleClose} color="primary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};
