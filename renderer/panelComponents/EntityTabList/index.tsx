import { Box, ClickAwayListener, IconButton, ListSubheader } from '@mui/material';
import clsx from 'clsx';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import { MdAddToPhotos, MdDeleteForever, MdKeyboardArrowDown, MdKeyboardArrowUp, MdSort } from 'react-icons/md';
import { useDispatch } from 'react-redux';
import { FixedSizeList, ListChildComponentProps } from 'react-window';
import { useKey } from 'rooks';

import { useSelector } from '@@core/redux';
import { EntityState, IEntityFrame, SortFilterProps } from '@@interface';
import { useWindowDimensions } from '@@panelComponents';
import { tabHeaderHeight, tabHeight, useStyles } from '@@style/home';
import { theme } from '@@style/lib/theme';

import { IFilterOption, ISortOption, SortFilterDialog } from './SortFilterDialog';

export const makeSortOptionsFromKeys = (
  rawOps: { legend: string; sortFunctionIds: string[]; labels: string[] }[],
  refs: {
    rootKey: string;
    ref: { legend: string; values: Readonly<string[]>; labels?: string[] };
  }[]
): ISortOption[] => {
  return rawOps.concat(
    refs.map(({ rootKey, ref }) => {
      const { legend, values } = ref;
      const labels = ref.hasOwnProperty('labels') ? ref.labels : [...values];
      return {
        legend,
        sortFunctionIds: values.map(value => {
          const [key, ...nestedKeys] = rootKey.split(':');
          return [key + '.' + value, ...nestedKeys].join(':');
        }),
        labels,
      };
    })
  );
};

export const makeFilterOptions = (
  refs: { key: string[]; ref: { legend: string; values: Readonly<string[]> } }[]
): IFilterOption[] => {
  return refs.map(({ key, ref: { legend, values } }) => {
    return { key: key.join('.'), legend, options: values };
  });
};

const scrollButtonHeight = 42;
const overscanCount = 8;
const smoothScrollCount = 24;
const smoothScrollThreshold = smoothScrollCount * tabHeight;

type EntityTabListProps = {
  entities: EntityState<IEntityFrame>;
  entityId: string | false;
  entityIdBuffer: string | false;
  setEntityId: (id: string | false, ...args: unknown[]) => void;
  setEntityIdBuffer: (id: string | false) => void;
  sortFilterProps: SortFilterProps;
  setSortFilterPropsFunc: (sortFilterProps: SortFilterProps) => void;
  sortOptions: ISortOption[];
  filterOptions: IFilterOption[];
  AddEntityDialog: React.FC<{ open: boolean; handleClose: () => void; setEntityId: (id: string) => void }>;
  extraPropsAdd?: Record<string, unknown>;
  deleteEntity: () => void;
  TabIcon: React.FC<{ entity: IEntityFrame }>;
  resolveEntityIdMismatch?: boolean;
};

export const EntityTabList: React.FC<EntityTabListProps> = props => {
  const classes = useStyles({});
  const {
    entities,
    entityId,
    entityIdBuffer,
    setEntityId,
    setEntityIdBuffer,
    sortFilterProps,
    setSortFilterPropsFunc,
    sortOptions,
    filterOptions,
    AddEntityDialog,
    extraPropsAdd,
    deleteEntity,
    TabIcon,
    resolveEntityIdMismatch = true,
  } = props;
  const length = entities.ids.length;

  const dispatch = useDispatch();
  const mounted = useSelector(state => state.present.ui.metaFlags.mounted);
  const setSortFilterProps = (newSortFilterProps: SortFilterProps) =>
    dispatch(setSortFilterPropsFunc(newSortFilterProps));
  const outerRef = useRef(null);

  const [openAddEntityDialog, setOpenAddEntityDialog] = useState(false);
  const [openSortFilterDialog, setOpenSortFilterDialog] = useState(false);
  const [focused, setFocused] = useState(false);
  const [selectedIndex, setSelectedIndex] = useState(0);
  const height = useWindowDimensions().height - 2 * scrollButtonHeight - tabHeaderHeight;
  const overflowY = length * tabHeight - height;
  const [atEndpoint, setAtEndpoint] = useState({ top: true, bottom: false });

  const onScroll = () => {
    /**
     * This callback will be set only once when the App is mounted,
     * so using overflowY makes the conditions fixed at the time and the buttons unable to respond to the window resize.
     * It is the reason why `clientHeight`(equal to `height` defined above, but has reference to the changing value in realtime)
     * of the list component is intentionally utilized here.
     */
    const { scrollTop, clientHeight } = outerRef.current;
    if (scrollTop === 0) {
      setAtEndpoint({ top: true, bottom: length * tabHeight - clientHeight <= 0 });
    } else {
      const bottom = scrollTop >= length * tabHeight - clientHeight;
      if (atEndpoint.top || atEndpoint.bottom !== bottom) {
        setAtEndpoint({ top: false, bottom });
      }
    }
  };
  useEffect(() => {
    if (mounted && outerRef && outerRef.current) {
      setAtEndpoint({ top: true, bottom: overflowY <= 0 });
      outerRef.current.addEventListener('scroll', onScroll, false);
      /**
       * Switching the main panel tab eliminates the previous component,
       * so when this cleanup func is called, there will not be the outerRef.current anymore and caused an error.
       */
      // return function cleanup() {
      //   outerRef.current.removeEventListener('scroll', onScroll, false);
      // };
    }
  }, [mounted]);

  useEffect(() => {
    if (!entityId) {
      /**
       * Whenever the current entityId becomes false, restore the previous value memorized in a buffer state.
       * e.g., Deleting an entity, id mismatch caused by sorting or filtering, the time when a new base is added and no layer belongs to it.
       */
      if (entityIdBuffer && entities.ids.includes(entityIdBuffer)) {
        setEntityId(entityIdBuffer);
      } else if (resolveEntityIdMismatch) {
        if (length) {
          setEntityId(entities.ids[0]);
        } else {
          setEntityId(false);
        }
      }
      setEntityIdBuffer(false);
    } else {
      /**
       * Automated smooth scrolling which guarantees a selected entity can be always displayed in a list.
       */
      const top = tabHeight * (focused ? selectedIndex : entities.ids.indexOf(entityId));
      const { scrollTop } = outerRef.current;
      const diff = top - scrollTop;
      if (diff < 0) {
        outerRef.current.scrollTo({
          left: 0,
          top,
          behavior: -diff <= smoothScrollThreshold ? 'smooth' : 'auto',
        });
      } else {
        const bottomOffset = tabHeight - height;
        if (diff + bottomOffset > 0) {
          outerRef.current.scrollTo({
            left: 0,
            top: top + bottomOffset,
            behavior: diff + bottomOffset <= smoothScrollThreshold ? 'smooth' : 'auto',
          });
        }
      }
    }
  }, [entityId]);

  /**
   * Row component for virtualized tab list.
   */
  const renderRow = (props: ListChildComponentProps) => {
    const { index, style } = props;
    const id = entities.ids[index];
    const classes = useStyles({});
    const selected = entityId === id;

    return (
      <Box
        key={index}
        className={classes.listItemVirtualized}
        style={{ ...style, opacity: selected ? 1 : 0.6, cursor: 'pointer' }}
        onClick={() => {
          setSelectedIndex(index);
          setEntityId(id);
        }}>
        <Box
          className={clsx(classes.tabIconLabelWrapperVirtualized, classes.defaultFontSize)}
          style={{
            ...(selected && { borderRight: `2px solid ${theme.palette.primary.main}` }),
          }}>
          <Box style={{ margin: '-2px 0px 2px 6px' }}>
            <TabIcon entity={entities.entities[id]} />
          </Box>
          <Box style={{ margin: '8px 0px -8px 1px' }}>{id}</Box>
        </Box>
      </Box>
    );
  };

  /**
   * Key down event listners for scrolling tab list.
   * */
  const moveTab = (step: number) => () => {
    if (focused && entityId) {
      const { ids } = entities;
      const next = (selectedIndex + step + length) % length;
      setSelectedIndex(next);
      setEntityId(ids[next]);
    }
  };

  useKey('ArrowUp', moveTab(-1));
  useKey('ArrowDown', moveTab(1));

  const listHeader = (
    <ListSubheader component="div" className={classes.listHeader}>
      {useMemo(() => {
        return (
          <IconButton
            onClick={() => setOpenAddEntityDialog(true)}
            className={clsx(classes.groupedButton, classes.insideShadowDark)}
            component="span"
            color="inherit"
            size="large">
            <MdAddToPhotos fontSize={13} />
          </IconButton>
        );
      }, [])}
      <IconButton
        onClick={deleteEntity}
        className={clsx(classes.groupedButton, classes.insideShadowDark)}
        style={{ borderTopRightRadius: 0, borderBottomRightRadius: 0 }}
        color="inherit"
        disabled={entityId ? entities.entities[entityId]?.undeletable : true}
        size="large">
        <MdDeleteForever fontSize={15} style={{ marginBottom: -2, marginLeft: 2, marginRight: -2 }} />
      </IconButton>
      {useMemo(() => {
        return (
          <IconButton
            onClick={() => setOpenSortFilterDialog(true)}
            className={clsx(classes.groupedButton, classes.insideShadowDark)}
            style={{ borderTopLeftRadius: 0, borderBottomLeftRadius: 0 }}
            color="inherit"
            size="large">
            <MdSort fontSize={15} style={{ marginBottom: -2, marginLeft: -1, marginRight: 1 }} />
          </IconButton>
        );
      }, [])}
    </ListSubheader>
  );

  /**
   * Rendering.
   */
  return (
    <>
      {openAddEntityDialog ? (
        <AddEntityDialog
          open={openAddEntityDialog}
          handleClose={() => setOpenAddEntityDialog(false)}
          setEntityId={(id: string, ...args: unknown[]) => {
            setEntityId(id, ...args);
          }}
          {...{ extra: extraPropsAdd }}
        />
      ) : null}
      {openSortFilterDialog ? (
        <SortFilterDialog
          open={openSortFilterDialog}
          handleClose={() => setOpenSortFilterDialog(false)}
          sortFilterProps={sortFilterProps}
          setSortFilterProps={setSortFilterProps}
          sortOptions={sortOptions}
          filterOptions={filterOptions}
          bufferEntityId={() => {
            if (entityId) {
              setEntityIdBuffer(entityId);
            }
            setEntityId(false);
          }}
        />
      ) : null}
      <Box
        className={clsx(classes.listBasement, classes.paperBackground, classes.borderLeftRightPanelColor)}
        style={{ margin: '0px 2px 0px 0px' }}>
        {listHeader}

        {/** Virtualized EntityTabList */}
        <ClickAwayListener
          onClickAway={() => {
            setFocused(false);
          }}>
          <Box
            className={clsx(classes.paperBackground, classes.list)}
            style={{
              borderBottomLeftRadius: 50,
              borderBottomRightRadius: 50,
              boxShadow: theme.shadows[24],
            }}
            onClick={() => {
              setFocused(true);
            }}>
            <Box height={scrollButtonHeight} display="flex" style={{ alignItems: 'center', justifyContent: 'center' }}>
              <MdKeyboardArrowUp color={atEndpoint.top ? 'transparent' : '#aaaaaa'} />
            </Box>
            <FixedSizeList
              outerRef={outerRef}
              overscanCount={overscanCount}
              height={height}
              width={181}
              itemSize={tabHeight}
              itemCount={length}>
              {renderRow}
            </FixedSizeList>
            <Box height={scrollButtonHeight} display="flex" style={{ alignItems: 'center', justifyContent: 'center' }}>
              <MdKeyboardArrowDown color={atEndpoint.bottom ? 'transparent' : '#aaaaaa'} />
            </Box>
          </Box>
        </ClickAwayListener>
      </Box>
    </>
  );
};
