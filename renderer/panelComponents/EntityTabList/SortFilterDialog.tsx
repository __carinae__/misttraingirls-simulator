import { TabContext, TabList, TabPanel } from '@mui/lab';
import {
  Box,
  Button,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  Radio,
  Tab,
} from '@mui/material';
import clsx from 'clsx';
import React, { useState } from 'react';

import { arrayOverrideMerge } from '@@core/utils';
import { SortFilterProps } from '@@interface';
import { useStyles } from '@@style/home';
import { theme } from '@@style/lib/theme';

export type ISortOption = { legend: string; sortFunctionIds: string[]; labels: string[] };
export type IFilterOption = { key: string; legend: string; options: Readonly<string[]> };

const labelMinWidth = 120;

export type SortFilterDialogProps = {
  open: boolean;
  handleClose: () => void;
  sortFilterProps: SortFilterProps;
  setSortFilterProps: (sortFilterProps: SortFilterProps) => void;
  sortOptions: ISortOption[];
  filterOptions: IFilterOption[];
  bufferEntityId: () => void;
};
export const SortFilterDialog: React.FC<SortFilterDialogProps> = props => {
  const classes = useStyles({});
  const { open, handleClose, sortFilterProps, setSortFilterProps, sortOptions, filterOptions, bufferEntityId } = props;
  const [propsInEditing, setPropsInEditing] = useState(sortFilterProps);
  const [tab, setTab] = useState('sort');
  const numCols = Math.max(sortOptions.length + 1, filterOptions.length);

  return (
    <Dialog
      open={open}
      onClose={() => {
        handleClose();
        setPropsInEditing(sortFilterProps);
      }}
      scroll="paper"
      maxWidth="lg">
      <TabContext value={tab}>
        <TabList
          selectionFollowsFocus
          value={tab}
          onChange={(event: React.ChangeEvent<unknown>, value: string) => setTab(value)}
          textColor="primary"
          orientation="horizontal"
          aria-label="sort filter label tabs"
          classes={{ indicator: classes.sortFilterTabIndicator }}
          className={clsx(classes.paperBackground, classes.dialogTitleWindow)}>
          <Tab value="sort" disableRipple label="sort" />
          <Tab value="filter" disableRipple label="filter" />
        </TabList>
        <Box className={classes.paperBackground}>
          <DialogContent
            dividers
            style={{
              width: (labelMinWidth + 26) * numCols + 58,
              height: 385,
              paddingBottom: 0,
              borderRadius: 50,
              boxShadow: theme.shadows[24],
            }}>
            <TabPanel value="sort" style={{ padding: 0, margin: 0 }}>
              <Box display="flex" flexDirection="row">
                <Box style={{ margin: '0px 1px 0px 1px' }}>
                  <FormControl component="fieldset" className={classes.sortOrderGroup} variant="standard">
                    <FormLabel component="legend" className={classes.defaultFontSize}>
                      {'　'}
                    </FormLabel>
                    <br />
                    <FormGroup>
                      <FormControlLabel
                        control={
                          <Radio
                            checked={propsInEditing.sortOrder === 'asc'}
                            onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                              setPropsInEditing(
                                arrayOverrideMerge<SortFilterProps>(propsInEditing, {
                                  sortOrder: event.target.value as 'asc' | 'desc',
                                })
                              )
                            }
                            value="asc"
                            className={classes.smallRadioAndCheckbox}
                          />
                        }
                        label="昇順"
                        style={{ minWidth: labelMinWidth }}
                        classes={{ label: classes.defaultFontSize }}
                      />
                      <FormControlLabel
                        control={
                          <Radio
                            checked={propsInEditing.sortOrder === 'desc'}
                            onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                              setPropsInEditing(
                                arrayOverrideMerge<SortFilterProps>(propsInEditing, {
                                  sortOrder: event.target.value as 'asc' | 'desc',
                                })
                              )
                            }
                            value="desc"
                            className={classes.smallRadioAndCheckbox}
                          />
                        }
                        label="降順"
                        style={{ minWidth: labelMinWidth }}
                        classes={{ label: classes.defaultFontSize }}
                      />
                    </FormGroup>
                  </FormControl>
                </Box>
                {sortOptions.map(sortOption => {
                  const { legend, sortFunctionIds, labels } = sortOption;
                  return (
                    <FormControl
                      component="fieldset"
                      key={legend}
                      className={clsx(classes.sortFilterFormGroup, classes.borderDividerColor)}
                      style={{ boxShadow: theme.shadows[6] }}
                      variant="standard">
                      <FormLabel component="legend" className={classes.defaultFontSize}>
                        {legend}
                      </FormLabel>
                      <br />
                      <FormGroup style={{ paddingBottom: 0 }}>
                        {sortFunctionIds.map((sortFunctionId, j) => {
                          return (
                            <FormControlLabel
                              key={sortFunctionId}
                              control={
                                <Radio
                                  checked={propsInEditing.sortFunctionId === sortFunctionId}
                                  onChange={() =>
                                    setPropsInEditing(
                                      arrayOverrideMerge<SortFilterProps>(propsInEditing, { sortFunctionId })
                                    )
                                  }
                                  name={labels[j]}
                                  className={classes.smallRadioAndCheckbox}
                                />
                              }
                              label={labels[j]}
                              style={{ minWidth: labelMinWidth }}
                              classes={{ label: classes.defaultFontSize }}
                            />
                          );
                        })}
                      </FormGroup>
                    </FormControl>
                  );
                })}
              </Box>
            </TabPanel>
            <TabPanel value="filter" style={{ padding: 0, margin: 0 }}>
              {filterOptions.map(filterOption => {
                const { key, legend, options } = filterOption;
                return (
                  <FormControl
                    component="fieldset"
                    key={legend}
                    className={clsx(classes.sortFilterFormGroup, classes.borderDividerColor)}
                    style={{ boxShadow: theme.shadows[6] }}
                    variant="standard">
                    <FormLabel component="legend" className={classes.defaultFontSize}>
                      {legend}
                    </FormLabel>
                    <br />
                    <FormGroup style={{ paddingBottom: 0 }}>
                      {options.map(option => {
                        return (
                          <FormControlLabel
                            key={option}
                            checked={(() => {
                              const { filters } = propsInEditing;
                              if (filters.hasOwnProperty(key)) {
                                return filters[key][option] ?? false;
                              } else {
                                return false;
                              }
                            })()}
                            control={
                              <Checkbox
                                onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                                  const { checked } = event.target;
                                  if (checked) {
                                    setPropsInEditing(
                                      arrayOverrideMerge<SortFilterProps>(propsInEditing, {
                                        filters: { [key]: { [option]: true } },
                                      })
                                    );
                                  } else {
                                    const { filters, ...props } = propsInEditing;
                                    const { [option]: {} = {}, ...options } = filters[key];
                                    setPropsInEditing({ ...props, filters: { ...filters, [key]: options } });
                                  }
                                }}
                                className={classes.smallRadioAndCheckbox}
                              />
                            }
                            label={option}
                            style={{ minWidth: labelMinWidth }}
                            classes={{ label: classes.defaultFontSize }}
                          />
                        );
                      })}
                    </FormGroup>
                  </FormControl>
                );
              })}
            </TabPanel>
          </DialogContent>
        </Box>
      </TabContext>
      <DialogActions className={clsx(classes.paperBackground, classes.dialogActionWindow)}>
        <Button
          onClick={() => {
            handleClose();
            setPropsInEditing(sortFilterProps);
          }}
          color="primary">
          Cancel
        </Button>
        <Button
          onClick={() => {
            setSortFilterProps(propsInEditing);
            bufferEntityId();
            handleClose();
          }}
          color="primary">
          OK
        </Button>
      </DialogActions>
    </Dialog>
  );
};
