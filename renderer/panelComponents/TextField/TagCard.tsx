import { Box, FormControl, Input, InputLabel } from '@mui/material';
import clsx from 'clsx';
import React, { useMemo } from 'react';

import { useStyles } from '@@style/home';
import { theme } from '@@style/lib/theme';

type TagCardContainerProps = {
  children: React.ReactNode;
  label: string;
  highlight?: boolean;
  inGroup?: boolean;
  half?: boolean;
  contentHalf?: boolean;
  transparent?: boolean;
  panelColor?: boolean;
  disableLabel?: boolean;
};

export const TagCardContainer = (props: TagCardContainerProps): JSX.Element => {
  const classes = useStyles({});
  const {
    children,
    label,
    highlight = false,
    inGroup = false,
    half = false,
    contentHalf = false,
    transparent = false,
    panelColor = false,
    disableLabel = false,
  } = props;

  return (
    <Box style={{ margin: 0, padding: 0 }} flexDirection="column" display="flex">
      <Box
        className={
          inGroup
            ? classes.tagCardInGroup
            : clsx(
                half ? classes.tagCardHalf : classes.tagCard,
                highlight ? classes.borderHighlightColor : classes.borderBackgroundColor,
                panelColor ? classes.panelBackground : classes.highlightedPanelBackground,
                !transparent && classes.insideShadow
              )
        }
        style={transparent ? { backgroundColor: 'transparent', borderColor: 'transparent' } : {}}>
        <FormControl variant="standard" className={half || contentHalf ? classes.formControlHalf : classes.formControl}>
          {disableLabel ? null : <InputLabel className={classes.inputLabel}>{label}</InputLabel>}
          {children}
        </FormControl>
      </Box>
    </Box>
  );
};

type TagCardProps = {
  value: string | number;
  label: string;
  startAdornment?: string | React.ReactNode;
  endAdornment?: string | React.ReactNode;
  highlight?: boolean;
  inGroup?: boolean;
  half?: boolean;
  contentHalf?: boolean;
  transparent?: boolean;
  panelColor?: boolean;
  disableLabel?: boolean;
  textAlign?: 'start' | 'end' | 'left' | 'right' | 'center' | 'justify' | 'match-parent';
};

export const TagCard = (props: TagCardProps): JSX.Element => {
  const classes = useStyles({});
  const { value, startAdornment, endAdornment, highlight = false, textAlign = 'center' as const } = props;

  return useMemo(
    () => (
      <TagCardContainer {...props}>
        <Input
          size="small"
          className={classes.defaultFontSize}
          value={value != null ? value.toString() : ''}
          readOnly
          inputProps={{ style: { textAlign } }}
          style={{ paddingTop: 2 }}
          sx={highlight ? { color: theme.palette.primary.main, fontWeight: 700 } : {}}
          disableUnderline
          startAdornment={
            startAdornment ? <div style={{ paddingRight: 6, marginBottom: 2.5 }}>{startAdornment}</div> : null
          }
          endAdornment={endAdornment ? <div style={{ paddingRight: 6 }}>{endAdornment}</div> : null}
        />
      </TagCardContainer>
    ),
    [value]
  );
};

export const DummyCard: React.FC = props => {
  const classes = useStyles({});
  const { children } = props;

  return (
    <Box style={{ margin: 0, padding: 0 }} flexDirection="column" display="flex">
      <Box className={classes.dummyCard}>{children}</Box>
    </Box>
  );
};
