import { Box, InputLabel } from '@mui/material';
import clsx from 'clsx';
import React from 'react';

import { useStyles } from '@@style/home';
import { theme } from '@@style/lib/theme';

type MonitorPropertyGroupProps = {
  label: string;
  labelColor?: string;
  highlight?: boolean;
  transparent?: boolean;
  height?: number | string;
  maxHeight?: number | string;
};
export const MonitorPropertyGroup: React.FC<MonitorPropertyGroupProps> = props => {
  const classes = useStyles({});
  const { label, labelColor, children, highlight = false, transparent = false, height, maxHeight } = props;

  return (
    <Box style={{ margin: 0, padding: 0 }} flexDirection="column" display="flex">
      <Box
        className={clsx(classes.monitorPropertyGroup, classes.insideShadow)}
        style={transparent ? { backgroundColor: 'transparent' } : {}}>
        <InputLabel
          className={classes.defaultFontSize}
          style={{ margin: '-6px 0px 6px 6px', fontSize: 8, textAlign: 'left' }}
          sx={{
            fontWeight: 200,
            ...(labelColor ? { color: labelColor } : highlight && { color: theme.palette.primary.main }),
          }}>
          {label}
        </InputLabel>
        <Box
          style={{
            margin: 0,
            padding: 0,
            overflowX: 'visible',
            overflowY: 'scroll',
            height: height !== null ? height : '100%',
            maxHeight: maxHeight !== null ? maxHeight : '100%',
          }}
          flexDirection="column"
          display="flex">
          {children}
        </Box>
      </Box>
    </Box>
  );
};
