import React from 'react';

import { theme } from '@@style/lib/theme';

type TagBadgeProps = {
  value: number | string;
};
export const TagBadge: React.FC<TagBadgeProps> = props => {
  const { value } = props;

  return (
    <div
      onClick={() => {
        console.log('badge');
      }}
      style={{
        borderRadius: '9px 4px 4px 9px',
        height: 18,
        width: 21,
        fontSize: 11,
        fontWeight: 600,
        textAlign: 'center',
        padding: '2px 0px 0px 1.5px',
        margin: '-20px 0px 20px -18px',
        color: theme.palette.background.default,
        backgroundColor: theme.palette.secondary.main,
      }}>
      {value}
    </div>
  );
};
