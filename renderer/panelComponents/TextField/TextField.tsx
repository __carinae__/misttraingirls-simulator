import { Box, FormControl, Input, InputLabel } from '@mui/material';
import clsx from 'clsx';
import React, { useMemo } from 'react';

import { verifyInputNumber } from '@@core/utils';
import { useStyles } from '@@style/home';

type CustomTextFieldProps = {
  value: string | number;
  label: string;
  startAdornment?: string | React.ReactNode;
  endAdornment?: string | React.ReactNode;
  setState?: (value: string) => void;
  highlight?: boolean;
  inGroup?: boolean;
  half?: boolean;
  contentHalf?: boolean;
  transparent?: boolean;
  panelColor?: boolean;
  disableLabel?: boolean;
  multiline?: boolean;
  maxRows?: number;
  readOnly?: boolean;
  autoFocus?: boolean;
};
export const CustomTextField = (props: CustomTextFieldProps): JSX.Element => {
  const classes = useStyles({});
  const {
    value,
    label,
    startAdornment,
    endAdornment,
    setState = () => null,
    highlight = false,
    inGroup = false,
    half = false,
    contentHalf = false,
    transparent = false,
    panelColor = false,
    disableLabel = false,
    multiline = false,
    maxRows = undefined,
    readOnly = false,
    autoFocus = false,
  } = props;

  return useMemo(
    () => (
      <Box style={{ margin: 0, padding: 0 }} flexDirection="column" display="flex" flexGrow={1}>
        <Box
          className={
            inGroup
              ? classes.tagCardInGroup
              : clsx(
                  half ? classes.tagCardHalf : classes.tagCard,
                  panelColor ? classes.panelBackground : classes.highlightedPanelBackground,
                  highlight ? classes.borderHighlightColor : classes.borderBackgroundColor,
                  !transparent && classes.insideShadow
                )
          }
          style={transparent ? { backgroundColor: 'transparent', borderColor: 'transparent' } : { height: '100%' }}>
          <FormControl
            variant="standard"
            className={half || contentHalf ? classes.formControlHalf : classes.formControl}>
            {disableLabel ? null : <InputLabel className={classes.inputLabel}>{label}</InputLabel>}
            <Input
              size="small"
              autoFocus={autoFocus}
              multiline={multiline}
              maxRows={maxRows}
              className={classes.defaultFontSize}
              classes={{ underline: classes.underline }}
              inputProps={{ style: { textAlign: 'justify' } }}
              value={value != null ? value.toString() : ''}
              onChange={event => setState(event.target.value.trim())}
              style={{ paddingTop: 2 }}
              readOnly={readOnly}
              disableUnderline={readOnly}
              startAdornment={
                startAdornment ? <div style={{ paddingRight: 6, marginBottom: 3 }}>{startAdornment}</div> : null
              }
              endAdornment={endAdornment ? <div style={{ paddingRight: 6 }}>{endAdornment}</div> : null}
            />
          </FormControl>
        </Box>
      </Box>
    ),
    [value, readOnly]
  );
};

type NumberFieldProps = {
  value: number;
  label: string | React.ReactNode;
  startAdornment?: string | React.ReactNode;
  endAdornment?: string | React.ReactNode;
  setState?: (value: number) => void;
  inputTypeInteger?: boolean;
  maxValue?: number;
  minValue?: number;
  highlight?: boolean;
  inGroup?: boolean;
  half?: boolean;
  contentHalf?: boolean;
  transparent?: boolean;
  panelColor?: boolean;
  disableLabel?: boolean;
  readOnly?: boolean;
  autoFocus?: boolean;
};
export const NumberField = (props: NumberFieldProps): JSX.Element => {
  const classes = useStyles({});
  const {
    value,
    label,
    startAdornment,
    endAdornment,
    setState = () => null,
    inputTypeInteger = false,
    maxValue = Infinity,
    minValue = -Infinity,
    highlight = false,
    inGroup = false,
    half = false,
    contentHalf = false,
    transparent = false,
    panelColor = false,
    disableLabel = false,
    readOnly = false,
    autoFocus = false,
  } = props;

  return useMemo(
    () => (
      <Box style={{ margin: 0, padding: 0 }} flexDirection="column" display="flex" flexGrow={1}>
        <Box
          className={
            inGroup
              ? classes.tagCardInGroup
              : clsx(
                  half ? classes.tagCardHalf : classes.tagCard,
                  panelColor ? classes.panelBackground : classes.highlightedPanelBackground,
                  highlight ? classes.borderHighlightColor : classes.borderBackgroundColor,
                  !transparent && classes.insideShadow
                )
          }
          style={transparent ? { backgroundColor: 'transparent', borderColor: 'transparent' } : {}}>
          <FormControl
            variant="standard"
            className={half || contentHalf ? classes.formControlHalf : classes.formControl}>
            {disableLabel ? null : <InputLabel className={classes.inputLabel}>{label}</InputLabel>}
            <Input
              size="small"
              autoFocus={autoFocus}
              className={classes.defaultFontSize}
              classes={{ underline: classes.underline }}
              value={value != null ? value.toString() : ''}
              inputProps={{ style: { textAlign: 'center' } }}
              onChange={event => {
                const value = Number(event.target.value);
                if (verifyInputNumber(value, { inputTypeInteger, minValue, maxValue })) {
                  setState(value);
                }
              }}
              style={{ paddingTop: 2 }}
              startAdornment={
                startAdornment ? <div style={{ paddingRight: 6, marginBottom: 3 }}>{startAdornment}</div> : null
              }
              endAdornment={endAdornment ? <div style={{ paddingLeft: 6 }}>{endAdornment}</div> : null}
              readOnly={readOnly}
              disableUnderline={readOnly}
            />
          </FormControl>
        </Box>
      </Box>
    ),
    [value, readOnly]
  );
};
