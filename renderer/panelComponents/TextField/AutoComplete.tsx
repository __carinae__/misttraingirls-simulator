import { Autocomplete, Box } from '@mui/material';
import { inputLabelClasses } from '@mui/material/InputLabel';
import { styled } from '@mui/material/styles';
import MuiTextField from '@mui/material/TextField';
import clsx from 'clsx';
import React, { useState } from 'react';
import { FixedSizeList, ListChildComponentProps } from 'react-window';
import { useKeys } from 'rooks';
import useUndo from 'use-undo';

import { useStyles } from '@@style/home';
import { theme } from '@@style/lib/theme';

const renderRow = (props: ListChildComponentProps) => {
  const { data, index, style } = props;
  return React.cloneElement(data[index], {
    style: {
      ...style,
      width: 1000,
      fontSize: 9.5,
    },
  });
};
const OuterElementContext = React.createContext({});
const OuterElementType = React.forwardRef<HTMLDivElement>(function OuterElementType(props, ref) {
  const outerProps = React.useContext(OuterElementContext);
  return <Box ref={ref} {...props} {...outerProps} />;
});

// Adapter for react-window
const ListboxComponent = React.forwardRef<HTMLDivElement>(function ListboxComponent(props, ref) {
  const { children, ...other } = props;
  const itemData = React.Children.toArray(children);
  const itemCount = itemData.length;
  const itemSize = 27;

  const getHeight = () => {
    if (itemCount > 10) {
      return 10 * itemSize;
    }
    return itemData.length * itemSize;
  };

  return (
    <Box ref={ref}>
      <OuterElementContext.Provider value={other}>
        <FixedSizeList
          overscanCount={5}
          itemData={itemData}
          height={getHeight()}
          width="calc(100% + 10px)"
          style={{ marginLeft: -10, padding: 0, overflowX: 'hidden' }}
          innerElementType="ul"
          outerElementType={OuterElementType}
          itemSize={itemSize}
          itemCount={itemCount}>
          {renderRow}
        </FixedSizeList>
      </OuterElementContext.Provider>
    </Box>
  );
});

const TextField = styled(MuiTextField)(`
  .${inputLabelClasses.root} {
    font-size: 10px;
  }
`);

type CustomAutoCompleteProps = {
  label: string;
  setState: (value: string) => void;
  startAdornment?: string | React.ReactNode;
  endAdornment?: string | React.ReactNode;
  highlight?: boolean;
  inGroup?: boolean;
  half?: boolean;
  contentHalf?: boolean;
  badge?: React.ReactNode;
  transparent?: boolean;
  panelColor?: boolean;
  freeSolo?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
  showError?: boolean;
  autoFocus?: boolean;
};

export const CustomAutoComplete = (
  props: CustomAutoCompleteProps & { value: string; options: string[] }
): JSX.Element => {
  const classes = useStyles({});
  const {
    options,
    value,
    label,
    setState = () => null,
    endAdornment,
    startAdornment,
    highlight = false,
    inGroup = false,
    half = false,
    contentHalf = false,
    transparent = false,
    panelColor = false,
    badge,
    freeSolo = false,
    disabled = false,
    readOnly = false,
    showError = false,
    autoFocus = false,
  } = props;
  const valueStr = value != null ? value.toString() : '';

  const [
    { present: inputValue },
    { set: setInputValue, undo: undoInputValue, redo: redoInputValue, reset: resetInputValue },
  ] = useUndo(valueStr);
  const [isFocused, setIsFocused] = useState(false);

  const error = showError && !options.includes(value);

  useKeys(['Meta', 'z'], event => {
    if (freeSolo && isFocused) {
      event.preventDefault();
      if (event.shiftKey) {
        /** Redo input state */
        redoInputValue();
      } else {
        /** Undo input state */
        undoInputValue();
      }
    }
  });

  return (
    <Box style={{ margin: 0, padding: 0 }} flexDirection="column" display="flex">
      <Box
        display="flex"
        flexDirection="row"
        className={
          inGroup
            ? classes.tagCardInGroup
            : clsx(
                half ? classes.tagCardHalf : classes.tagCard,
                panelColor ? classes.panelBackground : classes.highlightedPanelBackground,
                classes.borderBackgroundColor,
                !transparent && classes.insideShadow
              )
        }
        style={transparent ? { backgroundColor: 'transparent', borderColor: 'transparent' } : {}}>
        <Autocomplete
          size="small"
          options={options}
          noOptionsText={''}
          ListboxComponent={ListboxComponent as React.ComponentType<React.HTMLAttributes<HTMLElement>>}
          className={
            contentHalf ? classes.formControlHalfAutocomplete : half ? classes.formControlHalf : classes.formControl
          }
          classes={{
            input: classes.defaultFontSize,
            option: clsx(classes.defaultFontSize, classes.autocompleteOption),
            groupLabel: classes.defaultFontSize,
          }}
          inputValue={inputValue}
          onInputChange={(event, newValue) => {
            setInputValue(newValue.trim());
          }}
          value={valueStr}
          onChange={(event, newValue) => {
            if (!freeSolo) setState(newValue.trim());
          }}
          onFocus={() => {
            setIsFocused(true);
            if (freeSolo) resetInputValue(valueStr);
          }}
          // Just setState when bluring the focus, so that the global undo history does not conflict with the local one.
          onBlur={() => {
            setIsFocused(false);
            if (valueStr !== inputValue) {
              setState(inputValue);
            }
          }}
          disableClearable
          freeSolo={freeSolo}
          disabled={disabled || readOnly}
          renderInput={params => {
            return (
              <TextField
                {...params}
                disabled={false} // To avoid the gray out layout.
                label={label}
                size="small"
                error={error}
                variant="standard"
                inputProps={{
                  ...params.inputProps,
                  style: {
                    textAlign: 'center',
                    ...(highlight && { color: theme.palette.primary.main, fontWeight: 600 }),
                  },
                }}
                InputProps={{
                  ...params.InputProps,
                  autoFocus,
                  style: { paddingRight: 0, paddingTop: 2 },
                  classes: { underline: classes.underline },
                  startAdornment: startAdornment ? <Box style={{ paddingRight: 6 }}>{startAdornment}</Box> : null,
                  endAdornment: endAdornment ? <Box style={{ paddingLeft: 6 }}>{endAdornment}</Box> : null,
                  readOnly, // To enable the proper disabled actions.
                  disableUnderline: readOnly && !error,
                }}
              />
            );
          }}
        />
        {badge}
      </Box>
    </Box>
  );
};

export const CustomNumberComplete = (
  props: CustomAutoCompleteProps & { maxValue: number; minValue: number; step: number; value: number }
): JSX.Element => {
  const { maxValue, minValue, step, value, ...rest } = props;
  const len = Math.ceil((maxValue - minValue) / step) + 1;
  const options = new Array(len).fill(null).map((_, i) => (minValue + (len - 1 - i) * step).toString());
  return <CustomAutoComplete value={value.toString()} {...rest} options={options} />;
};

export const BadgeAutoComplete = (
  props: CustomAutoCompleteProps & { value: string; options: string[] }
): JSX.Element => {
  const classes = useStyles({});
  const { options, value, label, setState = () => null, endAdornment, startAdornment, readOnly = false } = props;
  const valueStr = value != null ? value.toString() : '';
  const [inputValue, setInputValue] = useState(valueStr);

  return (
    <Box
      style={{
        borderRadius: '9px 4px 4px 9px',
        height: 18,
        width: 21,
        padding: 0,
        margin: '0px 6.5px 20px -27.5px',
        backgroundColor: theme.palette.primary.main,
        boxShadow: theme.shadows[24],
      }}>
      <Autocomplete
        size="small"
        options={options}
        noOptionsText={''}
        ListboxComponent={ListboxComponent as React.ComponentType<React.HTMLAttributes<HTMLElement>>}
        style={{ padding: 0, margin: '3.75px 0px 0px 2px' }}
        classes={{
          input: classes.defaultFontSize,
          option: clsx(classes.defaultFontSize, classes.autocompleteOption),
        }}
        value={valueStr}
        inputValue={inputValue}
        onInputChange={(event, newValue) => setInputValue(newValue)}
        onChange={(event, newValue) => setState(newValue)}
        disableClearable
        disabled={readOnly}
        renderInput={params => {
          return (
            <TextField
              {...params}
              disabled={false} // To avoid the gray out layout.
              label={label}
              size="small"
              variant="standard"
              inputProps={{
                ...params.inputProps,
                style: {
                  textAlign: 'center',
                  fontSize: 10,
                  fontWeight: 600,
                  color: theme.palette.background.default,
                  width: 15,
                },
              }}
              InputProps={{
                ...params.InputProps,
                style: { padding: 0, margin: '-3px 0px 0px -6px', width: 28 },
                classes: { underline: classes.underline },
                startAdornment: startAdornment ? <Box style={{ paddingRight: 6 }}>{startAdornment}</Box> : null,
                endAdornment: endAdornment ? <Box style={{ paddingLeft: 6 }}>{endAdornment}</Box> : null,
                disableUnderline: true, // To enable the proper disabled actions.
                readOnly,
              }}
            />
          );
        }}
      />
    </Box>
  );
};

export const BadgeNumberComplete = (
  props: CustomAutoCompleteProps & { maxValue: number; minValue: number; step: number; value: number }
): JSX.Element => {
  const { maxValue, minValue, step, value, ...rest } = props;
  const len = Math.ceil((maxValue - minValue) / step) + 1;
  const options = Array(len)
    .fill(null)
    .map((_, i) => (minValue + (len - 1 - i) * step).toString());
  return (
    <BadgeAutoComplete
      value={value != null ? value.toString() : maxValue.toString()}
      {...rest}
      options={options}
      freeSolo={false}
    />
  );
};
