import { Box, Chip, IconButton, Tab, Tabs } from '@mui/material';
import clsx from 'clsx';
import React from 'react';

import { useStyles } from '@@style/home';
import { theme } from '@@style/lib/theme';

type SwitchableChipListProps = {
  tabId: string | number;
  tabMenu: { value: string; label: string; Icon: React.FC }[];
  onChangeTabList: (event: React.SyntheticEvent<Element, Event>, value: string) => void;
  orientation?: 'vertical' | 'horizontal';
};
export const SwitchableChipList: React.FC<SwitchableChipListProps> = props => {
  const classes = useStyles({});
  const { tabId, tabMenu, onChangeTabList, orientation = 'vertical' } = props;

  switch (orientation) {
    case 'horizontal':
      return (
        <Tabs
          style={{ margin: '0px 0px 0px 0px' }}
          selectionFollowsFocus
          value={tabId}
          onChange={onChangeTabList}
          classes={{ indicator: classes.sortFilterTabIndicator }}
          textColor="primary"
          orientation="horizontal">
          {tabMenu.map(tabProps => {
            const { value, label, Icon } = tabProps;
            const selected = tabId === value;

            return (
              <Tab
                key={value}
                style={{ margin: 0, padding: 0 }}
                icon={
                  <Chip
                    icon={
                      <IconButton
                        className={clsx(classes.groupedButton, classes.insideShadowDivider)}
                        style={{
                          margin: '1px -5px 0px 3px',
                          color: selected ? theme.palette.primary.main : theme.palette.secondary.main,
                        }}
                        disabled={selected}
                        component="span"
                        size="large">
                        <Icon />
                      </IconButton>
                    }
                    label={
                      <Box width={80} style={{ textAlign: 'center', color: selected ? 'white' : 'gray' }}>
                        {label}
                      </Box>
                    }
                    style={{
                      height: selected ? 47 : 46,
                      borderTopLeftRadius: 20,
                      borderTopRightRadius: 20,
                      borderBottomLeftRadius: 0,
                      borderBottomRightRadius: 0,
                    }}
                    className={clsx(classes.paperBackground, selected ? '' : classes.insideShadowDark)}
                  />
                }
                value={value}
                disableRipple
              />
            );
          })}
        </Tabs>
      );
    case 'vertical':
      return (
        <Tabs
          selectionFollowsFocus
          value={tabId}
          onChange={onChangeTabList}
          classes={{ indicator: classes.sortFilterTabIndicator }}
          textColor="primary"
          orientation="vertical">
          {tabMenu.map(tabProps => {
            const { value, label, Icon } = tabProps;
            const selected = tabId === value;

            return (
              <Tab
                key={value}
                style={{ margin: 0, padding: 0 }}
                icon={
                  <Chip
                    icon={
                      selected ? null : (
                        <IconButton
                          className={clsx(classes.groupedButton, classes.insideShadowDivider)}
                          style={{
                            margin: '1px -5px 0px 3px',
                            color: theme.palette.secondary.main,
                          }}
                          component="span"
                          size="large">
                          <Icon />
                        </IconButton>
                      )
                    }
                    onDelete={() => null}
                    deleteIcon={
                      selected ? (
                        <IconButton
                          disabled
                          className={clsx(classes.groupedButton, classes.insideShadowDivider)}
                          style={{
                            margin: '1px 3px 0px -5px',
                            color: theme.palette.primary.main,
                          }}
                          component="span"
                          size="large">
                          <Icon />
                        </IconButton>
                      ) : (
                        <div />
                      )
                    }
                    label={
                      <Box width={80} style={{ textAlign: 'center', color: selected ? 'white' : 'gray' }}>
                        {label}
                      </Box>
                    }
                    style={{ height: 46, borderRadius: 20 }}
                    className={clsx(classes.panelBackground, classes.insideShadowDark)}
                  />
                }
                value={value}
                disableRipple
              />
            );
          })}
        </Tabs>
      );
  }
};
