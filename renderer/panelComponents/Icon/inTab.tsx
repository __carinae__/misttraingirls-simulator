import { Box } from '@mui/system';
import clsx from 'clsx';
import React from 'react';

import { useStyles } from '@@style/home';

type IconInTabProps = {
  children?: React.ReactNode;
  padding?: number | string;
};
export const IconInTab: React.FC<IconInTabProps> = props => {
  const classes = useStyles({});
  const { children, padding = '0px 0px 0px 0px' } = props;
  return (
    <Box
      margin="0px 4px 0px -9px"
      padding={padding}
      width={32}
      height={32}
      display="flex"
      style={{ borderTopLeftRadius: 16, borderBottomLeftRadius: 16, alignItems: 'center', justifyContent: 'center' }}
      className={clsx(classes.insideShadow, classes.highlightedPanelBackground)}>
      {children}
    </Box>
  );
};
