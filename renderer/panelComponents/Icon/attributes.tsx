import React from 'react';

import { Attribute, terms } from '@@interface';
import { IconInTab } from '@@panelComponents';

type AttributeIconProps = {
  attributes: Attribute[];
};
export const AttributeIconInTab: React.FC<AttributeIconProps> = props => {
  const { attributes } = props;
  return (
    <IconInTab padding="0px 0px 1px 8px">
      {attributes.map((attribute, i) => {
        return React.createElement(terms.attributes.Icons[attribute], {
          key: attribute,
          style: { margin: `3px 5px 0px ${-11 * i}px` },
          size: 12,
        });
      })}
    </IconInTab>
  );
};
