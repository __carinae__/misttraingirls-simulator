import React from 'react';
import { MdFlare } from 'react-icons/md';

import { IBase, ILayer, Rarity } from '@@interface';
import { IconInTab } from '@@panelComponents';

const padding = '0px 0px 0px 0px';
const margin = '0px 10px 3px -2px';

export const getRarityColorCode = (rarity: Rarity): string => {
  switch (rarity) {
    case 'A':
      return '#a4733a';
    case 'S':
      return '#cdd1d6';
    case 'SS':
      return '#f8da65';
  }
};

type BaseRarityIconProps = {
  entity: IBase;
  style?: React.CSSProperties;
};
export const BaseRarityIcon: React.FC<BaseRarityIconProps> = props => {
  const { entity, style } = props;
  const { S, SS } = entity.layerIds;
  let rarity = 'A' as Rarity;
  if (Object.keys(S).length) rarity = 'S';
  if (Object.keys(SS).length) rarity = 'SS';
  return <MdFlare size={14} style={{ margin, padding, color: getRarityColorCode(rarity), ...style }} />;
};

type LayerRarityIconProps = {
  entity: ILayer;
  style?: React.CSSProperties;
};
export const LayerRarityIcon: React.FC<LayerRarityIconProps> = props => {
  const { entity, style } = props;
  return <MdFlare size={14} style={{ margin, padding, color: getRarityColorCode(entity.rarity), ...style }} />;
};

export const BaseRarityIconInTab: React.FC<BaseRarityIconProps> = props => {
  return (
    <IconInTab padding="1px 0px 0px 3px">
      <BaseRarityIcon {...props} style={{ margin: 0, padding: 0 }} />
    </IconInTab>
  );
};

export const LayerRarityIconInTab: React.FC<LayerRarityIconProps> = props => {
  return (
    <IconInTab padding="1px 0px 0px 3px">
      <LayerRarityIcon {...props} style={{ margin: 0, padding: 0 }} />
    </IconInTab>
  );
};
