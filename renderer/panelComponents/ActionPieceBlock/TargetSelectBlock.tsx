import { Box } from '@mui/system';
import React from 'react';
import { GiTargeting } from 'react-icons/gi';
import { useDispatch } from 'react-redux';

import { ActionRange, ActionTargetOption, terms } from '@@core/__interface__';
import { actions } from '@@core/redux';
import { ActionPieceBlockFrame, CustomAutoComplete, MonitorPropertyGroup } from '@@panelComponents';

import { ActionPieceBlockSharedProps } from './shared';

type TargetSelectBlockProps = ActionPieceBlockSharedProps & {
  range: ActionRange;
  targetOption: ActionTargetOption;
  onChangeRange: (value: string) => void;
  onChangeTargetOption: (value: string) => void;
};

export const TargetSelectBlock: React.FC<TargetSelectBlockProps> = props => {
  const dispatch = useDispatch();
  const { index, range, targetOption, hundleDeleteBlock, onChangeRange, onChangeTargetOption } = props;

  return (
    <ActionPieceBlockFrame
      index={index}
      Icon={<GiTargeting size={15} style={{ color: '#dddddd' }} />}
      hundleDeleteBlock={hundleDeleteBlock}>
      <MonitorPropertyGroup label={'対象選択'}>
        <Box flexDirection="row" display="flex">
          <CustomAutoComplete
            options={[...terms.actionRanges.values]}
            value={range}
            label={terms.actionRanges.legend}
            setState={value => {
              dispatch(actions.metaFlags.stampState({}));
              onChangeRange(value);
            }}
            half
            inGroup
          />
          <Box style={{ marginLeft: -7 }} />
          <CustomAutoComplete
            options={[...terms.actionTargetOptions.values]}
            value={targetOption}
            label={terms.actionTargetOptions.legend}
            setState={value => {
              dispatch(actions.metaFlags.stampState({}));
              onChangeTargetOption(value);
            }}
            half
            inGroup
          />
        </Box>
      </MonitorPropertyGroup>
    </ActionPieceBlockFrame>
  );
};
