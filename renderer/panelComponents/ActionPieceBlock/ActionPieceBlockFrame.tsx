import { Chip, IconButton, Typography } from '@mui/material';
import { Box } from '@mui/system';
import clsx from 'clsx';
import React from 'react';
import { TiDelete } from 'react-icons/ti';
import { useDispatch } from 'react-redux';

import { actions } from '@@core/redux';
import { convertToRoman } from '@@core/utils';
import { useStyles } from '@@style/home';
import { theme } from '@@style/lib/theme';

type ActionPieceBlockFrameProps = {
  index: number;
  Icon: React.ReactNode;
  hundleDeleteBlock: () => void;
};

export const ActionPieceBlockFrame: React.FC<ActionPieceBlockFrameProps> = props => {
  const classes = useStyles({});
  const dispatch = useDispatch();

  const { children, index, Icon, hundleDeleteBlock } = props;
  return (
    <Box display="flex" flexDirection="row" width="100%">
      {/* Block Tag */}
      <Box
        width={58}
        minWidth={58}
        margin="0px 0px 2px 0px"
        display="flex"
        flexDirection="row"
        style={{
          borderRight: `1px solid ${theme.palette.background.paper}`,
          borderRadius: '10px 0px 0px 10px',
          justifyContent: 'right',
          alignItems: 'center',
        }}
        className={clsx(classes.panelBackground, classes.insideShadowDark)}>
        <Chip
          icon={
            <IconButton
              disabled
              className={clsx(classes.groupedButton, classes.insideShadowDivider)}
              style={{
                margin: '0px -14px 1px 14px',
                color: 'white',
                borderRadius: '20px 0px 0px 10px',
                borderRight: `2px solid ${theme.palette.background.paper}`,
              }}
              component="span"
              size="large">
              <Typography style={{ width: 13, height: 15, fontWeight: 700, color: '#dddddd' }}>
                {convertToRoman(index + 1, false)}
              </Typography>
            </IconButton>
          }
          style={{
            height: 44,
            width: 41,
            borderRadius: '20px 0px 0px 10px',
          }}
          className={clsx(classes.panelBackground, classes.insideShadow)}
        />
      </Box>
      {/* Main Block */}
      <Box
        display="flex"
        flexDirection="row"
        flexGrow={1}
        margin="0px 0px 2px 0px"
        style={{
          borderRadius: '0px 10px 10px 0px',
          alignItems: 'center',
          justifyContent: 'center',
        }}
        className={clsx(classes.highlightedPanelBackground, classes.insideShadow)}>
        <Chip
          icon={
            <IconButton
              disabled
              className={clsx(classes.groupedButton)}
              style={{
                margin: '0px -13px 1px 13px',
                color: 'white',
                borderRadius: '0px 10px 20px 0px',
                borderRight: `2px solid ${theme.palette.background.paper}`,
              }}
              component="span"
              size="large">
              {Icon}
            </IconButton>
          }
          style={{
            height: 44,
            width: 41,
            borderRadius: '0px 10px 20px 0px',
          }}
          className={clsx(classes.panelBackground, classes.insideShadowDark)}
        />
        <Box flexGrow={1} />

        {/* Main Information */}
        {children}

        <Box flexGrow={1} />
        {/* Delete */}
        <IconButton
          className={clsx(classes.groupedButton)}
          style={{
            margin: '0px 0px 0px 0px',
            padding: '0px',
            color: '#909090',
            height: 23,
            width: 23,
            borderRadius: '23px',
          }}
          component="span"
          size="large"
          onClick={() => {
            dispatch(actions.metaFlags.stampState({}));
            hundleDeleteBlock();
          }}>
          <TiDelete size={17} />
        </IconButton>
      </Box>
    </Box>
  );
};
