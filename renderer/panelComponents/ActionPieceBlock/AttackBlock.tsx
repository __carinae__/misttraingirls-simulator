import { Divider, IconButton } from '@mui/material';
import { Box } from '@mui/system';
import clsx from 'clsx';
import React from 'react';
import { CgArrowsExchangeV } from 'react-icons/cg';
import { GiPunch } from 'react-icons/gi';
import { MdAddCircle } from 'react-icons/md';
import { TiDelete } from 'react-icons/ti';
import { useDispatch } from 'react-redux';

import { AttackStatusTag, Attribute, IImpactElment, terms } from '@@core/__interface__';
import { actions } from '@@core/redux';
import { ActionPieceBlockFrame, CustomAutoComplete, MonitorPropertyGroup, NumberField } from '@@panelComponents';
import { useStyles } from '@@style/home';
import { theme } from '@@style/lib/theme';

import { ActionPieceBlockSharedProps } from './shared';

const initialReferenceElm = {
  status: '物攻',
  attribute: '刃',
} as { status?: AttackStatusTag; attribute?: Attribute };

const initialImpactElm = {
  reference: [initialReferenceElm],
  value: 0,
} as IImpactElment;

type AttackBlockProps = ActionPieceBlockSharedProps & {
  impact: {
    reference?: {
      status?: AttackStatusTag;
      attribute?: Attribute;
    }[];
    value?: number;
  }[];
  onChangeImpactValue: (value: number, j: number) => void;
  onChangeAttribute: (value: string, j: number, k: number) => void;
  onChangeAttackStatus: (value: string, j: number, k: number) => void;
  hundleAddImpactHolder: (impactElm: IImpactElment) => void;
  hundleAddReferenceElementHolder: (reference: { status?: AttackStatusTag; attribute?: Attribute }, j: number) => void;
  hundleDeleteReferenceElementHolder: (j: number, k: number) => void;
};

export const AttackBlock: React.FC<AttackBlockProps> = props => {
  const classes = useStyles({});
  const dispatch = useDispatch();

  const {
    index,
    impact,
    hundleDeleteBlock,
    onChangeImpactValue,
    onChangeAttribute,
    onChangeAttackStatus,
    hundleAddImpactHolder,
    hundleAddReferenceElementHolder,
    hundleDeleteReferenceElementHolder,
  } = props;

  return (
    <ActionPieceBlockFrame
      index={index}
      Icon={<GiPunch size={15} style={{ color: '#f08080' }} />}
      hundleDeleteBlock={hundleDeleteBlock}>
      {' '}
      {/* Iteration over unique impact values (unique attribute group) */}
      {impact.map((impactElm, j) => {
        const lastElm = impact.length - 1;
        return (
          <Box key={j} flexDirection="row" display="flex" style={{ alignItems: 'center' }}>
            <Box className={classes.formWidthPaperContainer} style={{ marginLeft: j === 0 ? 16 : 0 }}>
              <MonitorPropertyGroup label={'攻撃'} labelColor={'#f08080'}>
                <Divider
                  style={{ margin: '2px 0px 2px 0px', borderBottom: `1.5px solid ${theme.palette.background.default}` }}
                />

                {/* Iteration over selectable attributes */}
                {impactElm.reference.map((ref, k) => {
                  const { status, attribute } = ref;
                  const last = impactElm.reference.length - 1;
                  return (
                    <Box key={k} flexDirection="column" display="flex" style={{ alignItems: 'center' }}>
                      <Box display="flex" flexDirection="row" style={{ justifyContent: 'flex-end' }} width="100%">
                        <IconButton
                          className={clsx(classes.groupedButton)}
                          style={{
                            margin: '3px 0px -20px 0px',
                            padding: '0px',
                            height: 16,
                            width: 14,
                            borderRadius: '16px',
                            zIndex: 1,
                          }}
                          component="span"
                          size="large"
                          onClick={() => {
                            dispatch(actions.metaFlags.stampState({}));
                            hundleDeleteReferenceElementHolder(j, k);
                          }}>
                          <TiDelete size={14} color={'#909090'} />
                        </IconButton>
                      </Box>
                      <Box flexDirection="row" display="flex">
                        <CustomAutoComplete
                          options={[...terms.attackStatusTags.values]}
                          value={status}
                          label={'参照'}
                          setState={(value: string) => {
                            dispatch(actions.metaFlags.stampState({}));
                            onChangeAttackStatus(value, j, k);
                          }}
                          half
                          inGroup
                        />
                        <Box style={{ marginLeft: -7 }} />
                        <CustomAutoComplete
                          options={[...terms.attributes.values]}
                          value={attribute}
                          label={terms.attributes.legend}
                          setState={(value: string) => {
                            dispatch(actions.metaFlags.stampState({}));
                            onChangeAttribute(value, j, k);
                          }}
                          half
                          inGroup
                        />
                      </Box>
                      {k === last ? (
                        /** Put a new attribute reference into a unique impact */
                        <IconButton
                          className={clsx(
                            classes.groupedButton,
                            classes.insideShadowDark,
                            classes.borderBackgroundColor
                          )}
                          style={{
                            margin: '0px 0px -18.5px 0px',
                            padding: '1.5px 0px 2px 0px',
                            height: 16,
                            width: 18,
                            borderRadius: '0px 0px 20px 20px',
                            zIndex: 1,
                          }}
                          component="span"
                          size="large"
                          onClick={() => {
                            dispatch(actions.metaFlags.stampState({}));
                            hundleAddReferenceElementHolder(initialReferenceElm, j);
                          }}>
                          <MdAddCircle size={12} color={'#909090'} />
                        </IconButton>
                      ) : (
                        <CgArrowsExchangeV
                          size={17}
                          color={'#f08080'}
                          style={{ margin: '-10px 0px -6px 0px', zIndex: 1 }}
                        />
                      )}
                    </Box>
                  );
                })}

                <Divider
                  style={{ margin: '2px 0px 2px 0px', borderBottom: `1.5px solid ${theme.palette.background.default}` }}
                />

                <NumberField
                  minValue={0}
                  inputTypeInteger
                  value={impactElm.value}
                  label="威力固有値"
                  inGroup
                  setState={value => {
                    dispatch(actions.metaFlags.stampState({}));
                    onChangeImpactValue(Number(value), j);
                  }}
                />
              </MonitorPropertyGroup>
            </Box>
            {j === lastElm ? (
              /** Add a new impact element */
              <IconButton
                className={clsx(classes.groupedButton, classes.insideShadowDark)}
                style={{
                  margin: '21px 0px 0px -4px',
                  padding: '0px 3px 0px 0px',
                  height: 25,
                  width: 20,
                  borderRadius: '0px 25px 25px 0px',
                  zIndex: 1,
                }}
                component="span"
                size="large"
                onClick={() => {
                  dispatch(actions.metaFlags.stampState({}));
                  hundleAddImpactHolder(initialImpactElm);
                }}>
                <MdAddCircle size={17} color={'#909090'} />
              </IconButton>
            ) : (
              <MdAddCircle size={17} color={'#f08080'} style={{ margin: '21px -10px 0px -10px', zIndex: 1 }} />
            )}
          </Box>
        );
      })}
    </ActionPieceBlockFrame>
  );
};
