import React, { useEffect, useState } from 'react';

import { cacheDomains, Rarity } from '@@interface';
import { useStyles } from '@@style/home';

import { getCacheOrStore } from './getBase64Src';
import { gateway } from './loadGateway';

const { getSDURL, getSDURLWithOnlyBase, getSDURLWithRarity } = cacheDomains.sdGifs;
const { undefinedBaseId, undefinedLayerId } = cacheDomains.sdGifs.default;
const undefinedCacheKey = undefinedBaseId + '.' + undefinedLayerId;

type ImageMotionSDProps = {
  baseId: string;
  rarity: Rarity;
  layerId: string;
  height: number;
};
export const ImageMotionSD: React.FC<ImageMotionSDProps> = props => {
  const classes = useStyles({});
  const [rerenderDetector, setRerenderDetector] = useState(false);

  useEffect(() => {
    if (!gateway.activated) {
      gateway.activateDatabase();
    }
    /**
     * If not for this detector, useEffect does not change any state or props of this component;
     * gateway.activated cannot be re-read properly and the first sdGif won't be displayed
     * (gif can be displayed just after any one of character tabs has been changed).
     * The value itself doesn't matter at all. It must just 'change'.
     */
    setRerenderDetector(!rerenderDetector);
  }, []);

  const { baseId, rarity, layerId, height } = props;
  const cacheKey = baseId + '.' + layerId;
  const [imgError, setImgError] = useState({} as { [_cacheKey: string]: number });

  /** Undisplay switch.
   * At the very first frame of the app launching, until the above useEffect has finished,
   * there is a blank duration where gateway does not refer to the database, which leads to the URL loading to the website.
   * This would not be desirable, so make sure to utilize gateway just after this component is ready to do so.
   */
  if (gateway.activated) {
    if (!(baseId && layerId) || imgError[cacheKey] === 3) {
      if (imgError[undefinedCacheKey]) {
        return <div className={classes.alternativeSDdisplay} />;
      } else {
        return (
          <img
            src={getCacheOrStore(
              gateway,
              ['sdGifs', undefinedCacheKey],
              getSDURL(undefinedBaseId, undefinedLayerId),
              height,
              (src: string) => {
                gateway.saveProperty(['sdGifs', undefinedCacheKey], {
                  id: undefinedCacheKey,
                  src,
                });
              },
              () => setImgError({ ...imgError, [undefinedCacheKey]: 1 })
            )}
            height={height}
            className={classes.imageGrayScale}
            style={{ padding: 0, margin: '0px 0px -3px 0px' }}
          />
        );
      }
    } else {
      let src: string;
      let rejectIndex: number;
      switch (imgError[cacheKey]) {
        case undefined:
          src = getSDURL(baseId, layerId);
          rejectIndex = 1;
          break;
        case 1:
          src = getSDURLWithOnlyBase(baseId, layerId);
          rejectIndex = 2;
          break;
        case 2:
          src = getSDURLWithRarity(baseId, rarity, layerId);
          rejectIndex = 3;
          break;
      }
      return (
        <img
          src={getCacheOrStore(
            gateway,
            ['sdGifs', cacheKey],
            src,
            height,
            (src: string) => {
              gateway.saveProperty(['sdGifs', cacheKey], {
                id: cacheKey,
                src,
              });
            },
            () => setImgError({ ...imgError, [cacheKey]: rejectIndex })
          )}
          height={height}
          style={{ padding: 0, margin: '0px 0px -3px 0px' }}
        />
      );
    }
  } else {
    return <div className={classes.alternativeSDdisplay} />;
  }
};
