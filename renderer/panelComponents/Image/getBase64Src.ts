import request from 'request';

import { name } from '@@identifier/core/imageCache';
import { PickType, Repository, StateSchemas } from '@@interface';
// import gifFrames from 'gif-frames';
// import * as jsgif from './jsgif';

export const getBase64Src = async (
  src: string,
  height: number,
  resolve: CallableFunction,
  reject: CallableFunction
): Promise<void> => {
  request
    .defaults({
      method: 'GET',
      encoding: null,
      agentOptions: {
        rejectUnauthorized: false,
      },
    })
    .get(src, (error, res, body) => {
      console.log(`[request] ${src}\n[error] ${error}`);
      if (res != null) {
        const { statusCode, headers } = res;
        if (error || statusCode !== 200) {
          if (statusCode === 404) {
            reject();
          }
          return;
        }
        const contentType = headers['content-type'];
        if (contentType === 'image/gif' || contentType === 'image/png' || contentType === 'image/jpeg') {
          const base64Str = Number.isFinite(body)
            ? Buffer.alloc(body).toString('base64')
            : Buffer.from(body).toString('base64');
          const base64Src = `data:${contentType};base64,${base64Str}`;

          /**
           * Confirmed that the codes below could successfully resize the frames of given gif animation,
           * but the resized image (250px -> 140px or even 250px -> 200px) looked too coarse to display.
           * p.s., With interpolation methods, it may be possible to maintain quality.
           *       However, the overall process becomes too slow, and the memory consumption is not so severe compared to it.
           * These are intentionally remained commented out as a legacy code.
           */
          // if (contentType === 'image/gif') {
          //   gifFrames({ url: base64Src, frames: 'all', outputType: 'canvas' }).then(function (frameData) {
          //     // console.log(frameData[0].getImage().data); // outputType: 'png': 4 continuous elements per chunk (r g b a), observed just 4 x 250 x 250 = 250000
          //     // const canvas = document.createElement('canvas');
          //     // const ctx = canvas.getContext('2d');
          //     // const image = new Image();
          //     // image.crossOrigin = 'Anonymous';

          //     // const ratio = 200 / frameData[0].height;
          //     // canvas.width = Math.ceil(frameData[0].width * ratio);
          //     // canvas.height = Math.ceil(frameData[0].height * ratio);

          //     const encoder = new jsgif.GIFEncoder();
          //     encoder.setRepeat(0);
          //     encoder.setFrameRate(25);
          //     encoder.setQuality(1);
          //     encoder.setTransparent(0);
          //     encoder.start();
          //     for (let i = 0; i < frameData.length; i++) {
          //       // for (let i = 0; i < 1; i++) {
          //       const frame = frameData[i];
          //       const canvasFrame = frame.getImage();
          //       encoder.addFrame(canvasFrame.getContext('2d'), false);
          //     }
          //     encoder.finish();
          //     console.log(encoder.transparent);
          //     console.log(encoder.transIndex);

          //     const res = encoder.stream().getData();
          //     const resizedBase64Src = 'data:image/gif;base64,' + jsgif.encode64(res);
          //     console.log(base64Src.length);
          //     console.log(resizedBase64Src.length);
          //     resolve(resizedBase64Src);
          //     // const canvasFrame = frameData[0].getImage();
          //     // const base64Frame = canvasFrame.toDataURL();
          //     // const canvas = document.createElement('canvas');
          //     // const ctx = canvas.getContext('2d');
          //     // const image = new Image();
          //     // image.crossOrigin = 'Anonymous';
          //     // image.onload = function (event) {
          //     //   const ratio = 200 / canvasFrame.height;
          //     //   canvas.width = Math.ceil(canvasFrame.width * ratio);
          //     //   canvas.height = Math.ceil(canvasFrame.height * ratio);
          //     //   ctx.drawImage(
          //     //     canvasFrame,
          //     //     0,
          //     //     0,
          //     //     canvasFrame.width,
          //     //     canvasFrame.height,
          //     //     0,
          //     //     0,
          //     //     canvas.width,
          //     //     canvas.height
          //     //   );
          //     //   console.log(canvas.toDataURL());
          //     //   resolve(canvas.toDataURL());
          //     // };
          //     // image.src = base64Frame;
          //   });
          // } else {
          //   resolve(base64Src);
          // }

          resolve(base64Src);
        } else {
          reject();
        }
      } else {
        // reject();
      }
    });
};

export const getCacheOrStore = (
  gateway: Repository<PickType<StateSchemas['core'], typeof name>>,
  cacheKeys: string[],
  src: string,
  height: number,
  resolve: CallableFunction,
  reject: CallableFunction
): string => {
  try {
    return gateway.loadProperty([...cacheKeys, 'src']) as string;
  } catch (e) {
    getBase64Src(src, height, resolve, reject);
    return src;
  }
};
