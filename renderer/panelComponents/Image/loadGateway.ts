import * as repository from '@@core/repository';
import defaultData from '@@defaultData/imageCache.default.json';
import { name } from '@@identifier/core/imageCache';
import { DefaultDataSchemas, PickType } from '@@interface';

export const gateway = new repository[name].Gateway(
  defaultData as PickType<DefaultDataSchemas, typeof name>,
  name,
  '.json'
);
