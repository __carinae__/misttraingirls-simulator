/**
 * This class handles LZW encoding
 * Adapted from Jef Poskanzer's Java port by way of J. M. G. Elliott.
 * @author Kevin Weiner (original Java version - kweiner@fmsware.com)
 * @author Thibault Imbert (AS3 version - bytearray.org)
 * @author Kevin Kwok (JavaScript version - https://github.com/antimatter15/jsgif)
 * @version 0.1 AS3 implementation
 */

// GIFCOMPR.C - GIF Image compression routines
// Lempel-Ziv compression based on 'compress'. GIF modifications by
// David Rowley (mgardi@watdcsu.waterloo.edu)
// General DEFINEs
const EOF = -1;
const BITS = 12;
const HSIZE = 5003; // 80% occupancy
const maxbits = BITS; // user settable max # bits/code
const maxmaxcode = 1 << BITS; // should NEVER generate this code
const masks = [
  0x0000, 0x0001, 0x0003, 0x0007, 0x000f, 0x001f, 0x003f, 0x007f, 0x00ff, 0x01ff, 0x03ff, 0x07ff, 0x0fff, 0x1fff,
  0x3fff, 0x7fff, 0xffff,
];

export class LZWEncoder {
  // GIF Image compression - modified 'compress'
  // Based on: compress.c - File compression ala IEEE Computer, June 1984.
  // By Authors: Spencer W. Thomas (decvax!harpo!utah-cs!utah-gr!thomas)
  // Jim McKie (decvax!mcvax!jim)
  // Steve Davies (decvax!vax135!petsd!peora!srd)
  // Ken Turkowski (decvax!decwrl!turtlevax!ken)
  // James A. Woods (decvax!ihnp4!ames!jaw)
  // Joe Orost (decvax!vax135!petsd!joe)
  imgW: number;
  imgH: number;
  pixAry;
  initCodeSize;
  remaining;
  curPixel;
  nBits; // number of bits/code
  maxcode; // maximum code, given nBits
  htab;
  codetab;
  hsize; // for dynamic table sizing
  freeEnt; // first unused entry

  // block compression parameters -- after all codes are used up,
  // and compression rate changes, start over.
  clearFlg;

  // Algorithm: use open addressing double hashing (no chaining) on the
  // prefix code / next character combination. We do a variant of Knuth's
  // algorithm D (vol. 3, sec. 6.4) along with G. Knott's relatively-prime
  // secondary probe. Here, the modular division first probe is gives way
  // to a faster exclusive-or manipulation. Also do block compression with
  // an adaptive reset, whereby the code table is cleared when the compression
  // ratio decreases, but after the table fills. The variable-length output
  // codes are re-sized at this point, and a special CLEAR code is generated
  // for the decompressor. Late addition: construct the table according to
  // file size for noticeable speed improvement on small files. Please direct
  // questions about this implementation to ames!jaw.
  gInitBits;
  ClearCode;
  EOFCode;

  // output
  // Output the given code.
  // Inputs:
  // code: A nBits-bit integer. If == -1, then EOF. This assumes
  // that nBits =< wordsize - 1.
  // Outputs:
  // Outputs code to the file.
  // Assumptions:
  // Chars are 8 bits long.
  // Algorithm:
  // Maintain a BITS character long buffer (so that 8 codes will
  // fit in it exactly). Use the VAX insv instruction to insert each
  // code in turn. When the buffer fills up empty it and start over.
  curAccum;
  curBits;

  // Number of characters so far in this 'packet'
  aCount;

  // Define the storage for the packet accumulator
  accum;

  constructor(width, height, pixels, colorDepth) {
    this.imgW = width;
    this.imgH = height;
    this.pixAry = pixels;
    this.initCodeSize = Math.max(2, colorDepth);

    this.htab = [];
    this.codetab = [];
    this.hsize = HSIZE;
    this.freeEnt = 0;

    this.clearFlg = false;

    this.curAccum = 0;
    this.curBits = 0;

    this.accum = [];
  }

  // Add a character to the end of the current packet, and if it is 254
  // characters, flush the packet to disk.
  charOut(c, outs) {
    this.accum[this.aCount++] = c;
    if (this.aCount >= 254) this.flushChar(outs);
  }

  // Clear out the hash table
  // table clear for block compress
  clBlock(outs) {
    this.clHash(this.hsize);
    this.freeEnt = this.ClearCode + 2;
    this.clearFlg = true;
    this.output(this.ClearCode, outs);
  }

  // reset code table
  clHash(hsize) {
    for (let i = 0; i < hsize; ++i) this.htab[i] = -1;
  }

  compress(initBits, outs) {
    let fcode;
    let i; /* = 0 */
    let c;
    let ent;
    let disp;
    let hshift;

    // Set up the globals: gInitBits - initial number of bits
    this.gInitBits = initBits;

    // Set up the necessary values
    this.clearFlg = false;
    this.nBits = this.gInitBits;
    this.maxcode = this.getMaxcode(this.nBits);

    this.ClearCode = 1 << (initBits - 1);
    this.EOFCode = this.ClearCode + 1;
    this.freeEnt = this.ClearCode + 2;

    this.aCount = 0; // clear packet

    ent = this.nextPixel();

    hshift = 0;
    for (fcode = this.hsize; fcode < 65536; fcode *= 2) ++hshift;
    hshift = 8 - hshift; // set hash code range bound

    const hsizeReg = this.hsize;
    this.clHash(hsizeReg); // clear hash table

    this.output(this.ClearCode, outs);

    outerLoop: while ((c = this.nextPixel()) != EOF) {
      fcode = (c << maxbits) + ent;
      i = (c << hshift) ^ ent; // xor hashing

      if (this.htab[i] == fcode) {
        ent = this.codetab[i];
        continue;
      } else if (this.htab[i] >= 0) {
        // non-empty slot

        disp = hsizeReg - i; // secondary hash (after G. Knott)
        if (i === 0) disp = 1;

        do {
          if ((i -= disp) < 0) i += hsizeReg;

          if (this.htab[i] == fcode) {
            ent = this.codetab[i];
            continue outerLoop;
          }
        } while (this.htab[i] >= 0);
      }

      this.output(ent, outs);
      ent = c;
      if (this.freeEnt < maxmaxcode) {
        this.codetab[i] = this.freeEnt++; // code -> hashtable
        this.htab[i] = fcode;
      } else this.clBlock(outs);
    }

    // Put out the final code.
    this.output(ent, outs);
    this.output(this.EOFCode, outs);
  }

  encode(os) {
    os.writeByte(this.initCodeSize); // write "initial code size" byte
    this.remaining = this.imgW * this.imgH; // reset navigation variables
    this.curPixel = 0;
    this.compress(this.initCodeSize + 1, os); // compress and write the pixel data
    os.writeByte(0); // write block terminator
  }

  // Flush the packet to disk, and reset the accumulator
  flushChar(outs) {
    if (this.aCount > 0) {
      outs.writeByte(this.aCount);
      outs.writeBytes(this.accum, 0, this.aCount);
      this.aCount = 0;
    }
  }

  getMaxcode(nBits) {
    return (1 << nBits) - 1;
  }

  nextPixel() {
    if (this.remaining === 0) return EOF;
    --this.remaining;
    const pix = this.pixAry[this.curPixel++];
    return pix & 0xff;
  }

  // ----------------------------------------------------------------------------
  // Return the next pixel from the image
  // ----------------------------------------------------------------------------
  output(code, outs) {
    this.curAccum &= masks[this.curBits];

    if (this.curBits > 0) this.curAccum |= code << this.curBits;
    else this.curAccum = code;

    this.curBits += this.nBits;

    while (this.curBits >= 8) {
      this.charOut(this.curAccum & 0xff, outs);
      this.curAccum >>= 8;
      this.curBits -= 8;
    }

    // If the next entry is going to be too big for the code size,
    // then increase it, if possible.

    if (this.freeEnt > this.maxcode || this.clearFlg) {
      if (this.clearFlg) {
        this.maxcode = this.getMaxcode((this.nBits = this.gInitBits));
        this.clearFlg = false;
      } else {
        ++this.nBits;
        if (this.nBits == maxbits) this.maxcode = maxmaxcode;
        else this.maxcode = this.getMaxcode(this.nBits);
      }
    }

    if (code == this.EOFCode) {
      // At EOF, write the rest of the buffer.
      while (this.curBits > 0) {
        this.charOut(this.curAccum & 0xff, outs);
        this.curAccum >>= 8;
        this.curBits -= 8;
      }

      this.flushChar(outs);
    }
  }
}
