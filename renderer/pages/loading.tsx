import Head from 'next/head';
import React from 'react';

import pack from '@@package';
import { useStyles } from '@@style/home';
import { BoundBall, LoopingRhombusesSpinner, Rhombus } from '@@style/loading/LoopingRhombus';

const Loading: React.FC = () => {
  const classes = useStyles({});

  return (
    <React.Fragment>
      <Head>
        <title>{pack.productName + ' ver.' + pack.version}</title>
      </Head>
      <div className={classes.root}>
        <LoopingRhombusesSpinner>
          <Rhombus />
          <Rhombus />
          <Rhombus />
          <Rhombus />
          <Rhombus />
          <BoundBall />
        </LoopingRhombusesSpinner>
      </div>
    </React.Fragment>
  );
};

export default Loading;
