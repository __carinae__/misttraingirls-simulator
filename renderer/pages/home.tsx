import { TabContext, TabList, TabPanel } from '@mui/lab';
import { Box, Divider, Drawer, List, ListItem, Tab, Tooltip } from '@mui/material';
import { ipcRenderer } from 'electron';
import Head from 'next/head';
import React, { useEffect, useState } from 'react';
import { CgRedo, CgUndo } from 'react-icons/cg';
import { HiSaveAs } from 'react-icons/hi';
import { MdLiveHelp } from 'react-icons/md';
import { useDispatch } from 'react-redux';
import { ActionCreators } from 'redux-undo';
import { panelKeys, panels } from 'renderer/panels';

import { actions, activateDatabase, load, save } from '@@core/redux';
import pack from '@@package';
import { useStyles } from '@@style/home';

const isProd = process.env.NODE_ENV === 'production';
const isMac = process.platform === 'darwin';

const Home: React.FC = () => {
  const classes = useStyles({});

  const dispatch = useDispatch();
  const openHelpDialog = () => dispatch(actions.help.setOpen(true));
  const closeHelpDialog = () => dispatch(actions.help.setOpen(false));

  const [pageId, setPageId] = useState(panelKeys[0]);
  const changePage = (event: React.ChangeEvent<unknown>, id: string) => {
    setPageId(id);
    closeHelpDialog();
  };

  useEffect(() => {
    dispatch(actions.metaFlags.setMounted(true));
    activateDatabase(dispatch);
    load(dispatch);
    console.log('[isProd] ' + isProd);
    console.log('[isMac] ' + isMac);
    dispatch(ActionCreators.clearHistory());

    /**
     * Save all the current data when catching the signal of terminating the main window.
     */
    ipcRenderer.on('on-app-closing', () => {
      save(dispatch);
      ipcRenderer.send('quitter');
    });

    /**
     * Key event listener to save all the app data to database.
     */
    ipcRenderer.on('save', () => {
      save(dispatch);
    });

    /**
     * Key event listeners to undo/redo the app state.
     */
    ipcRenderer.on('undo', () => {
      dispatch(ActionCreators.undo());
    });
    ipcRenderer.on('redo', () => {
      dispatch(ActionCreators.redo());
    });
  }, []);

  return (
    <React.Fragment>
      <Head>
        <title>{pack.productName + ' ver.' + pack.version}</title>
      </Head>
      <div className={classes.root}>
        <TabContext value={pageId}>
          {/* Left drawer containing the main menu. */}
          <Drawer
            className={classes.drawer}
            variant="permanent"
            classes={{
              paper: classes.drawerPaper,
            }}
            anchor="left">
            <TabList
              selectionFollowsFocus
              value={pageId}
              onChange={changePage}
              indicatorColor="primary"
              textColor="primary"
              orientation="vertical"
              className={classes.tabs}
              style={{ height: '100vh' }}>
              {panelKeys.map(key => (
                <Tab
                  icon={
                    <Tooltip
                      title={panels[key].name}
                      arrow={pageId !== key}
                      classes={{ tooltip: pageId === key ? classes.tabTooltip : classes.tabTooltipUnselected }}
                      placement="right">
                      <Box>{React.createElement(panels[key].Icon)}</Box>
                    </Tooltip>
                  }
                  key={'panel-tab-' + key}
                  value={key}
                  classes={{ root: classes.tab }}
                  disableRipple
                />
              ))}
            </TabList>
          </Drawer>
          {/* Content area. */}
          <Box className={classes.content}>
            {panelKeys.map(key => (
              <TabPanel key={'panel-' + key} value={key} style={{ padding: 0, margin: 0 }}>
                {React.createElement(panels[key].Component)}
                {panels[key].Help == null ? null : React.createElement(panels[key].Help)}
              </TabPanel>
            ))}
          </Box>
        </TabContext>
        {/* Right drawer containing help icon. */}
        <Drawer
          className={classes.drawer}
          variant="permanent"
          classes={{
            paper: classes.drawerPaperRight,
          }}
          anchor="right">
          <List>
            <ListItem
              button
              style={{ backgroundColor: 'transparent', margin: '0px 0px 0px -3px' }}
              disableRipple
              onClick={() => dispatch(ActionCreators.undo())}>
              <Tooltip
                title={`元に戻す［${isMac ? 'Cmd' : 'Ctrl'}+Z］`}
                placement="left"
                arrow
                classes={{ tooltip: classes.tooltip }}>
                <Box>
                  <CgUndo size={18} />
                </Box>
              </Tooltip>
            </ListItem>
            <ListItem
              button
              style={{ backgroundColor: 'transparent', margin: '0px 0px 0px -3px' }}
              disableRipple
              onClick={() => dispatch(ActionCreators.redo())}>
              <Tooltip
                title={`やり直し［Shift+${isMac ? 'Cmd' : 'Ctrl'}+Z］`}
                placement="left"
                arrow
                classes={{ tooltip: classes.tooltip }}>
                <Box>
                  <CgRedo size={18} />
                </Box>
              </Tooltip>
            </ListItem>
            <ListItem
              button
              style={{ backgroundColor: 'transparent', margin: '0px 0px 0px -3px' }}
              disableRipple
              onClick={() => save(dispatch)}>
              <Tooltip
                title={`保存［${isMac ? 'Cmd' : 'Ctrl'}+S］`}
                placement="left"
                arrow
                classes={{ tooltip: classes.tooltip }}>
                <Box>
                  <HiSaveAs size={18} />
                </Box>
              </Tooltip>
            </ListItem>
            <Divider />
            <Box height={5} />
            <ListItem
              button
              style={{ backgroundColor: 'transparent', margin: '0px 0px 0px -3px' }}
              disableRipple
              onClick={openHelpDialog}>
              <Tooltip title="ヘルプ" placement="left" arrow classes={{ tooltip: classes.tooltip }}>
                <Box>
                  <MdLiveHelp size={15} />
                </Box>
              </Tooltip>
            </ListItem>
          </List>
        </Drawer>
        {/* </img> */}
      </div>
    </React.Fragment>
  );
};

export default Home;
