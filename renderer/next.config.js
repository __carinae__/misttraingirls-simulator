// import { userData } from './resolveUserData';

const aliasWebpackConfig = (config, { isServer }) => {
  if (!isServer) {
    config.target = 'electron-renderer';
    config.resolve.alias = {
      ...config.resolve.alias,
      '@@package': './package.json',
      '@@core': './renderer/core',
      '@@identifier': './renderer/core/__identifier__',
      '@@interface': './renderer/core/__interface__',
      '@@panelComponents': './renderer/panelComponents',
      '@@style': './renderer/style',
      '@@defaultData': './renderer/defaultData',
    };
  }
  return config;
};

module.exports = {
  webpack: (config, { isServer }) => aliasWebpackConfig(config, { isServer }),
  // webpack5: false,
  images: { loader: 'imgix', path: 'https://misttraingirls.wikiru.jp', domains: ['misttraingirls.wikiru.jp'] },
};
