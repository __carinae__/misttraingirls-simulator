import { Weapon } from './const';

export type IWeaponMaster = {
  id?: Weapon;
  level?: number;
  condition?: { skill?: number; trans?: number };
};
