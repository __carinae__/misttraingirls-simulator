export * from './ability';
export * from './action';
export * from './actionPiece';
export * from './character';
export * from './const';
export * from './imageCache';
export * from './skill';
export * from './specialSkill';
export * from './weaponMaster';

export type IEntityFrame = { id?: string; undeletable?: boolean } & Record<string, unknown>;
export type TargetAccessor = { id: string; keys: string[][]; func: (...entityProps: unknown[]) => number | string };
export type SortFunction<T> = (sortOrder: 'asc' | 'desc') => (a: T, b: T) => number;
