import { IEffect } from './effect';

export type IAbility = {
  id?: string;
  undeletable?: boolean;
  description?: string;
  /** @arrayTypeInfo READ IN ORDER */
  effects?: IEffect[];
};
