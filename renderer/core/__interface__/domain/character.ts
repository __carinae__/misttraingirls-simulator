import { Affiliation, AttackStatusTag, Attribute, LayerType, Race, Rarity, StatusTag, Weapon } from './const';

export type IBase = {
  id?: string;
  undeletable?: boolean;
  weapon?: Weapon;
  from?: Affiliation;
  race?: Race;
  hp?: number;
  status?: Record<StatusTag, number>;
  layerIds?: Record<Rarity, Record<string, boolean>>;
  skillLevels?: Record<string, number>;
  growthStep?: number;
};

export type ILayer = {
  id?: string;
  undeletable?: boolean;
  baseId?: string;
  rarity?: Rarity;
  index?: number;
  type?: LayerType;
  attackType?: AttackStatusTag;
  layerLevelAttackType?: AttackStatusTag;
  gearLevelStatusBonus?: { alpha: StatusTag; beta: StatusTag };
  layerLevel?: number;
  gearLevel?: number;
  intimacyLevel?: number;
  scale?: Record<StatusTag, number>;
  bonus?: Record<StatusTag, number>;
  status?: Record<StatusTag, number>;
  initialTransGauge?: number;
  skillIds?: (string | undefined)[];
  SSkill?: string;
  abilityIds?: (string | undefined)[];
  resistance?: Record<Attribute, number>;
  flavorText?: string;
  statusGrowthLimit?: Record<StatusTag, number>;
};
