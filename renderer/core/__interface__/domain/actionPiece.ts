import { EnemySpecies } from '.';
import { IStateCondition } from './condition';
import {
  ActionRange,
  ActionTargetOption,
  ActionTargetTag,
  AttackStatusTag,
  Attribute,
  DamageChangableStatusTag,
  StatusAilment,
  StatusTag,
} from './const';

/**
 * All action pieces should inherit this base properties.
 */
export type IActionPieceBase = {
  probabilityPercentage?: number;
  preActionPieceHook?: boolean;
  postActionPieceHook?: boolean;
  turn?: number;
  wavePersistent?: boolean;
  battlePersistent?: boolean;
  /** @arrayTypeInfo OR */
  condition?: IStateCondition[];
};

export const activeActionTypes = [
  'cure',
  'targetSelect',
  'attack',
  'statusEffect',
  'statusAilment',
  'healStatusAilment',
  'spCharge',
  'cpCharge',
];

export type ICure = IActionPieceBase & {
  type: 'cure';
  target?: ActionTargetTag;
  value?: number;
};

/**
 * determine target of succeeding action pieces and store it until new target is determined by another targetSelect.
 */
export type ITargetSelect = {
  type: 'targetSelect';
  range?: ActionRange;
  targetOption?: ActionTargetOption;
};

/**
 * target must inherit the previously determined one(s), which means it always should be '相手'.
 * if reference of impact is not specified, it must inferit normal action settings of character base.
 * reference takes the form of array in order to express the OR attributes.
 * multiple attributes can be expressed by the array of impact.
 */
export type IImpactElment = {
  /** @arrayTypeInfo OR */
  reference?: { status?: AttackStatusTag; attribute?: Attribute }[];
  value?: number;
};
export type IAttack = IActionPieceBase & {
  type: 'attack';
  /** @arrayTypeInfo AND */
  impact?: IImpactElment[];
  /** @arrayTypeInfo OR */
  criticalTarget?: EnemySpecies[];
  /** @arrayTypeInfo AND */
  ignoreDifence?: {
    reference: { status: AttackStatusTag; percentage: number }[];
    condition?: IStateCondition[];
  }[];
};

export type IStatusEffect = IActionPieceBase & {
  type: 'statusEffect';
  target?: ActionTargetTag;
  status?: StatusTag | DamageChangableStatusTag;
  referenceBaseStatus?: boolean;
  resistance?: Attribute;
  percentage?: number;
  amount?: number;
  nonOverlappable?: boolean;
};

export type IStatusAilment = IActionPieceBase & {
  type: 'statusAilment';
  target?: ActionTargetTag;
  effect?: StatusAilment;
};

export type IHealStatusAilment = IActionPieceBase & {
  type: 'healStatusAilment';
  target?: ActionTargetTag;
  healAll?: boolean;
  /** @arrayTypeInfo AND */
  healSpecific?: StatusAilment[];
};

export type ISPCharge = IActionPieceBase & {
  type: 'spCharge';
  target?: ActionTargetTag;
  amount?: number;
};

export type ICPCharge = IActionPieceBase & {
  type: 'cpCharge';
  target?: ActionTargetTag;
  amount?: number;
};

export type IActiveActionPiece =
  | ITargetSelect
  | ICure
  | IStatusEffect
  | IStatusAilment
  | IHealStatusAilment
  | IAttack
  | ISPCharge
  | ICPCharge;

/**
 * Actions activated when being attacked, in other words instant protect or counter attack.
 * Inserted to the passive action time line according to the timing when the effect is called.
 */
export const passiveActionTypes = ['counter', 'instantProtect'];

export type IPassiveTargetAction = {
  /** @arrayTypeInfo OR */
  range?: ActionRange[];
  /** @arrayTypeInfo OR */
  targetOption?: ActionTargetOption[];
};

/**
 * The skill levels of action pieaces executed as counter all become lv.1.
 */
export type ICounterAttack = IActionPieceBase & {
  type: 'counter';
  /** @arrayTypeInfo OR */
  targetAction?: IPassiveTargetAction[];
  actions?: IActiveActionPiece[];
};

export type IInstantProtect = IActionPieceBase & {
  type: 'instantProtect';
  /** @arrayTypeInfo OR */
  targetAction?: IPassiveTargetAction[];
  percentage?: number;
};

export type IPassiveActionPiece = ICounterAttack | IInstantProtect;
export type IActionPiece = IActiveActionPiece | IPassiveActionPiece;
