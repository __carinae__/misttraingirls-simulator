import { IActionPiece } from './actionPiece';
import { ActionRange, ActionTargetOption, AttackStatusTag, Attribute, ImpactRank } from './const';

export type IAction = {
  id?: string;
  undeletable?: boolean;
  impactRank?: ImpactRank;
  rp?: number;
  sp?: { default?: number; maxAwakenLevel?: number };
  range?: ActionRange;
  targetOption?: ActionTargetOption;
  /** @arrayTypeInfo AND */
  attributes?: Attribute[];
  /** @arrayTypeInfo AND */
  attackStatus?: AttackStatusTag[];
  /** @arrayTypeInfo READ IN ORDER */
  actionPieces?: IActionPiece[];
  fast?: boolean;
  delay?: boolean;
  description?: string;
};
