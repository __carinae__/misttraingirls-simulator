import React from 'react';
import {
  GiAngelOutfit,
  GiArrowhead,
  GiBatteredAxe,
  GiBowArrow,
  GiEclipseFlare,
  GiFairyWand,
  GiFastArrow,
  GiFist,
  GiFluffyFlame,
  GiGooeyImpact,
  GiMagicGate,
  GiPistolGun,
  GiQuickSlash,
  GiRelicBlade,
  GiWaterSplash,
  GiWhip,
  GiWindHole,
} from 'react-icons/gi';

export const terms = {
  actionTargetTags: { legend: '対象', values: ['自身', '味方全体', '敵全体', '相手'] },
  attackStatusTags: { legend: '攻撃参照', values: ['物攻', '魔攻'] },
  defenceStatusTags: { legend: '防御参照', values: ['物防', '魔防'] },
  statusTags: { legend: 'ステータス', values: ['物攻', '物防', '命中', '速さ', '魔攻', '魔防', '回復', '幸運'] },
  damageChangableStatusTags: { legend: 'ダメージ関与', values: ['コンディション', 'プロテクト'] },
  chargableStatusTags: { legend: '蓄積値', values: ['SP', 'CP'] },
  attributes: {
    legend: '属性',
    values: ['刃', '衝', '貫', '火', '水', '風', '光', '闇'],
    Icons: {
      刃: function Icon(props) {
        return (
          <GiQuickSlash
            {...props}
            style={{ margin: '0px 0px -1px 0px', transform: 'scaleX(-1)', color: '#dddddd', ...props.style }}
          />
        );
      },
      衝: function Icon(props) {
        return <GiGooeyImpact {...props} style={{ margin: '0px 0px -1px 0px', color: '#dddddd', ...props.style }} />;
      },
      貫: function Icon(props) {
        return (
          <GiFastArrow
            {...props}
            style={{ margin: '0px 0px -1px 0px', transform: 'scaleX(-1)', color: '#dddddd', ...props.style }}
          />
        );
      },
      火: function Icon(props) {
        return <GiFluffyFlame {...props} style={{ margin: '0px 0px -1px 0px', color: '#ff8945', ...props.style }} />;
      },
      水: function Icon(props) {
        return <GiWaterSplash {...props} style={{ margin: '0px 0px -1px 0px', color: '#6bc1ff', ...props.style }} />;
      },
      風: function Icon(props) {
        return <GiWindHole {...props} style={{ margin: '0px 0px -1px 0px', color: '#9cff90', ...props.style }} />;
      },
      光: function Icon(props) {
        return <GiAngelOutfit {...props} style={{ margin: '0px 0px -1px 0px', color: '#fff589', ...props.style }} />;
      },
      闇: function Icon(props) {
        return <GiEclipseFlare {...props} style={{ margin: '0px 0px -1px 0px', color: '#c848fc', ...props.style }} />;
      },
    },
  },
  statusAilments: {
    legend: '状態異常',
    values: ['毒化', '気絶', '睡眠', '混乱', '暗闇', '麻痺', '魅了', '沈黙'],
  },
  weapons: {
    legend: '武器種',
    values: ['剣', '斧', '魔具', '拳', '鞭', '杖', '弓', '槍', '銃'],
    Icons: {
      剣: function Icon(props) {
        return <GiRelicBlade {...props} style={{ margin: '0px 0px 0px 0px', ...props.style }} />;
      },
      斧: function Icon(props) {
        return <GiBatteredAxe {...props} style={{ margin: '0px 0px 0px 0px', ...props.style }} />;
      },
      魔具: function Icon(props) {
        return <GiMagicGate {...props} style={{ margin: '0px 0px 0px 0px', ...props.style }} />;
      },
      拳: function Icon(props) {
        return <GiFist {...props} style={{ margin: '0px 0px 0px 0px', ...props.style }} />;
      },
      鞭: function Icon(props) {
        return <GiWhip {...props} style={{ margin: '0px 0px 0px 0px', ...props.style }} />;
      },
      杖: function Icon(props) {
        return <GiFairyWand {...props} style={{ margin: '0px 0px 0px 0px', ...props.style }} />;
      },
      弓: function Icon(props) {
        return <GiBowArrow {...props} style={{ margin: '0px 0px 0px 0px', ...props.style }} />;
      },
      槍: function Icon(props) {
        return <GiArrowhead {...props} style={{ margin: '0px 0px 0px 0px', ...props.style }} />;
      },
      銃: function Icon(props) {
        return <GiPistolGun {...props} style={{ margin: '0px 0px 0px 0px', ...props.style }} />;
      },
    },
  },
  actionEvents: { legend: 'イベント', values: ['撃破', '被弾'] },
  actionRanges: { legend: '射程', values: ['近', '中', '遠', '味方'] },
  actionTargetOptions: { legend: '範囲', values: ['単体', '全体', 'ランダム', '横一列', '縦一列', '十字'] },
  affiliations: {
    legend: '所属',
    values: ['セントイリス', 'ニシキ', 'アイゼングラート', 'ヴェルフォレット', 'フレイマリン', 'その他'],
  },
  races: { legend: '種族', values: ['人間', '獣人', '鬼人', 'エルフ', 'シャナ族', 'ハーフエルフ', 'ダークエルフ'] },
  rarities: { legend: 'レアリティ', values: ['SS', 'S', 'A'] },
  layerTypes: { legend: 'タイプ', values: ['アタッカー', 'ディフェンダー', 'サポーター', 'トリックスター'] },
  impactRanks: { legend: '威力', values: ['SSS', 'SS', 'S', 'A', 'B', 'C', 'D', 'E', '─'] },
  enemySpecies: {
    legend: '敵種族',
    values: ['魔獣', '悪魔', '不死', '無機', '害虫', '怪鳥', '妖怪', '幽体', '人型', '妖精', '植物'],
  },
} as const;

export type ActionTargetTag = typeof terms.actionTargetTags.values[number];
export type AttackStatusTag = typeof terms.attackStatusTags.values[number];
export type DefenceStatusTag = typeof terms.defenceStatusTags.values[number];
export type StatusTag = typeof terms.statusTags.values[number];
export type DamageChangableStatusTag = typeof terms.damageChangableStatusTags.values[number];
export type ChargableStatusTag = typeof terms.chargableStatusTags.values[number];
export type Attribute = typeof terms.attributes.values[number];
export type StatusAilment = typeof terms.statusAilments.values[number];
export type Weapon = typeof terms.weapons.values[number];
export type ActionEvent = typeof terms.actionEvents.values[number];
export type ActionRange = typeof terms.actionRanges.values[number];
export type ActionTargetOption = typeof terms.actionTargetOptions.values[number];
export type ImpactRank = typeof terms.impactRanks.values[number];
export type EnemySpecies = typeof terms.enemySpecies.values[number];

export type Affiliation = typeof terms.affiliations.values[number];
export type Race = typeof terms.races.values[number];
export type Rarity = typeof terms.rarities.values[number];
export type LayerType = typeof terms.layerTypes.values[number];
