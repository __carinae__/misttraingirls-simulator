import { IAction } from './action';
import { IActionCondition } from './condition';
import { IEffect } from './effect';

export type ISkill = IAction & {
  link?: {
    description?: {
      condition: string;
      effect: string;
    };
    turnLag?: number;
    join?: { threshold: number; operator: 'geq' | 'eq' | 'leq' };
    /** @arrayTypeInfo OR */
    condition?: IActionCondition[];
    /**
     * The skill levels of action pieaces executed as instantEffects all inherit the original skill which called them.
     * @arrayTypeInfo READ IN ORDER
     * */
    instantEffects?: IEffect[];
    extraAbility?: {
      /** @arrayTypeInfo READ IN ORDER */
      effects?: IEffect[];
    };
  };
};
