import {
  ActionEvent,
  ActionRange,
  ActionTargetOption,
  ActionTargetTag,
  AttackStatusTag,
  Attribute,
  ImpactRank,
  StatusAilment,
  Weapon,
} from './const';

/**
 * Basic condition properties related to actions.
 * Properties inside skill all take the form of array type, which means OR operations.
 * e.g., range: ["近", "中", "遠"] means the skills attacking enemies somehow.
 */
export type IActionConditionBase = {
  /** @arrayTypeInfo OR */
  position?: number[];
  weapon?: Weapon;
  skill?: {
    /** @arrayTypeInfo OR */
    id?: string[];
    /** @arrayTypeInfo OR */
    attributes?: Attribute[];
    /** @arrayTypeInfo OR */
    status?: AttackStatusTag[];
    /** @arrayTypeInfo OR */
    range?: ActionRange[];
    /** @arrayTypeInfo OR */
    targetOption?: ActionTargetOption[];
    /** @arrayTypeInfo OR */
    impactRank?: ImpactRank[];
  };
};

/**
 * Action order condition.
 * This meats the character whose action order among all the units
 * who select the actions satisfying condition property equals index.
 */
export type IActionOrder = {
  index?: number;
  condition: IActionConditionBase;
};

/**
 * Condition related to actions.
 */
export type IActionCondition = { order?: IActionOrder } & IActionConditionBase;

/**
 * Condition related to the character's state.
 */
export type IStateCondition = {
  attack?: { weak?: boolean; resist?: boolean; critical?: boolean };
  event?: ActionEvent;
  state?: {
    target?: ActionTargetTag;
    statusAilment?: StatusAilment;
  };
  hp?: { thresholdPercentage: number; operator: 'gt' | 'geq' | 'leq' | 'lt' };
  order?: IActionOrder;
  currentLinkedTimes?: { threshold: number; operator: 'gt' | 'geq' | 'eq' | 'leq' | 'lt' };
};
