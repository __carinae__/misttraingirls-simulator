import { IActionPiece } from './actionPiece';
import { IActionCondition } from './condition';
import { ActionTargetTag } from './const';

export type EffectTarget = {
  tag?: ActionTargetTag | '参加';
  condition?: IActionCondition;
};

/**
 * Battle has two time lines conposed of active or passive action pieces of all units
 * including enemies, and units have effect pool of each property below for each proper timing.
 * Effects should be accumulated into a proper pool.
 * Abilities and effects of party skills and formations shuold be inserted to such pools constantly.
 */
export type IEffect = {
  /** @arrayTypeInfo OR */
  target?: EffectTarget[];
  /** Endow fast property to the target's action. */
  fast?: boolean;
  /** Endow delay property to the target's action. */
  delay?: boolean;
  /**
   * Giving layers permanent states as some of equipment abilities,
   * party skills, formation effects, etc.
   * Inserted to the active action time line only once in the beginning of the battle.
   * @arrayTypeInfo READ IN ORDER
   */
  preBattleHook?: IActionPiece[];
  /**
   * Inserted to the active action time line at the beginning or the end of each wave.
   * @arrayTypeInfo READ IN ORDER
   */
  preWaveHook?: IActionPiece[];
  postWaveHook?: IActionPiece[];
  /**
   * Inserted to the active action time line at the beginning or the end of each turn.
   * @arrayTypeInfo READ IN ORDER
   */
  preTurnHook?: IActionPiece[];
  postTurnHook?: IActionPiece[];
  /**
   * Inserted to the active action time line at each turn after all units' actions are selected.
   * Action is necessarily selected just once at each turn by each layer.
   * @arrayTypeInfo READ IN ORDER
   */
  preActionHook?: IActionPiece[];
  postActionHook?: IActionPiece[];
  /**
   * Inserted to the active action time line whenever an attack type action piece is executed.
   * For the situation where an enemy is crushed in the middle of multiple hit attack and
   * succeeded actions are canceled, these hooks should be inserted in real time.
   * @arrayTypeInfo READ IN ORDER
   */
  preAttackHook?: IActionPiece[];
  postAttackHook?: IActionPiece[];
};
