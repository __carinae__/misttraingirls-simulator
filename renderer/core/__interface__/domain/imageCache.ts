import { Rarity } from './const';

export type ImageCacheBase64 = {
  id?: string;
  src?: string;
};

export const cacheDomains = {
  sdGifs: {
    getSDURL: (baseId: string, layerId: string): string =>
      `https://misttraingirls.wikiru.jp/index.php?plugin=attach&refer=` +
      encodeURI(`［${layerId}］${baseId}&openfile=SD_［${layerId}］${baseId}`) +
      `_idle.gif`,
    getSDURLWithOnlyBase: (baseId: string, layerId: string): string =>
      `https://misttraingirls.wikiru.jp/index.php?plugin=attach&refer=` +
      encodeURI(`［${layerId}］${baseId}&openfile=SD_${baseId}`) +
      `_idle.gif`,
    getSDURLWithRarity: (baseId: string, rarity: Rarity, layerId: string): string =>
      `https://misttraingirls.wikiru.jp/index.php?plugin=attach&refer=` +
      encodeURI(`［${layerId}］${baseId}&openfile=SD_${rarity}${baseId}`) +
      `_idle.gif`,

    default: { undefinedBaseId: 'プトラ', undefinedLayerId: '蒼海の槍使い' },
  },
} as const;
