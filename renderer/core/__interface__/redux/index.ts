import { StateSchemasCore } from './core';
import { StateSchemasUI } from './ui';

export * from './core';
export * from './ui';

export type StateSchemas = { core: StateSchemasCore; ui: StateSchemasUI };
export type ReducerActionsOutput = { type: string; payload: unknown };
