import { EntityState } from '@reduxjs/toolkit';

import { ImageCacheBase64 } from '@@interface/domain';

export type StateSchema = {
  sdGifs: EntityState<ImageCacheBase64>;
};
