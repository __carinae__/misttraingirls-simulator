import { EntityState } from '@reduxjs/toolkit';

import { ISkill } from '@@interface/domain';

export type StateSchema = {
  skills: EntityState<ISkill>;
};
