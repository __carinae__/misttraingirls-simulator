import { EntityState } from '@reduxjs/toolkit';

import { ISpecialSkill } from '@@interface/domain';

export type StateSchema = {
  specialSkills: EntityState<ISpecialSkill>;
};
