import { EntityState } from '@reduxjs/toolkit';

import * as ability from './ability';
import * as action from './action';
import * as character from './character';
import * as imageCache from './imageCache';
import * as skill from './skill';
import * as specialSkill from './specialSkill';
import * as weaponMaster from './weaponMaster';

export type StateSchemaFrame = Record<string, EntityState<Partial<Record<string, unknown>>> | unknown>;

/**
 * Register and integrate related interfaces.
 * */
export type StateSchemasCore = {
  imageCache: imageCache.StateSchema;
  weaponMaster: weaponMaster.StateSchema;
  character: character.StateSchema;
  skill: skill.StateSchema;
  specialSkill: specialSkill.StateSchema;
  ability: ability.StateSchema;
  action: action.StateSchema;
};
