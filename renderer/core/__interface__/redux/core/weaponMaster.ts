import { EntityState } from '@reduxjs/toolkit';

import { IWeaponMaster } from '@@interface/domain';

export type StateSchema = {
  weaponMasters: EntityState<IWeaponMaster>;
};
