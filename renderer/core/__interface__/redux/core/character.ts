import { EntityState } from '@reduxjs/toolkit';

import { IBase, ILayer } from '@@interface/domain';

export type StateSchema = {
  bases: EntityState<IBase>;
  layers: EntityState<ILayer>;
  nonEntityFields: {
    maxHP: number;
  };
};
