import { EntityState } from '@reduxjs/toolkit';

import { IAction } from '@@interface/domain';

export type StateSchema = {
  actions: EntityState<IAction>;
};
