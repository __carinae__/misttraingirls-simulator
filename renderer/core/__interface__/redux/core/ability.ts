import { EntityState } from '@reduxjs/toolkit';

import { IAbility } from '@@interface/domain';

export type StateSchema = {
  abilities: EntityState<IAbility>;
};
