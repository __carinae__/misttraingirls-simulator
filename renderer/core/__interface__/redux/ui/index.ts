export * from './shared';
import * as characterPanel from './character';
import * as help from './help';
import * as metaFlags from './metaFlags';
import * as skillPanel from './skill';

/**
 * Register and integrate related interfaces.
 * */
export type StateSchemasUI = {
  help: help.StateSchema;
  metaFlags: metaFlags.StateSchema;
  characterPanel: characterPanel.StateSchema;
  skillPanel: skillPanel.StateSchema;
};
