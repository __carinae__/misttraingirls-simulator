import { SortFilterProps } from './shared';

export type StateSchema = {
  selectedId: { skill: string | false };
  selectedIdBuffer: { skill: string | false };
  sortFilterProps: { skills: SortFilterProps };
};
