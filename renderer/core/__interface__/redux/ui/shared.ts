import { Identifiers } from '@@identifier/core';

export type SortFilterProps = {
  sortFunctionId?: string;
  sortOrder?: 'desc' | 'asc';
  filters?: Record<string, Record<string | number, boolean>>;
};

export type SelectorPayload<IEntity> = {
  entitylist: IEntity[];
  name: keyof Identifiers;
  key: string;
} & SortFilterProps;
