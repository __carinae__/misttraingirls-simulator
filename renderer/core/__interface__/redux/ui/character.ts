import { SortFilterProps } from './shared';

export type StateSchema = {
  selectedId: { base: string | false; layer: string | false };
  selectedIdBuffer: { base: string | false; layer: string | false };
  sortFilterProps: { bases: SortFilterProps; layers: SortFilterProps };
  GrowthTable: {
    open: boolean;
    tableId: string;
  };
};
