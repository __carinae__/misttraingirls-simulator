export type StateSchema = {
  mounted: boolean;
  recordingAllowed: boolean;
};
