import { EntityAdapter } from '@reduxjs/toolkit';

import * as ability from './ability';
import * as action from './action';
import * as character from './character';
import * as imageCache from './imageCache';
import * as skill from './skill';
import * as specialSkill from './specialSkill';
import * as weaponMaster from './weaponMaster';

/**
 * Register and integrate related interfaces.
 * */
export type IEntityPremitives = {
  imageCache: imageCache.IEntityPremitives;
  weaponMaster: weaponMaster.IEntityPremitives;
  character: character.IEntityPremitives;
  skill: skill.IEntityPremitives;
  specialSkill: specialSkill.IEntityPremitives;
  ability: ability.IEntityPremitives;
  action: action.IEntityPremitives;
};

export type IEntityRecords = {
  [K in keyof IEntityPremitives]: { [T in keyof IEntityPremitives[K]]: IEntityPremitives[K][T][] };
};

export type IEntityHashRecords = {
  [K in keyof IEntityPremitives]: { [T in keyof IEntityPremitives[K]]: { [ids: string]: IEntityPremitives[K][T] } };
};

export type AdapterRecords = {
  [K in keyof IEntityPremitives]: { [T in keyof IEntityPremitives[K]]: EntityAdapter<IEntityPremitives[K][T]> };
};

export type EntityState<IEntity> = {
  ids: string[];
  entities: Record<string, IEntity>;
};
