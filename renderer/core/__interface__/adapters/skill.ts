import { ISkill } from '@@interface/domain';

export type IEntityPremitives = {
  skills: ISkill;
};
