import { ISpecialSkill } from '@@interface/domain';

export type IEntityPremitives = {
  specialSkills: ISpecialSkill;
};
