import { IWeaponMaster } from '@@interface/domain';

export type IEntityPremitives = {
  weaponMasters: IWeaponMaster;
};
