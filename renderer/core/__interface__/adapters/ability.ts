import { IAbility } from '@@interface/domain';

export type IEntityPremitives = {
  abilities: IAbility;
};
