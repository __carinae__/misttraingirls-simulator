import { IAction } from '@@interface/domain';

export type IEntityPremitives = {
  actions: IAction;
};
