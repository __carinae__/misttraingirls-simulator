import { ImageCacheBase64 } from '@@interface/domain';

export type IEntityPremitives = {
  sdGifs: ImageCacheBase64;
};
