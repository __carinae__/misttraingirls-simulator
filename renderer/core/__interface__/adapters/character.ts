import { IBase, ILayer } from '@@interface/domain';

export type IEntityPremitives = {
  bases: IBase;
  layers: ILayer;
};
