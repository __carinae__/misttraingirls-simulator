import { IEntityHashRecords } from '@@interface/adapters';

type DataStoreSchemaProps = {
  version: string;
};

type NonEntityFields = {
  nonEntityFields?: Record<string, unknown>;
};

export type DefaultDataSchemaFrame = {
  basis:
    | NonEntityFields
    | Record<string /** adapterKey */, Record<string /** id */, Record<string /** entity field */, unknown>>>;
  override: Record<
    string /** version */,
    | Record<
        string /** adapterKey */,
        { keys: string[] /** entity field */; ids: string[] /** ids to apply override */ }[]
      >
    | { nonEntityFields?: { keys: string[] } }
  >;
};

export type DefaultDataSchemas = {
  [K in keyof IEntityHashRecords]: {
    basis: NonEntityFields | IEntityHashRecords[K];
    override: Record<
      string /** version */,
      Partial<
        | Record<
            keyof IEntityHashRecords[K] /** adapterKey */,
            { keys: string[] /** entity field */; ids: string[] /** ids to apply override */ }[]
          >
        | { nonEntityFields?: { keys: string[] } }
      >
    >;
  };
};

export type DataStoreSchemaFrame = {
  memory:
    | NonEntityFields
    | Record<string /** adapterKey */, Record<string /** id */, Record<string /** entity field */, unknown>>>;
} & DataStoreSchemaProps;

export type DataStoreSchemas = {
  [K in keyof IEntityHashRecords]: {
    memory: NonEntityFields | IEntityHashRecords[K];
  } & DataStoreSchemaProps;
};

export interface Repository<StateSchema> {
  activateDatabase(): void;
  load(): StateSchema;
  save(state: StateSchema): void;
  loadProperty(keys: string[]): unknown;
  saveProperty(keys: string[], value: unknown): void;
}
