import { createSlice, EntitySelectors, PayloadAction, Slice } from '@reduxjs/toolkit';

import { Action } from '@@core/domain';
import { deepUpdate } from '@@core/utils';
import defaultData from '@@defaultData/action';
import { name } from '@@identifier/core/action';
import {
  AdapterRecords,
  IEntityPremitives,
  PickType,
  ReducerActionsOutput,
  Repository,
  StateSchemas,
} from '@@interface';

import { getSelectors, SortFilterableEntitySelector } from '../customSelectors';
import { RootState } from '../RootState';
type AdapterRecord = PickType<AdapterRecords, typeof name>;
type StateSchema = PickType<StateSchemas['core'], typeof name>;

/**
 * Create a reducer slice with initial states reflecting the newest database when launching the app.
 * Calling laod function in the home routed page with useEffect is unsufficient
 * because page contents will be reset to the database state after page transition.
 */
export type ReducerActionsInput = {
  addAction: { id: string };
  deleteAction: { id: string };
  updateActionField: { id: string; keys: (string | number)[]; value: unknown };
  activateDatabase: Record<string, never>;
  load: Record<string, never>;
  save: Record<string, never>;
};

export const createSliceFunc = (gateway: Repository<StateSchema>, adapters: AdapterRecord): Slice =>
  createSlice({
    name,
    initialState: Object.fromEntries(
      Object.entries(adapters).map(([key, adapter]) => [key, adapter.getInitialState()])
    ) as StateSchema,
    reducers: {
      addAction: (state, { payload: { id } }) => {
        adapters.actions.addOne(state.actions, Action.getInitializedAction(id));
      },

      deleteAction: (state, { payload: { id } }) => {
        adapters.actions.removeOne(state.actions, id);
      },

      updateActionField: (state, { payload: { id, keys, value } }) => {
        deepUpdate(state.actions.entities[id], keys, value);
      },

      activateDatabase: () => {
        gateway.activateDatabase();
      },

      load: state => {
        return { ...state, ...gateway.load() };
      },

      save: state => {
        gateway.save(state);
      },
    } as {
      [K in keyof ReducerActionsInput]: (
        state: StateSchema,
        action: PayloadAction<ReducerActionsInput[K]>
      ) => void | StateSchema;
    },
  });

export type ReducerActions = {
  [K in keyof ReducerActionsInput]: (payload: ReducerActionsInput[K]) => ReducerActionsOutput;
};

/**
 * Type of record which contains custom selectors related to the identifier.
 */
type SelectorsType = {
  [K in keyof PickType<AdapterRecords, typeof name>]: {
    standard: EntitySelectors<PickType<IEntityPremitives, typeof name>[K], RootState>;
    custom: SortFilterableEntitySelector<PickType<IEntityPremitives, typeof name>[K]>;
    extract: SortFilterableEntitySelector<PickType<IEntityPremitives, typeof name>[K]>;
  };
};

/**
 * Get a record of custom selectors related to the identifier.
 */
export const getCustomSelectors = (): SelectorsType =>
  getSelectors<keyof PickType<AdapterRecords, typeof name>, SelectorsType>(name);

export { defaultData, name };
