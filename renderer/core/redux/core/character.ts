import { createSlice, EntitySelectors, PayloadAction, Slice } from '@reduxjs/toolkit';
import { Matrix } from 'react-spreadsheet';

import { Character } from '@@core/domain';
import { deepUpdate, mergeFields, verifyInputNumber } from '@@core/utils';
import defaultData from '@@defaultData/character';
import { name } from '@@identifier/core/character';
import {
  AdapterRecords,
  Affiliation,
  AttackStatusTag,
  IBase,
  IEntityPremitives,
  ILayer,
  LayerType,
  PickType,
  Race,
  Rarity,
  ReducerActionsOutput,
  Repository,
  StateSchemas,
  StatusTag,
  terms,
  Weapon,
} from '@@interface';

import { getSelectors, SortFilterableEntitySelector } from '../customSelectors';
import { RootState } from '../RootState';
type AdapterRecord = PickType<AdapterRecords, typeof name>;
type StateSchema = PickType<StateSchemas['core'], typeof name>;

/**
 * Create a reducer slice with initial states reflecting the newest database when launching the app.
 * Calling laod function in the home routed page with useEffect is unsufficient
 * because page contents will be reset to the database state after page transition.
 */
export type ReducerActionsInput = {
  addBase: { id: string; weapon: Weapon; from: Affiliation; race: Race; hp: number };
  deleteBase: { id: string };
  updateBaseField: { id: string; keys: (string | number)[]; value: unknown };
  mergeBaseFields: { id: string; changes: { [K in keyof IBase]: Partial<IBase[K]> } };
  addLayer: {
    id: string;
    rarity: Rarity;
    type: LayerType;
    attackType: AttackStatusTag;
    layerLevelAttackType: AttackStatusTag;
    gearLevelStatusBonus: { alpha: StatusTag; beta: StatusTag };
    flavorText: string;
    baseId: string;
  };
  deleteLayer: { id: string };
  updateLayerField: { id: string; keys: (string | number)[]; value: unknown };
  mergeLayerFields: { id: string; changes: { [K in keyof ILayer]: Partial<ILayer[K]> } };
  updateSkillInCharacter: { baseId: string; layerId: string; index: number; skillId: string };
  setLayerStatusBonus: { id: string };
  setLayerStatus: { base: IBase; layer: ILayer };
  setLayerStatusBonusAndStatus: { base: IBase; layer: ILayer };
  updateLevel: { layerLevel?: number; gearLevel?: number; intimacyLevel?: number; baseId: string; layerId: string };
  updateScale: { tag: StatusTag; value: number; baseId: string; layerId: string };
  setAllLayerStatusesBelongingToBase: { id: string };
  updateBaseStatus: { status: Record<StatusTag, number>; id: string };
  updateNonEntityField: { keys: (string | number)[]; value: unknown };
  onChangeBaseStatusTableData: { ids: string[]; data: Matrix<{ value: number }> };
  onChangeGrowthLimitTableData: { ids: string[]; data: Matrix<{ value: number }> };
  hundleRaiseHP: { ids: string[]; checkboxes: boolean[] };
  hundleRaiseBaseStatus: { ids: string[]; checkboxes: boolean[]; differenceTableData: Matrix<{ value: number }> };
  activateDatabase: Record<string, never>;
  load: Record<string, never>;
  save: Record<string, never>;
};

export const createSliceFunc = (gateway: Repository<StateSchema>, adapters: AdapterRecord): Slice =>
  createSlice({
    name,
    initialState: {
      ...Object.fromEntries(Object.entries(adapters).map(([key, adapter]) => [key, adapter.getInitialState()])),
      nonEntityFields: { maxHP: 5000 },
    } as StateSchema,
    reducers: {
      addBase: (state, { payload: { id, weapon, from, race, hp } }) => {
        if (id !== '' && !state.bases.entities.hasOwnProperty(id)) {
          adapters.bases.addOne(state.bases, Character.getInitializedBase(id, weapon, from, race, hp));
        }
      },

      deleteBase: (state, { payload: { id } }) => {
        for (const layerId of Character.getLayersBelonging(state.bases.entities[id].layerIds)) {
          adapters.layers.removeOne(state.layers, layerId);
        }
        adapters.bases.removeOne(state.bases, id);
      },

      updateBaseField: (state, { payload: { id, keys, value } }) => {
        deepUpdate(state.bases.entities[id], keys, value);
      },

      mergeBaseFields: (state, { payload: { id, changes } }) => {
        mergeFields(state.bases.entities[id], changes);
      },

      addLayer: (
        state,
        { payload: { id, rarity, type, attackType, layerLevelAttackType, gearLevelStatusBonus, flavorText, baseId } }
      ) => {
        if (id !== '' && !state.layers.entities.hasOwnProperty(id)) {
          const base = state.bases.entities[baseId];
          adapters.layers.addOne(
            state.layers,
            Character.getInitializedLayer(
              id,
              rarity,
              type,
              attackType,
              layerLevelAttackType,
              gearLevelStatusBonus,
              flavorText,
              base
            )
          );
          base.layerIds[rarity][id] = true;
        }
      },

      deleteLayer: (state, { payload: { id } }) => {
        const { baseId, rarity } = state.layers.entities[id];
        adapters.layers.removeOne(state.layers, id);
        delete state.bases.entities[baseId].layerIds[rarity][id];
      },

      updateLayerField: (state, { payload: { id, keys, value } }) => {
        deepUpdate(state.layers.entities[id], keys, value);
      },

      mergeLayerFields: (state, { payload: { id, changes } }) => {
        mergeFields(state.layers.entities[id], changes);
      },

      updateSkillInCharacter: (state, { payload: { baseId, layerId, index, skillId } }) => {
        const { layerIds, skillLevels } = state.bases.entities[baseId];
        const { entities } = state.layers;
        const { skillIds } = entities[layerId];
        const currentSkillId = skillIds[index];
        if (currentSkillId) {
          skillLevels[skillId] = skillLevels[currentSkillId];
          if (
            !Character.getLayersBelonging(layerIds)
              .filter(id => id !== layerId)
              .some(id => entities[id].skillIds.includes(currentSkillId))
          ) {
            delete skillLevels[currentSkillId];
          }
        } else {
          skillLevels[skillId] = 99;
        }
        skillIds[index] = skillId;
      },

      setLayerStatusBonus: (state, { payload: { id } }) => {
        adapters.layers.updateOne(state.layers, {
          id,
          changes: Character.getLayerStatusBonus(state.layers.entities[id]),
        });
      },

      setLayerStatus: (state, { payload: { base, layer } }) => {
        adapters.layers.updateOne(state.layers, { id: layer.id, changes: Character.getLayerStatus(base, layer) });
      },

      setLayerStatusBonusAndStatus: (state, { payload: { base, layer } }) => {
        const updates = Character.getLayerStatusBonus(layer);
        adapters.layers.updateOne(state.layers, {
          id: layer.id,
          changes: { ...updates, ...Character.getLayerStatus(base, { ...layer, ...updates }) },
        });
      },

      updateLevel: (state, { payload: { layerLevel, gearLevel, intimacyLevel, baseId, layerId } }) => {
        const base = state.bases.entities[baseId];
        const layer = state.layers.entities[layerId];

        if (layerLevel) {
          layer.layerLevel = layerLevel;
        }
        if (gearLevel) {
          layer.gearLevel = gearLevel;
        }
        if (intimacyLevel) {
          layer.intimacyLevel = intimacyLevel;
        }
        const { bonus, initialTransGauge } = Character.getLayerStatusBonus(layer);
        layer.bonus = bonus;
        layer.initialTransGauge = initialTransGauge;
        const { status } = Character.getLayerStatus(base, layer);
        layer.status = status;
      },

      updateScale: (state, { payload: { tag, value, baseId, layerId } }) => {
        const base = state.bases.entities[baseId];
        const layer = state.layers.entities[layerId];

        layer.scale[tag] = value;
        const { status } = Character.getLayerStatus(base, layer);
        layer.status = status;
      },

      setAllLayerStatusesBelongingToBase: (state, { payload: { id } }) => {
        if (id) {
          const base = state.bases.entities[id];
          for (const layerId of Character.getLayersBelonging(base.layerIds)) {
            const layer = state.layers.entities[layerId];
            layer.status = Character.getLayerStatus(base, layer).status;
          }
        }
      },

      updateBaseStatus: (state, { payload: { status, id } }) => {
        if (id) {
          const base = state.bases.entities[id];
          base.status = { ...base.status, ...status };
          for (const layerId of Character.getLayersBelonging(base.layerIds)) {
            const layer = state.layers.entities[layerId];
            layer.status = Character.getLayerStatus(base, layer).status;
          }
        }
      },

      updateNonEntityField: (state, { payload: { keys, value } }) => {
        deepUpdate(state.nonEntityFields, keys, value);
      },

      onChangeBaseStatusTableData: (state, { payload: { ids, data } }) => {
        // Loop order: (2 + numStatus) * the number of base characters ~ thousands ~ μs
        const { entities } = state.bases;
        const statusTags = terms.statusTags.values;
        const numStatus = statusTags.length;
        const numBases = ids.length;

        for (let j = 0; j < numBases; j++) {
          const row = data[j];
          const id = ids[j];
          const base = entities[id];
          const [growthStepCell, hpCell, ...statusCells] = row;
          /** Growth step */
          if (growthStepCell == null) {
            base.growthStep = 0;
          } else {
            const value = Number(growthStepCell.value);
            if (verifyInputNumber(value, { inputTypeInteger: true }) && base.growthStep !== value) {
              base.growthStep = value;
            }
          }
          /** HP */
          if (hpCell == null) {
            base.hp = 0;
          } else {
            const value = Number(hpCell.value);
            if (verifyInputNumber(value, { inputTypeInteger: true }) && base.hp !== value) {
              base.hp = value;
            }
          }
          /** Base status */
          const { status } = base;
          let updateFlag = false;
          for (let i = 0; i < numStatus; i++) {
            const tag = statusTags[i];
            const cell = statusCells[i];
            if (cell == null) {
              status[tag] = 0;
              updateFlag = true;
            } else {
              const value = Number(cell.value);
              if (verifyInputNumber(value, { inputTypeInteger: true }) && base.status[tag] !== value) {
                status[tag] = value;
                updateFlag = true;
              }
            }
          }
          if (updateFlag) {
            for (const layerId of Character.getLayersBelonging(base.layerIds)) {
              adapters.layers.updateOne(state.layers, {
                id: layerId,
                changes: Character.getLayerStatus(base, state.layers.entities[layerId]),
              });
            }
          }
        }
      },

      onChangeGrowthLimitTableData: (state, { payload: { ids, data } }) => {
        /** Growth limit values */
        const { entities } = state.layers;
        const statusTags = terms.statusTags.values;
        const numStatus = statusTags.length;
        const numLayers = ids.length;

        for (let j = 0; j < numLayers; j++) {
          const row = data[j];
          const id = ids[j];
          // Status growth limits
          const { statusGrowthLimit } = entities[id];
          for (let i = 0; i < numStatus; i++) {
            const tag = statusTags[i];
            const cell = row[i];
            if (cell == null) {
              statusGrowthLimit[tag] = undefined;
            } else {
              const value = Number(cell.value);
              if (verifyInputNumber(value, { inputTypeInteger: true }) && statusGrowthLimit?.[tag] !== value) {
                statusGrowthLimit[tag] = value;
              }
            }
          }
        }
      },

      hundleRaiseHP: (state, { payload: { ids, checkboxes } }) => {
        const { maxHP } = state.nonEntityFields;
        const { entities } = state.bases;
        const numBases = ids.length;

        for (let i = 0; i < numBases; i++) {
          if (checkboxes[i]) {
            const id = ids[i];
            const base = entities[id];
            if (base.hp < maxHP) {
              base.hp = maxHP;
            }
          }
        }
      },

      hundleRaiseBaseStatus: (state, { payload: { ids, checkboxes, differenceTableData } }) => {
        const { entities } = state.bases;
        const statusTags = terms.statusTags.values;
        const numStatus = statusTags.length;
        const numBases = ids.length;

        for (let i = 0; i < numBases; i++) {
          if (checkboxes[i]) {
            const { status, growthStep } = entities[ids[i]];
            const differenceRow = differenceTableData[i];

            if (growthStep > 0) {
              for (let j = 0; j < numStatus; j++) {
                const difference = differenceRow[j + 4].value as number;
                if (difference < 0) {
                  status[statusTags[j]] += Math.ceil(-difference / growthStep) * growthStep;
                }
              }
            }
          }
        }
      },

      activateDatabase: () => {
        gateway.activateDatabase();
      },

      load: state => {
        return { ...state, ...gateway.load() };
      },

      save: state => {
        gateway.save(state);
      },
    } as {
      [K in keyof ReducerActionsInput]: (
        state: StateSchema,
        action: PayloadAction<ReducerActionsInput[K]>
      ) => void | StateSchema;
    },
  });

export type ReducerActions = {
  [K in keyof ReducerActionsInput]: (payload: ReducerActionsInput[K]) => ReducerActionsOutput;
};

/**
 * Type of record which contains custom selectors related to the identifier.
 */
type SelectorsType = {
  [K in keyof PickType<AdapterRecords, typeof name>]: {
    standard: EntitySelectors<PickType<IEntityPremitives, typeof name>[K], RootState>;
    custom: SortFilterableEntitySelector<PickType<IEntityPremitives, typeof name>[K]>;
    extract: SortFilterableEntitySelector<PickType<IEntityPremitives, typeof name>[K]>;
  };
};

/**
 * Get a record of custom selectors related to the identifier.
 */
export const getCustomSelectors = (): SelectorsType =>
  getSelectors<keyof PickType<AdapterRecords, typeof name>, SelectorsType>(name);

export { defaultData, name };
