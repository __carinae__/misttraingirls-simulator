import { createSelector, EntityAdapter, EntitySelectors, OutputSelector } from '@reduxjs/toolkit';

import * as adapters from '@@core/adapters';
import { sortFunctions, targetAccessors } from '@@core/domain';
import { deepAccess } from '@@core/utils';
import { Identifiers } from '@@identifier/core';
import { EntityState, IEntityFrame, SelectorPayload, SortFilterProps } from '@@interface';

import type { RootState } from './RootState';

const sortFilterEntities = (payload: SelectorPayload<IEntityFrame>) => {
  const { entitylist, sortFunctionId, sortOrder, filters, name, key }: SelectorPayload<IEntityFrame> = {
    sortFunctionId: 'id',
    sortOrder: 'asc',
    filters: {},
    ...payload,
  };
  const sortFunc = sortFunctions[name][key][sortFunctionId](sortOrder);

  // Sort all the entities by a given key and order.
  let res = entitylist.slice().sort(sortFunc);

  // Filter by given keys and queries.
  for (const [key, options] of Object.entries(filters)) {
    if (key != null && Object.keys(options).length) {
      res = res.filter(a => {
        const ak = deepAccess(a, key.split('.'));
        if (Array.isArray(ak)) {
          for (const elm of ak) {
            if (options.hasOwnProperty(elm as string | number)) {
              return true;
            }
          }
          return false;
        } else {
          return options.hasOwnProperty(ak as string | number);
        }
      });
    }
  }
  const ids = res.map(entity => entity.id);
  const entities = Object.fromEntries(res.map(entity => [entity.id, entity]));
  return { ids, entities };
};

export type SortFilterableEntitySelector<IEntity> = OutputSelector<
  [(state: RootState, props: SortFilterProps) => SelectorPayload<IEntity>],
  EntityState<IEntity>,
  [props: SortFilterProps],
  ((args_0: SelectorPayload<IEntity>, ...args_1: unknown[]) => EntityState<IEntity>) & {
    clearCache: () => void;
  }
>;

export function getSelectors<
  Keys extends string,
  SelectorsType extends {
    [K in Keys]: {
      standard: EntitySelectors<IEntityFrame, RootState>;
      custom: SortFilterableEntitySelector<IEntityFrame>;
      extract: SortFilterableEntitySelector<IEntityFrame>;
    };
  }
>(name: keyof Identifiers): SelectorsType {
  const adapterRecord = adapters[name].adapterRecord;
  return Object.fromEntries(
    Object.keys(adapterRecord).map(key => {
      const adapter = adapterRecord[key] as EntityAdapter<IEntityFrame>;
      const selectors = adapter.getSelectors((state: RootState) => state.present.core[name][key]);
      return [
        key,
        {
          /** Default selectors of redux-toolkit. */
          standard: selectors,
          /** Selectors with sort & filter functionalities. */
          custom: createSelector((state: RootState, props: SortFilterProps) => {
            return { entitylist: selectors.selectAll(state), ...props, name, key };
          }, sortFilterEntities),
          /** Memorized selectors with sort & filter functionalities, which contain entities only with the required properties. */
          extract: createSelector((state: RootState, props: SortFilterProps) => {
            let memoProps = ['id'];
            if (props.sortFunctionId != null) {
              for (const accessKey of targetAccessors[name][key][props.sortFunctionId].keys) {
                memoProps.push(accessKey[0]);
              }
            }
            if (props.filters != null) {
              for (const key of Object.keys(props.filters)) {
                memoProps.push(key.split('.')[0]);
              }
            }
            memoProps = Array.from(new Set(memoProps));
            return {
              entitylist: selectors
                .selectAll(state)
                .map(entity => Object.fromEntries(memoProps.map(prop => [prop, entity[prop]]))) as IEntityFrame[],
              ...props,
              name,
              key,
            };
          }, sortFilterEntities),
        },
      ];
    })
  ) as SelectorsType;
}
