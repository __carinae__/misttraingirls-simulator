import { configureStore } from '@reduxjs/toolkit';
import { combineReducers } from '@reduxjs/toolkit';
import { TypedUseSelectorHook, useSelector as rawUseSelector } from 'react-redux';
import undoable, { combineFilters, excludeAction } from 'redux-undo';

import * as coreModules from './dependencyInjection';
import type { RootState } from './RootState';
import * as uiModules from './ui';

export const useSelector: TypedUseSelectorHook<RootState> = rawUseSelector;
export const useSelectorWithInitialValue = <T>(func: (state: RootState) => T, init: T): T => {
  return rawUseSelector(func) ?? init;
};
import logger from 'redux-logger';

/**
 * Define a dictionary of selector groups which can be invoked by a redux module name.
 */
export const selectors = {
  /** core */
  weaponMaster: coreModules.weaponMaster.getCustomSelectors(),
  character: coreModules.character.getCustomSelectors(),
  skill: coreModules.skill.getCustomSelectors(),
  specialSkill: coreModules.specialSkill.getCustomSelectors(),
  ability: coreModules.ability.getCustomSelectors(),
  action: coreModules.action.getCustomSelectors(),
};

/**
 * Define a dictionary of action groups which can be invoked by a redux module name.
 */
export const actions = {
  /** core */
  weaponMaster: coreModules.weaponMaster.slice.actions as unknown as coreModules.weaponMaster.ReducerActions,
  character: coreModules.character.slice.actions as unknown as coreModules.character.ReducerActions,
  skill: coreModules.skill.slice.actions as unknown as coreModules.skill.ReducerActions,
  specialSkill: coreModules.specialSkill.slice.actions as unknown as coreModules.specialSkill.ReducerActions,
  ability: coreModules.ability.slice.actions as unknown as coreModules.ability.ReducerActions,
  action: coreModules.action.slice.actions as unknown as coreModules.action.ReducerActions,
  /** ui */
  help: uiModules.help.slice.actions as unknown as uiModules.help.ReducerActions,
  metaFlags: uiModules.metaFlags.slice.actions as unknown as uiModules.metaFlags.ReducerActions,
  characterPanel: uiModules.character.slice.actions as unknown as uiModules.character.ReducerActions,
  skillPanel: uiModules.skill.slice.actions as unknown as uiModules.skill.ReducerActions,
};

/**
 * Define a redux store.
 */
const store = configureStore({
  reducer: undoable(
    combineReducers({
      core: combineReducers(
        Object.fromEntries(Object.values(coreModules).map(__module => [__module.name, __module.slice.reducer]))
      ),
      ui: combineReducers(
        Object.fromEntries(Object.values(uiModules).map(__module => [__module.name, __module.slice.reducer]))
      ),
    }),
    {
      limit: 10,
      filter: combineFilters(
        excludeAction(
          Object.values(uiModules)
            .map(__module => __module.excludedActionTypes)
            .flat()
        ),
        (action, state) => {
          return state.ui.metaFlags.recordingAllowed;
        }
      ),
    }
  ),
  middleware: getDefaultMiddleware => {
    const middleware = [...getDefaultMiddleware({ serializableCheck: false })];
    if (process.env.NODE_ENV === `development`) {
      middleware.push(logger);
    }
    return middleware;
  },
});
export default store;
export type AppDispatch = typeof store.dispatch;

/**
 * Activate all databases.
 */
export const activateDatabase = (dispatch: AppDispatch): void => {
  for (const __module of Object.values(coreModules)) {
    const { slice } = __module;
    if (slice.actions.hasOwnProperty('activateDatabase')) {
      dispatch(slice.actions.activateDatabase({}));
    }
  }
};

/**
 * Load all the states.
 */
export const load = (dispatch: AppDispatch): void => {
  for (const __module of Object.values(coreModules)) {
    const { slice } = __module;
    if (slice.actions.hasOwnProperty('load')) {
      dispatch(slice.actions.load({}));
    }
  }
};

/**
 * Save all the states.
 */
export const save = (dispatch: AppDispatch): void => {
  for (const __module of Object.values(coreModules)) {
    const { slice } = __module;
    if (slice.actions.hasOwnProperty('save')) {
      dispatch(slice.actions.save({}));
    }
  }
};

/**
 *  Prepare for consecutive actions to be seen as a unit action in undo/redo history.
 */
export const prepareForConsecutiveActions = (dispatch: AppDispatch): void => {
  dispatch(actions.metaFlags.stampState({}));
  dispatch(actions.metaFlags.setRecordingPermission(false));
};

/**
 *  Finish consecutive actions to be seen as a unit action in undo/redo history.
 */
export const finishConsecutiveActions = (dispatch: AppDispatch): void => {
  dispatch(actions.metaFlags.setRecordingPermission(true));
  dispatch(actions.metaFlags.stampState({}));
};
