import * as adapters from '@@core/adapters';
import { createSliceFunc, defaultData, getCustomSelectors, name, ReducerActions } from '@@core/redux/core/character';
import * as repository from '@@core/repository';
import { DefaultDataSchemas, PickType } from '@@interface';

const gateway = new repository[name].Gateway(defaultData as PickType<DefaultDataSchemas, typeof name>, name);
const adapterRecord = adapters[name].adapterRecord;

export const slice = createSliceFunc(gateway, adapterRecord);
export { getCustomSelectors, name };
export type { ReducerActions };
