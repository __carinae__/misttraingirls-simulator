import { createSlice, PayloadAction, Slice } from '@reduxjs/toolkit';

import { name } from '@@identifier/ui/skill';
import { PickType, ReducerActionsOutput, SortFilterProps, StateSchemas } from '@@interface';
type StateSchema = PickType<StateSchemas['ui'], typeof name>;

export const initialState: StateSchema = {
  selectedId: { skill: undefined },
  selectedIdBuffer: { skill: false },
  sortFilterProps: {
    skills: {
      sortFunctionId: 'impactRankNumbered:sp.maxAwaken',
      sortOrder: 'desc' as const,
      filters: {},
    },
  },
};

export type ReducerActionsInput = {
  setSkillId: string | false;
  setSkillIdBuffer: string | false;
  setSkillSortFilterProps: SortFilterProps;
};

const reducers = {
  setSkillId: (state, { payload: id }) => {
    state.selectedId.skill = id;
  },
  setSkillIdBuffer: (state, { payload: id }) => {
    state.selectedIdBuffer.skill = id;
  },
  setSkillSortFilterProps: (state, { payload: sortFilterProps }) => {
    state.sortFilterProps.skills = sortFilterProps;
  },
} as {
  [K in keyof ReducerActionsInput]: (
    state: StateSchema,
    action: PayloadAction<ReducerActionsInput[K]>
  ) => void | StateSchema;
};

const slice: Slice = createSlice({
  name,
  initialState,
  reducers,
});

export type ReducerActions = {
  [K in keyof ReducerActionsInput]: (payload: ReducerActionsInput[K]) => ReducerActionsOutput;
};

export const excludedActionTypes = Object.keys(reducers).map(actionType => name + '/' + actionType);

export { name, slice };
