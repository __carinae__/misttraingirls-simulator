import { createSlice, PayloadAction, Slice } from '@reduxjs/toolkit';

import { name } from '@@identifier/ui/help';
import { PickType, ReducerActionsOutput, StateSchemas } from '@@interface';
type StateSchema = PickType<StateSchemas['ui'], typeof name>;

export const initialState: StateSchema = {
  open: false,
};

export type ReducerActionsInput = {
  setOpen: boolean;
};

const reducers = {
  setOpen: (state, { payload: open }) => {
    state.open = open;
  },
} as {
  [K in keyof ReducerActionsInput]: (
    state: StateSchema,
    action: PayloadAction<ReducerActionsInput[K]>
  ) => void | StateSchema;
};

const slice: Slice = createSlice({
  name,
  initialState,
  reducers,
});

export type ReducerActions = {
  [K in keyof ReducerActionsInput]: (payload: ReducerActionsInput[K]) => ReducerActionsOutput;
};

export const excludedActionTypes = Object.keys(reducers).map(actionType => name + '/' + actionType);

export { name, slice };
