import { createSlice, PayloadAction, Slice } from '@reduxjs/toolkit';

import { name } from '@@core/__identifier__/ui/metaFlags';
import { PickType, ReducerActionsOutput, StateSchemas } from '@@interface';

type StateSchema = PickType<StateSchemas['ui'], typeof name>;

export const initialState: StateSchema = {
  mounted: false,
  recordingAllowed: true,
};

export type ReducerActionsInput = {
  setMounted: boolean;
  stampState: Record<string, never>;
  setRecordingPermission: boolean;
};

const reducers = {
  setMounted: (state, { payload: mounted }) => {
    state.mounted = mounted;
  },
  stampState: () => {
    /**
     * Do nothing here, but stamp the present state to past if there is any state change including ui state.
     * This is useful to record the panel shift or entity tab shift which are required for better UX in undoing state.
     */
  },
  setRecordingPermission: (state, { payload: recordingAllowed }) => {
    state.recordingAllowed = recordingAllowed;
  },
} as {
  [K in keyof ReducerActionsInput]: (
    state: StateSchema,
    action: PayloadAction<ReducerActionsInput[K]>
  ) => void | StateSchema;
};

const slice: Slice = createSlice({
  name,
  initialState,
  reducers,
});

export type ReducerActions = {
  [K in keyof ReducerActionsInput]: (payload: ReducerActionsInput[K]) => ReducerActionsOutput;
};

export const excludedActionTypes = ['setMounted', 'setRecordingPermission'].map(actionType => name + '/' + actionType);

export { name, slice };
