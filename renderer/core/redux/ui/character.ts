import { createSlice, PayloadAction, Slice } from '@reduxjs/toolkit';

import { name } from '@@identifier/ui/character';
import { PickType, ReducerActionsOutput, SortFilterProps, StateSchemas } from '@@interface';

type StateSchema = PickType<StateSchemas['ui'], typeof name>;

export const initialState: StateSchema = {
  selectedId: { base: undefined, layer: false }, // set base: undifined to enable useSelectorWithInitialValue work at the beginning of rendering
  selectedIdBuffer: { base: false, layer: false },
  sortFilterProps: {
    bases: {
      sortFunctionId: 'id',
      sortOrder: 'asc' as const,
      filters: {},
    },
    layers: {
      sortFunctionId: 'rarity:baseId:index',
      sortOrder: 'desc' as const,
      filters: {},
    },
  },
  GrowthTable: {
    open: false,
    tableId: 'status',
  },
};

export type ReducerActionsInput = {
  setBaseId: string | false;
  setLayerId: string | false;
  selectBaseTab: { id: string | false; layerIdLinked: string | false };
  selectLayerTab: { id: string | false; baseIdLinked: string | false };
  setBaseIdBuffer: string | false;
  setLayerIdBuffer: string | false;
  setBaseSortFilterProps: SortFilterProps;
  setLayerSortFilterProps: SortFilterProps;
  setOpenGrowthTable: boolean;
  setGrowthTableId: string;
};

const reducers = {
  setBaseId: (state, { payload: id }) => {
    state.selectedId.base = id;
  },
  setLayerId: (state, { payload: id }) => {
    state.selectedId.layer = id;
  },
  selectBaseTab: (state, { payload: { id, layerIdLinked } }) => {
    state.selectedId.base = id;
    state.selectedId.layer = layerIdLinked;
  },
  selectLayerTab: (state, { payload: { id, baseIdLinked } }) => {
    state.selectedId.layer = id;
    state.selectedId.base = baseIdLinked;
  },
  setBaseIdBuffer: (state, { payload: id }) => {
    state.selectedIdBuffer.base = id;
  },
  setLayerIdBuffer: (state, { payload: id }) => {
    state.selectedIdBuffer.layer = id;
  },
  setBaseSortFilterProps: (state, { payload: sortFilterProps }) => {
    state.sortFilterProps.bases = sortFilterProps;
  },
  setLayerSortFilterProps: (state, { payload: sortFilterProps }) => {
    state.sortFilterProps.layers = sortFilterProps;
  },
  setOpenGrowthTable: (state, { payload: open }) => {
    state.GrowthTable.open = open;
  },
  setGrowthTableId: (state, { payload: tableId }) => {
    state.GrowthTable.tableId = tableId;
  },
} as {
  [K in keyof ReducerActionsInput]: (
    state: StateSchema,
    action: PayloadAction<ReducerActionsInput[K]>
  ) => void | StateSchema;
};

const slice: Slice = createSlice({
  name,
  initialState,
  reducers,
});

export type ReducerActions = {
  [K in keyof ReducerActionsInput]: (payload: ReducerActionsInput[K]) => ReducerActionsOutput;
};

export const excludedActionTypes = Object.keys(reducers).map(actionType => name + '/' + actionType);

export { name, slice };
