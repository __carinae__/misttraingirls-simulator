import { StateWithHistory } from 'redux-undo';

import { StateSchemas } from '@@core/__interface__';

export type RootState = StateWithHistory<StateSchemas>;
