import { createEntityAdapter } from '@reduxjs/toolkit';

import { name } from '@@identifier/core/specialSkill';
import { AdapterRecords, ISpecialSkill, PickType } from '@@interface';

export const adapterRecord: PickType<AdapterRecords, typeof name> = {
  specialSkills: createEntityAdapter<ISpecialSkill>(),
};
