import { createEntityAdapter } from '@reduxjs/toolkit';

import { name } from '@@identifier/core/skill';
import { AdapterRecords, ISkill, PickType } from '@@interface';

export const adapterRecord: PickType<AdapterRecords, typeof name> = {
  skills: createEntityAdapter<ISkill>(),
};
