import { createEntityAdapter } from '@reduxjs/toolkit';

import { name } from '@@identifier/core/imageCache';
import { AdapterRecords, ImageCacheBase64, PickType } from '@@interface';

export const adapterRecord: PickType<AdapterRecords, typeof name> = {
  sdGifs: createEntityAdapter<ImageCacheBase64>(),
};
