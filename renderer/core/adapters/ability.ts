import { createEntityAdapter } from '@reduxjs/toolkit';

import { name } from '@@identifier/core/ability';
import { AdapterRecords, IAbility, PickType } from '@@interface';

export const adapterRecord: PickType<AdapterRecords, typeof name> = {
  abilities: createEntityAdapter<IAbility>(),
};
