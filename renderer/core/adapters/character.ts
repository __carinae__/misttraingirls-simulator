import { createEntityAdapter } from '@reduxjs/toolkit';

import { name } from '@@identifier/core/character';
import { AdapterRecords, IBase, ILayer, PickType } from '@@interface';

export const adapterRecord: PickType<AdapterRecords, typeof name> = {
  bases: createEntityAdapter<IBase>(),
  layers: createEntityAdapter<ILayer>(),
};
