import { createEntityAdapter } from '@reduxjs/toolkit';

import { name } from '@@identifier/core/weaponMaster';
import { AdapterRecords, IWeaponMaster, PickType } from '@@interface';

export const adapterRecord: PickType<AdapterRecords, typeof name> = {
  weaponMasters: createEntityAdapter<IWeaponMaster>(),
};
