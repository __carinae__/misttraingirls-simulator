import { createEntityAdapter } from '@reduxjs/toolkit';

import { name } from '@@identifier/core/action';
import { AdapterRecords, IAction, PickType } from '@@interface';

export const adapterRecord: PickType<AdapterRecords, typeof name> = {
  actions: createEntityAdapter<IAction>(),
};
