import * as core from './core';
import * as ui from './ui';

export type WholeIdentifiers = core.Identifiers & ui.Identifiers;
