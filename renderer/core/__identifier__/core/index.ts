import * as ability from './ability';
import * as action from './action';
import * as character from './character';
import * as imageCache from './imageCache';
import * as skill from './skill';
import * as specialSkill from './specialSkill';
import * as weaponMaster from './weaponMaster';

export type Identifiers = {
  [imageCache.name]: typeof imageCache.name;
  [weaponMaster.name]: typeof weaponMaster.name;
  [character.name]: typeof character.name;
  [skill.name]: typeof skill.name;
  [specialSkill.name]: typeof specialSkill.name;
  [ability.name]: typeof ability.name;
  [action.name]: typeof action.name;
};
