import * as character from './character';
import * as help from './help';
import * as metaFlags from './metaFlags';
import * as skill from './skill';

export type Identifiers = {
  [character.name]: typeof character.name;
  [skill.name]: typeof skill.name;
  [help.name]: typeof help.name;
  [metaFlags.name]: typeof metaFlags.name;
};
