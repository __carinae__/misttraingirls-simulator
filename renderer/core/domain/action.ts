import { IAction } from '@@interface';

/**
 * The domain class mainly handling action objects.
 */
export class Action {
  /**
   * Return an initialized action object with a given id.
   */
  static getInitializedAction = (id: string): IAction => {
    return {
      id,
      sp: { default: 0, maxAwakenLevel: 0 },
      actionPieces: [],
      fast: false,
      delay: false,
    };
  };
}
