import { ISkill } from '@@interface';

/**
 * The domain class mainly handling skill objects.
 */
export class Skill {
  /**
   * Return an initialized skill object with a given id.
   */
  static getInitializedSkill = (id: string): ISkill => {
    return {
      id,
      sp: { default: 3, maxAwakenLevel: 1 },
      rp: 0,
      range: '近',
      targetOption: '単体',
      attributes: [],
      attackStatus: [],
      impactRank: '─',
      actionPieces: [],
      fast: false,
      delay: false,
      description: '',
    };
  };
}
