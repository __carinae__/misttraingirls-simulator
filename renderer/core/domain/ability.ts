import { IAbility } from '@@interface';

/**
 * The domain class mainly handling ability objects.
 */
export class Ability {
  /**
   * Return an initialized ability object with a given id.
   */
  static getInitializedAbility = (id: string): IAbility => {
    return {
      id,
      effects: [],
    };
  };
}
