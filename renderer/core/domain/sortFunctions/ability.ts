import { name } from '@@identifier/core/ability';

import * as targetAccessors from '../targetAccessor';
import { createSortFunctionsForBasicProps } from './shared';

export const abilities = {
  ...createSortFunctionsForBasicProps(targetAccessors[name].abilities),
};
