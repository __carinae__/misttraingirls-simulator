import { name } from '@@identifier/core/character';

import * as targetAccessors from '../targetAccessor';
import { createNestedSortFunctions, createSortFunctionsForBasicProps } from './shared';

const baseAccessors = targetAccessors[name].bases;
export const bases = {
  ...createSortFunctionsForBasicProps(baseAccessors),
};

const layerAccessors = targetAccessors[name].layers;
export const layers = {
  ...createSortFunctionsForBasicProps(layerAccessors),
  ...createNestedSortFunctions(['baseId'], layerAccessors, ['rarity', 'desc'], ['index', 'desc']),
  ...createNestedSortFunctions(['rarity'], layerAccessors, ['baseId', 'asc'], ['index', 'desc']),
  ...createNestedSortFunctions(['type'], layerAccessors, ['baseId', 'asc'], ['rarity', 'desc'], ['index', 'desc']),
  ...createNestedSortFunctions(
    ['attackType'],
    layerAccessors,
    ['baseId', 'asc'],
    ['rarity', 'desc'],
    ['index', 'desc']
  ),
};
