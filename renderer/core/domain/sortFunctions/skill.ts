import { name } from '@@identifier/core/skill';

import * as targetAccessors from '../targetAccessor';
import { createNestedSortFunctions, createSortFunctionsForBasicProps } from './shared';

const skillAccessors = targetAccessors[name].skills;
export const skills = {
  ...createSortFunctionsForBasicProps(skillAccessors),
  ...createNestedSortFunctions(
    ['sp.default'],
    skillAccessors,
    ['impactRankNumbered', 'desc'],
    ['sp.maxAwakenLevel', 'desc']
  ),
  ...createNestedSortFunctions(
    ['sp.maxAwakenLevel'],
    skillAccessors,
    ['sp.default', 'desc'],
    ['impactRankNumbered', 'desc']
  ),
  ...createNestedSortFunctions(['sp.maxAwaken'], skillAccessors, ['impactRankNumbered', 'desc']),
  ...createNestedSortFunctions(['rp'], skillAccessors, ['sp.maxAwaken', 'desc'], ['impactRankNumbered', 'desc']),
  ...createNestedSortFunctions(['impactRankNumbered'], skillAccessors, ['sp.maxAwaken', 'desc']),
  ...createNestedSortFunctions(['range'], skillAccessors, ['impactRankNumbered', 'desc'], ['sp.maxAwaken', 'desc']),
  ...createNestedSortFunctions(
    ['targetOption'],
    skillAccessors,
    ['impactRankNumbered', 'desc'],
    ['sp.maxAwaken', 'desc']
  ),
};
