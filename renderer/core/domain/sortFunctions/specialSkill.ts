import { name } from '@@identifier/core/specialSkill';

import * as targetAccessors from '../targetAccessor';
import { createSortFunctionsForBasicProps } from './shared';

export const specialSkills = {
  ...createSortFunctionsForBasicProps(targetAccessors[name].specialSkills),
};
