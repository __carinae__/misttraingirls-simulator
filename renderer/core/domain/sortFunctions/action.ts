import { name } from '@@identifier/core/action';

import * as targetAccessors from '../targetAccessor';
import { createSortFunctionsForBasicProps } from './shared';

export const actions = {
  ...createSortFunctionsForBasicProps(targetAccessors[name].actions),
};
