import { deepAccess } from '@@core/utils';
import { IEntityFrame, SortFunction, TargetAccessor } from '@@interface';

export const makeAccessSortTarget = (accessor: TargetAccessor, entity: IEntityFrame): number | string => {
  return accessor.func(...accessor.keys.map(key => deepAccess(entity, key)));
};

export const basicSortFunction = (
  accessors: Record<string, TargetAccessor>,
  targetAccessorId: string
): SortFunction<IEntityFrame> => {
  const accessSortTarget = (entity: IEntityFrame) => makeAccessSortTarget(accessors[targetAccessorId], entity);

  return (sortOrder: 'asc' | 'desc') =>
    (a: IEntityFrame, b: IEntityFrame): number => {
      const [ak, bk] = [accessSortTarget(a), accessSortTarget(b)];
      if (ak != null) {
        if (bk != null) {
          const [left, right] = sortOrder === 'asc' ? [ak, bk] : [bk, ak];
          return left.toString().localeCompare(right.toString(), 'ja', { numeric: true });
        } else {
          return sortOrder === 'asc' ? 1 : -1;
        }
      } else {
        if (bk != null) {
          return sortOrder === 'asc' ? -1 : 1;
        } else {
          return 0;
        }
      }
    };
};

export const nestedSortFunction = (
  accessors: Record<string, TargetAccessor>,
  targetAccessorId: string,
  ...args: [string, 'asc' | 'desc'][]
): SortFunction<IEntityFrame> => {
  const func = basicSortFunction(accessors, targetAccessorId);

  return (sortOrder: 'asc' | 'desc') =>
    (a: IEntityFrame, b: IEntityFrame): number => {
      const res = func(sortOrder)(a, b);
      if (res === 0) {
        const [[nextTargetAccessorId, nextOrder], ...rest] = args;
        return nestedSortFunction(accessors, nextTargetAccessorId, ...rest)(nextOrder)(a, b);
      } else {
        return res;
      }
    };
};

export const nestedSortFunctionIdCompensation = (
  accessors: Record<string, TargetAccessor>,
  targetAccessorId: string,
  ...args: [string, 'asc' | 'desc'][]
): SortFunction<IEntityFrame> => nestedSortFunction(accessors, targetAccessorId, ...args, ['id', 'asc']);

export const createSortFunctionsForBasicProps = (
  accessors: Record<string, TargetAccessor>
): Record<string, SortFunction<IEntityFrame>> =>
  Object.fromEntries(
    Object.keys(accessors).map(targetAccessorId => [
      targetAccessorId,
      nestedSortFunctionIdCompensation(accessors, targetAccessorId),
    ])
  );

export const createNestedSortFunctions = (
  targetAccessorIds: string[],
  accessors: Record<string, TargetAccessor>,
  ...args: [string, 'asc' | 'desc'][]
): Record<string, SortFunction<IEntityFrame>> => {
  const suffix = args.map(arg => arg[0]).join(':');
  return Object.fromEntries(
    targetAccessorIds.map(targetAccessorId => [
      [targetAccessorId, suffix].join(':'),
      nestedSortFunctionIdCompensation(accessors, targetAccessorId, ...args),
    ])
  );
};
