import { Attribute, Weapon } from '@@interface';

export const mainWeaponAttribute = (weapon: Weapon): Attribute => {
  switch (weapon) {
    case '剣':
      return '刃';
    case '斧':
      return '刃';
    case '魔具':
      return '刃';
    case '拳':
      return '衝';
    case '鞭':
      return '衝';
    case '杖':
      return '衝';
    case '弓':
      return '貫';
    case '槍':
      return '貫';
    case '銃':
      return '貫';
  }
};
