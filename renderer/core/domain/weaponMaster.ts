import { IWeaponMaster } from '@@interface';

/**
 * The domain class mainly handling weapon master objects.
 */
export class WeaponMaster {
  /**
   * Compute weapon master properties.
   */
  static computeConditions = (level: number): IWeaponMaster => {
    return {
      level,
      condition: { skill: Math.floor(level / 2) * 0.5, trans: Math.floor((level - 1) / 2) * 0.5 },
    };
  };
}
