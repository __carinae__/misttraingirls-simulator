import { ISpecialSkill } from '@@interface';

/**
 * The domain class mainly handling special skill objects.
 */
export class SpecialSkill {
  /**
   * Return an initialized special skill object with a given id.
   */
  static getInitializedSpecialSkill = (id: string): ISpecialSkill => {
    return {
      id,
      range: '近',
      targetOption: '単体',
      attributes: [],
      actionPieces: [],
    };
  };
}
