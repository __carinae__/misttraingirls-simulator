import { terms } from '@@interface';

import { directAccessor, nestedAccessors } from './shared';

export const bases = {
  ...Object.fromEntries(['id', 'weapon', 'attackType', 'from', 'race'].map(key => [key, directAccessor(key)])),
  ...nestedAccessors(['status'], terms.statusTags.values),
};
export const layers = {
  ...Object.fromEntries(
    ['id', 'baseId', 'rarity', 'index', 'type', 'attackType'].map(key => [key, directAccessor(key)])
  ),
  ...nestedAccessors(['scale'], terms.statusTags.values),
  ...nestedAccessors(['bonus'], terms.statusTags.values),
  ...nestedAccessors(['status'], terms.statusTags.values),
};
