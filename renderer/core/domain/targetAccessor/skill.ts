import { ImpactRank } from '@@core/__interface__';

import { directAccessor, nestedAccessors } from './shared';

export const skills = {
  ...Object.fromEntries(
    ['id', 'rp', 'impactRank', 'range', 'targetOption', 'attributes', 'status', 'fast', 'delay'].map(key => [
      key,
      directAccessor(key),
    ])
  ),
  ...nestedAccessors(['sp'], ['default', 'maxAwakenLevel']),
  'sp.maxAwaken': {
    id: 'sp.maxAwaken',
    keys: [
      ['sp', 'default'],
      ['sp', 'maxAwakenLevel'],
    ],
    func: (def: number, maxAwakenLevel: number): number => {
      return def - maxAwakenLevel;
    },
  },
  impactRankNumbered: {
    id: 'impactRankNumbered',
    keys: [['impactRank']],
    func: (prop: ImpactRank): number => {
      switch (prop) {
        case 'E':
          return 0;
        case 'D':
          return 1;
        case 'C':
          return 2;
        case 'B':
          return 3;
        case 'A':
          return 4;
        case 'S':
          return 5;
        case 'SS':
          return 6;
        case 'SSS':
          return 7;
      }
    },
  },
};
