import { directAccessor } from './shared';

export const abilities = {
  ...Object.fromEntries(['id'].map(key => [key, directAccessor(key)])),
  idRanked: {
    id: 'idRanked',
    keys: [['id']],
    func: (id: string): string => {
      return id.replace('極小', '0').replace('小', '1').replace('中', '2').replace('大', '3').replace('特大', '4');
    },
  },
};
