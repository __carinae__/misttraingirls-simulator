import { directAccessor, nestedAccessors } from './shared';

export const weaponMasters = {
  ...Object.fromEntries(['id', 'level'].map(key => [key, directAccessor(key)])),
  ...nestedAccessors(['condition'], ['skill', 'trans']),
};
