import { directAccessor } from './shared';

export const actions = Object.fromEntries(['id'].map(key => [key, directAccessor(key)]));
