import { directAccessor } from './shared';

export const specialSkills = Object.fromEntries(['id'].map(key => [key, directAccessor(key)]));
