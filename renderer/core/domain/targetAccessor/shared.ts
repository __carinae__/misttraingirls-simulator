import { TargetAccessor } from '@@interface';

export const directAccessor = (key: string): TargetAccessor => ({
  id: key,
  keys: [[key]],
  func: (prop: number | string): number | string => prop,
});

export const nestedAccessors = (rootKey: string[], values: Readonly<string[]>): Record<string, TargetAccessor> => {
  return Object.fromEntries(
    values.map(value => {
      const key = rootKey.concat([value]);
      const id = key.join('.');
      return [id, { id, keys: [key], func: (prop: number | string): number | string => prop }];
    })
  );
};
