import {
  Affiliation,
  AttackStatusTag,
  Attribute,
  IBase,
  ILayer,
  LayerType,
  Race,
  Rarity,
  StatusTag,
  terms,
  Weapon,
} from '@@interface';

import { mainWeaponAttribute } from './shared';

/**
 * The domain class mainly handling character objects.
 */
export class Character {
  /**
   * Return an initialized base object with a given id.
   */
  static getInitializedBase(id: string, weapon: Weapon, from: Affiliation, race: Race, hp: number): IBase {
    const baseStatusBasis = this.getBaseStatusBaseline(hp);
    return {
      id,
      weapon,
      from,
      race,
      hp,
      status: {
        物攻: baseStatusBasis,
        物防: baseStatusBasis,
        命中: baseStatusBasis,
        回復: baseStatusBasis,
        魔攻: baseStatusBasis,
        魔防: baseStatusBasis,
        速さ: baseStatusBasis,
        幸運: baseStatusBasis,
      },
      layerIds: { A: {}, S: {}, SS: {} },
      skillLevels: {},
      growthStep: 10,
    };
  }

  /**
   * Return basiline number of character's base status.
   */
  static getBaseStatusBaseline(hp: number): number {
    return hp <= 5000 ? Math.ceil(hp / 200) * 20 : Math.ceil(hp / 500) * 50;
  }

  /**
   * Get layers belonging to the given base.
   */
  static getLayersBelonging(layerIds: Record<Rarity, Record<string, boolean>>): string[] {
    const { A, S, SS } = layerIds;
    return Object.keys({ ...A, ...S, ...SS });
  }

  /**
   * Return an initialized base object with a given id.
   */
  static getInitializedLayer(
    id: string,
    rarity: Rarity,
    type: LayerType,
    attackType: AttackStatusTag,
    layerLevelAttackType: AttackStatusTag,
    gearLevelStatusBonus: { alpha: StatusTag; beta: StatusTag },
    flavorText: string,
    base: IBase
  ): ILayer {
    let priority: Attribute[];
    let values: number[];
    const { id: baseId, weapon } = base;
    switch (mainWeaponAttribute(weapon)) {
      case '刃':
        priority = ['刃', '貫', '衝'];
        break;
      case '衝':
        priority = ['衝', '刃', '貫'];
        break;
      case '貫':
        priority = ['貫', '衝', '刃'];
        break;
    }
    switch (rarity) {
      case 'A':
        values = [20, -20, -60];
        break;
      case 'S':
        values = [30, 0, -50];
        break;
      case 'SS':
        values = [40, 20, -40];
        break;
    }
    const resistance = {
      ...Object.fromEntries(priority.map((p, i) => [p, values[i]])),
      火: 0,
      水: 0,
      風: 0,
      光: 0,
      闇: 0,
    } as Record<Attribute, number>;

    const layer: ILayer = {
      id,
      baseId,
      rarity,
      index: Object.keys(base.layerIds[rarity]).length,
      type,
      attackType,
      layerLevelAttackType,
      gearLevelStatusBonus,
      layerLevel: 50,
      gearLevel: 20,
      intimacyLevel: 10,
      scale: {
        物攻: 100,
        物防: 100,
        命中: 100,
        回復: 100,
        魔攻: 100,
        魔防: 100,
        速さ: 100,
        幸運: 100,
      },
      skillIds: [undefined, undefined, undefined],
      SSkill: rarity === 'SS' ? '' : undefined,
      abilityIds: [undefined, undefined, undefined],
      resistance,
      flavorText,
      statusGrowthLimit: {
        物攻: undefined,
        物防: undefined,
        命中: undefined,
        回復: undefined,
        魔攻: undefined,
        魔防: undefined,
        速さ: undefined,
        幸運: undefined,
      },
    };
    const { bonus, initialTransGauge } = this.getLayerStatusBonus(layer);
    const { status } = this.getLayerStatus(base, { ...layer, bonus });
    return { ...layer, bonus, status, initialTransGauge };
  }

  /**
   * Get status bonus according to the base and layer informations.
   */
  static getLayerStatusBonus(layer: ILayer): ILayer {
    const {
      rarity,
      layerLevelAttackType,
      gearLevelStatusBonus: { alpha, beta },
      layerLevel,
      gearLevel,
      intimacyLevel,
    } = layer;
    const isSS = rarity === 'SS';
    const isA = rarity === 'A';
    let initialTransGauge = 0;

    const bonus = Object.fromEntries(
      terms.statusTags.values.map(tag => {
        return [tag, 0];
      })
    ) as Record<StatusTag, number>;

    // depends on layer level and layerLevelAttackType
    if (layerLevel >= 2) {
      bonus['速さ'] += 5;
      if (layerLevel >= 4) {
        bonus[layerLevelAttackType] += 20;
        if (layerLevel >= 8) {
          bonus['速さ'] += isSS ? 10 : 5;
          if (layerLevel >= 18) {
            bonus[layerLevelAttackType] += 20;
            if (layerLevel >= 22) {
              for (const tag of terms.statusTags.values) {
                bonus[tag] += 10;
              }
              if (layerLevel >= 26) {
                bonus['速さ'] += 5;
                if (layerLevel >= 34) {
                  bonus['速さ'] += 5;
                  if (layerLevel >= 38) {
                    bonus[layerLevelAttackType] += isSS ? 20 : 10;
                    if (layerLevel >= 46) {
                      bonus['速さ'] += 5;
                      if (layerLevel === 50) {
                        for (const tag of terms.statusTags.values) {
                          bonus[tag] += isSS ? 10 : 5;
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    // depends on gear level.
    if (gearLevel >= 2) {
      bonus[alpha] += 5;
      if (gearLevel >= 3) {
        bonus[beta] += 10;
        if (gearLevel >= 4) {
          bonus[alpha] += 5;
          if (gearLevel >= 6) {
            initialTransGauge += 10;
            if (gearLevel >= 7) {
              bonus[beta] += isSS ? 10 : 5;
              if (gearLevel >= 8) {
                bonus[alpha] += 10;
                if (gearLevel >= 9) {
                  for (const tag of terms.statusTags.values) {
                    bonus[tag] += 5;
                  }
                  if (gearLevel >= 10) {
                    initialTransGauge += 10;
                    if (gearLevel >= 12) {
                      bonus[beta] += isA ? 5 : 10;
                      if (gearLevel >= 13) {
                        bonus[alpha] += 10;
                        if (gearLevel >= 14) {
                          for (const tag of terms.statusTags.values) {
                            bonus[tag] += 5;
                          }
                          if (gearLevel >= 16) {
                            initialTransGauge += 20;
                            if (gearLevel >= 17) {
                              bonus[beta] += isA ? 10 : 20;
                              if (gearLevel >= 18) {
                                bonus[alpha] += isA ? 10 : 20;
                                if (gearLevel >= 19) {
                                  for (const tag of terms.statusTags.values) {
                                    bonus[tag] += isA ? 10 : 20;
                                  }
                                  if (gearLevel === 20) {
                                    initialTransGauge += 20;
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    // depends on intimacy level.
    for (const tag of terms.statusTags.values) {
      if (intimacyLevel >= 3) {
        bonus[tag] += 5;
        if (intimacyLevel >= 5) {
          bonus[tag] += 5;
          if (intimacyLevel >= 6) {
            bonus[tag] += 10;
            if (intimacyLevel >= 7) {
              bonus[tag] += 5;
              if (intimacyLevel >= 9) {
                bonus[tag] += 5;
              }
            }
          }
        }
      }
    }
    return { bonus, initialTransGauge };
  }

  /**
   * Get total layer status according to the base and layer informations.
   */
  static getLayerStatus(base: IBase, layer: ILayer): ILayer {
    return {
      status: Object.fromEntries(
        terms.statusTags.values.map(tag => {
          const scale = layer.scale[tag];
          return [tag, Math.floor((scale / 100) * base.status[tag]) + Math.floor((scale / 100) * layer.bonus[tag])];
        })
      ) as Record<StatusTag, number>,
    };
  }
}
