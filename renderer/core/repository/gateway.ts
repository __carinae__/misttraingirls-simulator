import { JsonDB } from 'node-json-db';
import { Config } from 'node-json-db/dist/lib/JsonDBConfig';
import * as path from 'path';

import { arrayOverrideMerge } from '@@core/utils';
import { DataStoreSchemaFrame, DefaultDataSchemaFrame, IEntityFrame, Repository, StateSchemaFrame } from '@@interface';
import pack from '@@package';

import { userDatabase } from './resolveUserData';

const sep = '/';
const { version } = pack;

/**
 * Gateway for saving and loading data.
 * All state properties are supposed to be related to a EntityState.
 * If an exceptional Gateway class is required, override corresponding methods in a class inheriting this class.
 * `save` & `load` can properly function in such a senario.
 */
export class GatewayBase<
  StateSchema extends StateSchemaFrame,
  DefaultDataSchema extends DefaultDataSchemaFrame,
  DataStoreSchema extends DataStoreSchemaFrame
> implements Repository<StateSchema>
{
  dbPath: string;
  db: JsonDB;
  defaultData: DefaultDataSchema;
  dbFilename: string;
  keys: string[];
  activated: boolean;

  /**
   * Define a gateway class.
   */
  constructor(defaultData: DefaultDataSchema, name: string, fileExt = '.db.json') {
    this.dbFilename = name + fileExt;
    this.dbPath = path.join(userDatabase, this.dbFilename);
    this.defaultData = defaultData;
    this.keys = Object.keys(defaultData.basis);
    this.activated = false;
  }

  /**
   * Construct a database object and set saved data if exisit.
   */
  activateDatabase(): void {
    this.db = new JsonDB(new Config(this.dbPath, true, true, sep));
    const emptyData = { memory: {}, version: '0.0.0' };
    if (!this.db.exists(sep)) {
      // Set default values if database is empty.
      this.db.push(sep, this.dataMerge(this.defaultData, emptyData as DataStoreSchema));
    } else {
      // Merge default values and saved data if exists.
      // Data may be overriden if needed.
      this.db.push(sep, this.dataMerge(this.defaultData, { ...emptyData, ...this.db.getObject<DataStoreSchema>(sep) }));
    }
    this.activated = true;
  }

  /**
   * Merge the default data and user saved data.
   * This function must be able to properly deal with the data conflict caused by version updates.
   */
  dataMerge(defaultData: DefaultDataSchema, data: DataStoreSchema): DataStoreSchema {
    const dataVersion = this.splitVersionInfo(data.version);
    data = this.convertOutdatedDataFormat(data);
    const { basis, override } = defaultData;
    let memory = arrayOverrideMerge(this.stampUndeletableTagOnDefaultEntities(basis), data.memory);

    const overrideVersions = Object.keys(override).map(this.splitVersionInfo).sort(this.compareVersions);
    for (const overrideVersion of overrideVersions) {
      if (this.compareVersions(overrideVersion, dataVersion) === 1) {
        const _override = override[overrideVersion.join('.')];

        /**
         * TODO: Value validation if override is computed at least once.
         * e.g., changing layer skill ids can make effects on the corresponding base too.
         */
        for (const adapterKey of Object.keys(_override)) {
          if (adapterKey === 'nonEntityFields') {
            memory = arrayOverrideMerge(memory, {
              nonEntityFields: Object.fromEntries(
                (_override.nonEntityFields.keys as string[]).map(key => [key, basis[adapterKey][key]])
              ),
            } as typeof memory);
          } else {
            for (const { keys, ids } of _override[adapterKey]) {
              memory = arrayOverrideMerge(memory, {
                [adapterKey]: Object.fromEntries(
                  ids.map(id => [id, Object.fromEntries(keys.map(key => [key, basis[adapterKey][id][key]]))])
                ),
              });
            }
          }
        }
      }
    }
    return { memory, version } as DataStoreSchema;
  }

  /**
   * Add `undeletable` property to all entities in the default data.
   */
  stampUndeletableTagOnDefaultEntities(basis: DefaultDataSchema['basis']): DefaultDataSchema['basis'] {
    return Object.fromEntries(
      this.keys.map(key => {
        if (key === 'nonEntityFields') {
          return [key, basis[key]];
        } else {
          return [
            key,
            Object.fromEntries(
              Object.entries(basis[key]).map(([id, entity]) => [id, { ...(entity as IEntityFrame), undeletable: true }])
            ),
          ];
        }
      })
    ) as DefaultDataSchema['basis'];
  }

  /**
   * Convert user saved data if it is outdated and has conflict with the latest data schema.
   * Expected to be implemented with switch sentences.
   * [NOTE] Expected to be overiden if needed.
   */
  convertOutdatedDataFormat(data: DataStoreSchema): DataStoreSchema {
    return data;
  }

  /**
   * Extract version information as numbers from string version tag.
   * e.g., '0.0.1' => [0, 0, 1]
   */
  splitVersionInfo(version: string): number[] {
    return version.split('.').map(str => Number(str));
  }

  /**
   * Return a number from [-1, 0, 1] which can be used in a sort function.
   */
  compareVersions(a: number[], b: number[]): number {
    let c = 0;
    a.forEach((num, i) => {
      if (num < b[i]) {
        c = -1;
      } else if (num > b[i]) {
        c = 1;
      }
    });
    return c;
  }

  /**
   * Load saved data if exists.
   */
  load(): StateSchema {
    const data = this.db.getObject<DataStoreSchema>(sep).memory;

    return Object.fromEntries(
      this.keys.map(key => {
        const d = data[key];
        if (key === 'nonEntityFields') {
          return [key, d];
        } else {
          return [key, { ids: Object.keys(d), entities: d }];
        }
      })
    ) as StateSchema;
  }

  /**
   * Save given data to the database.
   */
  save(state: StateSchema): void {
    this.db.push(sep, {
      memory: Object.fromEntries(
        this.keys.map(key => {
          const s = state[key];
          if (s.hasOwnProperty('entities')) {
            return [key, s['entities']];
          } else {
            return [key, s];
          }
        })
      ),
      version,
    });
  }

  /**
   * Load a specifyed property of saved data if exists.
   */
  loadProperty(keys: string[]): unknown {
    return this.db.getObject(sep + ['memory', ...keys].join(sep));
  }

  /**
   * Save a specifyed property to the database.
   */
  saveProperty(keys: string[], value: unknown): void {
    return this.db.push(sep + ['memory', ...keys].join(sep), value);
  }
}
