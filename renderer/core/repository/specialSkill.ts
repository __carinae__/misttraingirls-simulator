import { name } from '@@identifier/core/specialSkill';
import { DataStoreSchemas, DefaultDataSchemas, PickType, StateSchemas } from '@@interface';

import { GatewayBase } from './gateway';

type StateSchema = PickType<StateSchemas['core'], typeof name>;
type DefaultDataSchema = PickType<DefaultDataSchemas, typeof name>;
type DataStoreSchema = PickType<DataStoreSchemas, typeof name>;

/**
 * Gateway for special skill data.
 */
export class Gateway extends GatewayBase<StateSchema, DefaultDataSchema, DataStoreSchema> {
  /**
   * Convert user saved data if it is outdated and has conflict with the latest data schema.
   */
  convertOutdatedDataFormat(data: DataStoreSchema): DataStoreSchema {
    return data;
  }
}
