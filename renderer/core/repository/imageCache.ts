import { JsonDB } from 'node-json-db';
import { Config } from 'node-json-db/dist/lib/JsonDBConfig';

import { name } from '@@identifier/core/imageCache';
import { DataStoreSchemas, DefaultDataSchemas, PickType, StateSchemas } from '@@interface';
import pack from '@@package';

import { GatewayBase } from './gateway';

type StateSchema = PickType<StateSchemas['core'], typeof name>;
type DefaultDataSchema = PickType<DefaultDataSchemas, typeof name>;
type DataStoreSchema = PickType<DataStoreSchemas, typeof name>;

const sep = '/';
const { version } = pack;

/**
 * Gateway for ability data.
 */
export class Gateway extends GatewayBase<StateSchema, DefaultDataSchema, DataStoreSchema> {
  existIds: Record<string, Set<string>>;

  /**
   * Convert user saved data if it is outdated and has conflict with the latest data schema.
   */
  convertOutdatedDataFormat(data: DataStoreSchema): DataStoreSchema {
    return data;
  }

  /**
   * Construct a database object and set saved data if exisit.
   */
  activateDatabase(): void {
    this.db = new JsonDB(new Config(this.dbPath, true, true, sep));
    const emptyData = { memory: {}, version: '0.0.0' };
    let data: DataStoreSchema;
    if (!this.db.exists(sep)) {
      // Set default values if database is empty.
      data = this.dataMerge(this.defaultData, emptyData as DataStoreSchema);
    } else {
      // Merge default values and saved data if exists.
      // Data may be overriden if needed.
      data = this.dataMerge(this.defaultData, { ...emptyData, ...this.db.getObject<DataStoreSchema>(sep) });
    }
    this.existIds = Object.fromEntries(this.keys.map(key => [key, new Set(Object.keys(data.memory[key]))]));
    this.db.push(sep, data);
    this.activated = true;
  }

  /**
   * Save given data to the database.
   */
  save(state: StateSchema): void {
    for (const key of this.keys) {
      const s = state[key];
      if (s.hasOwnProperty('entities')) {
        const { ids, entities } = s;
        const newIds = ids.filter(id => !this.existIds[key].has(id));
        this.existIds[key] = new Set(ids as string[]);

        if (newIds.length) {
          this.db.push(sep + 'memory', { [key]: Object.fromEntries(newIds.map(id => [id, entities[id]])) }, false);
          this.db.reload();
        }
      } else {
        this.db.push(sep + ['memory', key].join(sep), s);
      }
    }

    this.db.push(sep + 'version', version);
  }
}
