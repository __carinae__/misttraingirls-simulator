import merge from 'deepmerge';

export const deepAccess = (data: Record<string, unknown>, keys: string[]): unknown => {
  return keys.reduce((current, key) => {
    try {
      return current[key];
    } catch (e) {
      return undefined;
    }
  }, data);
};

type SomeVar = string | number | unknown[] | Record<string, unknown>;
export const compareVars = (a: SomeVar, b: SomeVar): boolean => {
  return JSON.stringify(a) === JSON.stringify(b);
};

export const arrayOverrideMerge = <T>(a: T, b: T): T => {
  const overwriteMerge = (destinationArray: unknown[], sourceArray: unknown[]): unknown[] => sourceArray;
  return merge<T>(a, b, { arrayMerge: overwriteMerge });
};

export const mergeFields = (
  entity: Record<string, unknown>,
  changes: Record<string, unknown>,
  mergeMethod: CallableFunction = arrayOverrideMerge
): void => {
  for (const [key, change] of Object.entries(changes)) {
    if (change instanceof Object) {
      entity[key] = mergeMethod(entity[key], changes[key]);
    } else {
      entity[key] = changes[key];
    }
  }
  /** This code is equivalent to the below in the aspect of the state changes, but super super slow.
   * Maybe because it changes whole state and redux must detect it so many times.
   */
  // dict[id] = mergeMethod(dict[id], changes);
};

export const deepUpdate = (
  entity: Record<string, unknown>,
  keys: (string | number)[],
  value: unknown,
  option: { deleteArrayElement?: boolean } = {}
): unknown => {
  const { deleteArrayElement = false } = option;
  const last = keys.length - 1;
  const deleteProp = typeof value == 'undefined';
  return keys.reduce((current, key, i) => {
    try {
      if (i === last) {
        if (deleteProp) {
          if (Array.isArray(current)) {
            if (deleteArrayElement) {
              current.splice(key as number, 1);
            } else {
              current[key] = undefined;
            }
          } else {
            delete current[key];
          }
          return undefined;
        } else {
          current[key] = value;
          return value;
        }
      } else {
        return current[key];
      }
    } catch (e) {
      return undefined;
    }
  }, entity);
};

/**
 * Normalize input string for
 * 1) equating Hira-ganas and Kata-kanas,
 * 2) equating full-width and half-width alphabets and numbers,
 * 3) ignoring spaces on both ends.
 */
export const normalizeString = (str: string): string => {
  if (!str) {
    return str;
  }
  return str
    .trim()
    .replace(/[ぁ-ん]/g, b => String.fromCharCode(b.charCodeAt(0) + 0x60))
    .replace(/[Ａ-Ｚａ-ｚ０-９]/g, b => String.fromCharCode(b.charCodeAt(0) - 0xfee0));
};

export const verifyInputNumber = (
  value: unknown,
  options: {
    inputTypeInteger?: boolean;
    minValue?: number;
    maxValue?: number;
  } = {}
): boolean => {
  const { inputTypeInteger = true, minValue = -Infinity, maxValue = Infinity } = options;
  return (inputTypeInteger ? Number.isInteger : Number.isFinite)(value) && value <= maxValue && value >= minValue;
};

export const isObject = (value: unknown): boolean => {
  return value !== null && typeof value === 'object' && !Array.isArray(value);
};

export const verifyInputString = (value: unknown): boolean => {
  return typeof value == 'string' || value instanceof String;
};

export const convertToRoman = (num: number, capitalized = true): string => {
  const decimal = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
  const romanNumeral = capitalized
    ? ['M', 'CM', 'D', 'CD', 'C', 'XC', 'L', 'XL', 'X', 'IX', 'V', 'IV', 'I']
    : ['m', 'cm', 'd', 'cd', 'c', 'xc', 'l', 'xl', 'x', 'ix', 'v', 'iv', 'i'];

  let romanized = '';

  for (let i = 0; i < decimal.length; i++) {
    while (decimal[i] <= num) {
      romanized += romanNumeral[i];
      num -= decimal[i];
    }
  }
  return romanized;
};
