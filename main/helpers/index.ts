import createWindow from './create-window';
import { menuTemplate } from './menu';

export { createWindow, menuTemplate };
