# MistTrainGirls Unofficial Simulator

This repository is an unofficial simulator of MistTrainGirls, online RPG produced by EXNOA and KMS. This serves as a desktop application which can be used on Mac / Windows / Linux (inheriting [a template repository for creating nextron desktop app](https://gitlab.com/__carinae__/nextron-template)).

The purpose of this project is to build a custom database collection of basic information about characters, skills, equipments, etc., and based on the database, to create a battle simulator that helps construct strategies for high level contents such as INFERNO raid, allied battle, altars of conquering a devil, etc.

## Sample Demo

The latest sample video (Japanese):

[![](https://img.youtube.com/vi/VTk_ABKZG9s/0.jpg)](https://www.youtube.com/watch?v=VTk_ABKZG9s)

If you would like to see more details, please check my [YouTube channel](https://www.youtube.com/channel/UCy6rMnzsiOMRa6oCV3iefWg).

## Requirements

- npm: ^16.0.0
- yarn: ^1.22

## Install Dependencies

```bash
$ cd misttraingirls-simulator
$ yarn install
```

## Fix the codes with ESLint & Prettier

```bash
$ yarn fix:all
```

Code checkers will be also automatically activated when a new git commit is done.

## Developer mode & build instruction

```bash
# development mode
$ yarn dev
# you can also specify the port (default port: 8888)
$ yarn dev -p 8889

# production build
$ yarn build
# you can also specify the build options, for example,
$ yarn build:mac
```

## Functionalities

### About MistTrainGirls

- Database & Edit Panel
  - [x] Base characters
    - [x] Base status growth & limit table
  - [x] Layer information connecting to base characters
  - [ ] Layer abilities
  - [ ] Skills
  - [ ] Special skills
  - [ ] Weapons
  - [ ] Armors
  - [ ] Accessorys
  - [ ] Orbs
  - [ ] Equipment abilities
  - [ ] Party skills
  - [ ] Formations
  - [ ] Enemies
  - [ ] Enemy skills
  - [ ] Enemy abilities
- Battle Simulator
  - [ ] Map creator
    - [ ] 1 wave
    - [ ] Multi waves
  - [ ] Party formation
  - [ ] Action time series
    - [ ] Arbitrary number of turns per wave
    - [ ] Damage computation
    - [ ] Buff/Debuff checker
      - [ ] Buffs distribution viewers
      - [ ] Picker at an arbitrary turn (conditional probabilities)
    - [ ] Status ailment checker
    - [ ] Random attack target selector where there is greater than one enemy
    - [ ] Auto optimizer after fixing enemies' actions
          (This may be impossible in terms of computation cost but would like to try at least)
      - [ ] Action series
      - [ ] Equipments
      - [ ] Party skills
      - [ ] Formation

### General Functions

- [x] Clean architecture with typescript interfaces and redux toolkit
- [x] Auto-defined EntityAdapters which have sortable & filterable selectors of entities
- [x] Single page application which can switch arbitrary numbers of content panels
- [x] Data persistency with local json database
- [x] ESLint & Prettier code format checker
- [x] Loading screen in launching application
- [x] Custom application menu
- [x] Data override onto default data so that user-made database could be kept even if the app (=default data) would be updated
- [x] Data override onto outdated data so that critical content changes to already existing enttities can be reflected
- [x] Keyboard shortcuts for data save & other ops
- [x] Save when closing main window (Electron IPC)
  - [x] One second timeout to force window close for infinite loop preventing from quitting
- [x] Generic entity tab list
  - [x] Flexible sort options
  - [x] Flexible filter options
  - [x] high extendability
  - [x] CRUD operations
  - [x] Virtualized list
- [x] Image cache as Base64 data format
  - [x] Image saving only for newly added data (otherwise every time data are saved, tens to hundreds MB files must be dealt)
- [x] Easy default data format
- [x] Arbitrary data saving & loading without using EntityState
- [x] Undo/Redo functions which can work with redux toolkit
  - [x] If you implement this with redux-undo, you may have to
    - [x] put some of the useSates for the ui operations which are not related to the data persistance into redux reducer methods which you want undo/redo to deal with
    - [x] make grouped actions which you would like to see as unit actions that one step redo/undo can change
    - [x] enable some action to be separated from undo/redo dunctionality (e.g., image cahce, list movement).
