module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true,
  },
  extends: [
    'plugin:react/recommended',
    'google',
    'plugin:@typescript-eslint/recommended',
    'plugin:@next/next/recommended',
    'prettier',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: 'module',
  },
  plugins: ['react', '@typescript-eslint', 'simple-import-sort', 'prettier'],
  rules: {
    'react/prop-types': 'off',
    'prettier/prettier': ['error'],
    'require-jsdoc': 'off',
    'valid-jsdoc': 'off',
    '@next/next/no-img-element': 'off',
    'simple-import-sort/imports': 'error',
    'simple-import-sort/exports': 'error',
  },
  // reference: https://stackoverflow.com/questions/60547153/eslint-parsing-error-unexpected-token-on-typescript-cast-operator-as
  settings: {
    react: {
      pragma: 'React',
      version: 'detect',
      flowVersion: '0.53',
    },
    overrides: [
      {
        files: ['*.js', '*.jsx'],
        rules: {
          '@typescript-eslint/explicit-function-return-type': 'off',
        },
      },
    ],
    'import/resolver': {
      node: {
        extensions: ['.js', '.jsx', '.ts', '.tsx'],
        moduleDirectory: ['node_modules'],
      },
    },
  },
};
